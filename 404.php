<?php
	// Template Name: 404
	get_header();

	header("Location: ".get_bloginfo('url'));
	exit();
?>

	<section class="landing">
		<div class="container landing__container">
			<div class="row">
				<div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
					<div class="tire">
						<div class="tire__triangle"></div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tire.png" class="tire__image">
					</div>
					<h3>Je bent offroad gegaan</h3>
					<p>Sorry, we kunnen de pagina die je zoekt niet vinden.</p>
					<a href="<?php echo get_site_url(); ?>" class="button">Naar home</a>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>