<?php

get_header();
$map_settings         = new stdClass();
$map_settings->marker = get_stylesheet_directory_uri() . '/assets/img/marker.png';
$map_settings->zoom   = intval( '10' ); //intval(get_sub_field('map_zoom_leven'));

$current_location = get_flow_info( 'current-location' ); // lat lng
if ( isset( $current_location['lat'] ) && isset( $current_location['lng'] ) ) {
	$current_latlng = $current_location['lat'] . ', ' . $current_location['lng'];
}

?>
    <script>
        var map_settings = <?php echo json_encode( $map_settings ); ?>;
    </script>

    <div class="map acf-map">

        <div class="map__overlay js-map-overlay">
            <form action="" class="map__form">
                <div class="map__input-wrapper">
                    <input id="location-input" class="map__input" type="search"
                           data-latlng="<?php echo $current_latlng ?? ''; ?>" placeholder="Postcode of plaatsnaam"
                           autofocus>
                    <a href="" class="gps get_gps_location">
                        <svg class="gps-icon">
                            <use xlink:href="#icon-gps"/>
                        </svg>
                    </a>
                </div>
                <div class="map__button-wrapper">
                    <button type="submit" class="map__button load-locations-with-ajax">
                        <svg class="map__button-icon">
                            <use xlink:href="#icon-search"/>
                        </svg>
                    </button>
                </div>
            </form>

            <div class="map__results ajax-posts-results">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/loop/content', 'garage' ); ?>
				<?php
				endwhile;
				wp_reset_postdata();
				?>
            </div><!-- end map__results -->

            <div class="map__detail">
                <div class="map__back js-map-result">Ga terug</div>
                <article><!-- content get loaded here --></article>
            </div>
        </div>
        <div id="map-canvas" class="map__element map-canvas"></div>

    </div>

<?php get_footer();
