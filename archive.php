<?php
/**
 * archive template file
 *
 */

get_header(); ?>

<header class="hero--news">
  <?php include( STYLESHEETPATH. '/parts/shape-animation.php');  ?>
  <div class="container container--hero">
    <div class="top--news">
      <h1 class="news__hero-title"><?php the_archive_title(); ?></h1>
    </div>
  </div>
</header>

<section class="news__section">
  <div class="container  container--news">
    <div id="main" class="row row--news">

    <?php if ($wp_query->have_posts() ) : ?>
    <?php
        //pre_print_r($wp_query->query_vars);
    ?>

      <!-- pagination here -->

      <?php
        $post_type = get_post_type();
      ?>
      <!-- the loop -->
      <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
          <?php get_template_part( 'template-parts/loop/content', $post_type ); ?>
      <?php endwhile; ?>

      <!-- end of the loop -->

        <navigation class="pagination">
          <?php $args = array(
          'base'               => '%_%',
          'format'             => '?paged=%#%',
          'total'              => 1,
          'current'            => 0,
          'show_all'           => false,
          'end_size'           => 1,
          'mid_size'           => 2,
          'prev_next'          => true,
          'prev_text'          => __('« Vorige'),
          'next_text'          => __('Volgende »'),
          'type'               => 'plain',
          'add_args'           => false,
          'add_fragment'       => '',
          'before_page_number' => '',
          'after_page_number'  => ''
          ); ?>
          <?php //echo paginate_links( ); ?>
        </navigation>

      <!-- pagination here -->
      <?php
      global $wp_query; // you can remove this line if everything works for you

      // don't display the button if there are not enough posts
      if (  $wp_query->max_num_pages > 1 )
        echo '<button class="button button--primary button--news load-ajax-posts">Meer berichten</button>';
      ?>

      <?php wp_reset_postdata(); ?>

    <?php else : ?>
      <p><?php esc_html_e( 'Sorry, we kunnen de pagina die je zoekt niet vinden.' ); ?></p>
    <?php endif; ?>

    </div>
  </div>
</section>



<?php get_footer();
