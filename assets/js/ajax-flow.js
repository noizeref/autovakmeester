jQuery(function($){

  //location session
  $('.location-search__results').on('click', '.location-search__card', function(event){
    event.preventDefault();
    var location = JSON.stringify({"location":{"page-id":$(this).attr('data-page-id')}});
    SetFlowSessionData(location, function() { window.location.href = flowUrls[2] });
  });


  //checklist session
  $(".checklist__form").change(function() {
      var tasks = {};
      $(".checklist__form input:checked").each(function() {
        var element = $(this);
        if(element.attr('id') == 'checklist-text'){ // is is text
          tasks[element.attr('id')] = $('.checklist__textarea').val();
          // console.log('get text!');
        } else{
          tasks[element.attr('id')] = element.val();
        }

      });
      tasks = JSON.stringify({"tasks":tasks});
      // console.log(tasks);
      SetFlowSessionData(tasks);
  });

  //timeslots session
  $(".schedule__checkbox").change(function() {
      var timeslots = {};

      var checklimit = 4;
      if($("input.schedule__checkbox:checked").length > checklimit) {
        this.checked = false;
        $('.schedule-notice').fadeIn().delay(1000).fadeOut();
      } else{
        $(".schedule__checkbox:checked").each(function(i) {
          var element = $(this);
          timeslots[element.attr('id')] = element.val();
        });

        if($.isEmptyObject(timeslots)){
          $('.gfield.application-type select').val('Offerte aanvraag');
        } else{
          $('.gfield.application-type select').val('Afspraak aanvraag');
        }

        $('.timeslots-list .gfield_list input').val('');
        var i = 0;
        $.each(timeslots , function(index, val) {
          i++;
          $('.timeslots-list .gfield_list tbody tr:nth-child('+i+') input').val( val );
          //console.log(index, val);
        });



        timeslots = JSON.stringify({"timeslots":timeslots});
        //console.log(timeslots);
        SetFlowSessionData(timeslots);
      }

  });

  $("#privacyCheckbox").change(function(event){
    event.preventDefault();
    if( $(this).prop( "checked" ) ){
      $('.flow__end button').prop("disabled", false); // Element(s) are now enabled.
    }else{
      $('.flow__end button').prop("disabled", true); // Element(s) are now disabled.
    }
  });

  $('#flow-submit').click(function(event){
    event.preventDefault();
    formid = $(this).data('formid');
    $('#gform_' + formid)[0].submit();
    // console.log('go submit: #gform_' + formid);
  });
});

function SetFlowSessionData(newData, callback){
    data = {
      'action'      : 'load_ajax_flow_session_handler',
      'session'     : load_ajax_flow_session_params.session, // that's how we get params from wp_localize_script() function
      'new_data'    : newData
    };

    //console.log('bestaande array' + load_ajax_flow_session_params.session);
    //console.log('toevoeging array' + newData);

    $.ajax({
      url : load_ajax_flow_session_params.ajaxurl, // AJAX handler
      data : data,
      type : 'POST',
      success: function(data, xhr) {
          //console.log('added session data:' + data); // do with data e.g success message
          if ($.isFunction(callback)) {
            callback.call();
          }
      },
      error: function(xhr) {
          console.log('nope.....');
      }
    });
}//end LoadLocationsWithAjax()


