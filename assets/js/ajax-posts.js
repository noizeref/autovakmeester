jQuery(function($){
  $('.load-ajax-posts').click(function(){
    var button = $(this),
    data = {
      'action'      : 'load_ajax_posts_handler',
      'query'       : load_ajax_posts_params.wp_query, // that's how we get params from wp_localize_script() function
      'page'        : load_ajax_posts_params.current_page,
      'search_val'  : load_ajax_posts_params.search_val
    };
    $.ajax({
      url : load_ajax_posts_params.ajaxurl, // AJAX handler
      data : data,
      type : 'POST',
      beforeSend : function ( xhr ) {
        button.text('Loading...'); // change the button text, you can also add a preloader image
      },
      success : function( data ){
        if( data ) {
          button.text( load_ajax_posts_params.loadmore ).prev().before(data); // insert new posts
          load_ajax_posts_params.current_page++;

          if ( load_ajax_posts_params.current_page == load_ajax_posts_params.max_page )
            button.remove(); // if last page, remove the button

          // you can also fire the "post-load" event here if you use a plugin that requires it
          // $( document.body ).trigger( 'post-load' );
        } else {
          console.log(load_ajax_posts_params.noposts);
          button.remove(); // if no data, remove the button as well
        }
      }
    });
  });
});

jQuery(function($){
  $('.load-locations-with-ajax').click(function(event){
    event.preventDefault();
    $('.get_gps_location').addClass('loading');

    GetLatLngByAddres();
  })
});



function LoadLocationsWithAjax(){
    var latlng = $('#location-input').attr('data-latlng');
    var latlngStr = latlng.split(',', 2);
    var button = $('.load-locations-with-ajax');

    data = {
      'action'      : 'load_ajax_posts_handler',
      'query'       : load_ajax_posts_params.wp_query, // that's how we get params from wp_localize_script() function
      'page'        : load_ajax_posts_params.current_page,
      'is_archive'  : load_ajax_posts_params.is_archive,
      'latlng'      : latlngStr,
      'is_html'        : button.length
    };

    console.log(data)

    $.ajax({
      url : load_ajax_posts_params.ajaxurl, // AJAX handler
      data : data,
      type : 'POST',
      beforeSend : function ( xhr ) {
        //button.text('Loading...'); // change the button text, you can also add a preloader image
        $('.ajax-posts-results').addClass('loading-blink');
        $('.home .hero').addClass('loading-circle');
        $('.ajax-posts-results').addClass('loading-circle');
      },
      success : function( data ){
        if( data ) {
            $('.ajax-posts-results').html( data ); // where to insert posts
            $('.ajax-posts-results').removeClass('loading-blink');
            $('.ajax-posts-results').removeClass('loading-circle');
            $('.home .hero').removeClass('loading-circle');

          // you can also fire the "post-load" event here if you use a plugin that requires it
          // $( document.body ).trigger( 'post-load' );
        } else {
          console.log(load_ajax_posts_params.noposts);
        }
      }
    });
}//end LoadLocationsWithAjax()




// NIET IN GEBRUIK....
jQuery(function($){
  $("#searchsubmit").click(function(e){
    e.preventDefault();

    var search_val=$("#s").val();

    $.ajax({
      url : load_ajax_posts_params.ajaxurl, // AJAX handler
      type:"POST",
      data: {
        action:'load_ajax_posts_handler',
        search_val:search_val
      },
      success:function(response){
        $('#main').html(response);
      }
    });
  });
});

