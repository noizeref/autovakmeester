jQuery(function($){

  $( ".license-plate__check" ).click(function(event) {
      event.preventDefault();
      get_rdw_data();
  });

  $( ".license-plate__reset" ).click(function() {
    $('.license-plate__reset').hide();
    $('.license-plate__skip').show();
    $('.license-plate__input').val('');
    $('.license-plate__submit').removeClass('active');
    $('.license-plate__card').hide();
    $('.license-plate__card').html('')
  });


});

function get_rdw_data(licenseplate_param){
  $('.license-plate__card').show();

  if (typeof(licenseplate_param)==='undefined' && $('.license-plate__input').val() ){
    licenseplate = $('.license-plate__input').val();
  }else if(typeof(licenseplate_param)!='undefined'){
    licenseplate = licenseplate_param;
  } else {
    $(".license-plate__card").html('<div class="-caption">Geen kenteken opgegeven...</div>')
    return;
  }


  if (licenseplate.length) {
    licenseplate = licenseplate.replace(/[^a-zA-Z0-9]+/g, '').toUpperCase();
    $.ajax({
      url: "https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken="+licenseplate,
      type: "GET",
      data: {
        "$limit" : 5000,
        "$$app_token" : "xKKv2hRgOcIvXbAfq8E1F1gbs"
      }
    }).done(function(data) {
      //console.log(data);
      var html = '';
      if(data.length){
        $.each(data, function(key, value){
            vehicle = {
              "licenseplate"  : value.kenteken,
              "brand"         : value.merk,
              "type"          : value.handelsbenaming,
              "apkdate"       : value.vervaldatum_apk
            }
        });
        $('.license-plate__submit').addClass('active');
        $('.license-plate__reset').show();
        $('.license-plate__skip').hide();


        var vehicle = JSON.stringify({vehicle:vehicle});
        SetFlowSessionData(vehicle);
        LoadRDWDataWithAjax(vehicle);
      } else {
        $(".license-plate__card").html('<div class="-caption">Geen geldig kenteken opgegeven...</div>')
      }
    });
  }
}

$( ".license-plate__reset" ).click(function() {
  $('.license-plate__reset').hide();
  $('.license-plate__skip').show();
  $('.license-plate__input').val('');
  $('.license-plate__submit').removeClass('active');
  $('.license-plate__card').hide();
  $('.license-plate__card').html('')
});


function LoadRDWDataWithAjax(vehicle){
    data = {
      'action'      : 'load_ajax_rdw_handler',
      'session'     : load_ajax_rdw_params.session, // that's how we get params from wp_localize_script() function
      'vehicle'     : vehicle
    };
    $.ajax({
      url : load_ajax_rdw_params.ajaxurl, // AJAX handler
      data : data,
      type : 'POST',
      beforeSend : function ( xhr ) {
        //button.text('Loading...'); // change the button text, you can also add a preloader image
      },
      success : function( data ){
        if( data ) {
            $('.license-plate__card').replaceWith( data ); // where to insert posts
        } else {
          //console.log(load_ajax_posts_params.noposts);
        }
      }
    });
}//end LoadRDWDataWithAjax()

