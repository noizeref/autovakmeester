// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.


$(".get_gps_location").click(function (event) {
    event.preventDefault();
    get_geolocation();
    $(this).addClass('loading');
});

$(".hero__search-form").on("submit", function (event) {
    event.preventDefault();
    var location = $(this).attr('action');

    $(".get_gps_location").addClass('loading');
    GetLatLngByAddres('', function () {
        window.location = location;
    });
});

function get_geolocation() {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var latlng_pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            // console.log('Location found: ' + latlng_pos.lat + ', ' + latlng_pos.lng);
            $('#location-input').attr('data-latlng', latlng_pos.lat + ', ' + latlng_pos.lng);

            GetAddresByLatLng(latlng_pos);

            $('.get_gps_location').removeClass('loading');
        }, function () {
            handleLocationError(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false);
    }
}

function handleLocationError(browserHasGeolocation) {
    errormessage = (browserHasGeolocation ? 'Het ophalen van jouw locatie is mislukt' : 'De browser lijkt geolocation niet te ondersteunen.');
    $('<div class"geo-error-message">' + errormessage + '</div>').insertAfter('.location-search__form');
}


//Reverse geocode to get address for automatic options needed for directions link
function GetAddresByLatLng(latlng_pos) {
    if (latlng_pos) {
        var latlng_pos = latlng_pos;
    } else {
        var input = $('#location-input').data('latlng');
        var latlngStr = input.split(',', 2);
        var latlng_pos = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
    }
    var geocoder = new google.maps.Geocoder;

    geocoder.geocode({'location': latlng_pos}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) { //succes!
                $('#location-input').val(results[0].formatted_address);
                $('.get_gps_location').removeClass('loading');
                SetMapToNewCenter(latlng_pos);

                var currentLocation = JSON.stringify({"current-location": latlng_pos});
                SetFlowSessionData(currentLocation);

                LoadLocationsWithAjax();
            } else {
                console.log('No results found');
                shake('form');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
            shake('form');
        }
    });
}

function shake(element) {
    $(element).addClass('shake').delay(500).queue(function () {
        $(this).removeClass('shake').dequeue();
    });
}

//Geocode function for the origin location
function GetLatLngByAddres(address, callback) {
    if (address) {
        address = address;
    } else {
        var value = $('#location-input').val();
        address = value.trim().toUpperCase().replace(/\s/g, '');
    }

    var geocoder = new google.maps.Geocoder;

    address = address + ", Nederland";
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {

                // console.log(results);
                var latlng_pos = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                };
                $('#location-input').attr('data-latlng', latlng_pos.lat + ', ' + latlng_pos.lng);
                SetMapToNewCenter(latlng_pos);

                var currentLocation = JSON.stringify({"current-location": latlng_pos});
                SetFlowSessionData(currentLocation);
                LoadLocationsWithAjax();
                if (typeof callback === "function") {
                    callback(true)
                }
                ;
            } else {
                console.log('No results found');
                shake('form');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
            shake('form');
        }
        $('.get_gps_location').removeClass('loading');
    });
}



