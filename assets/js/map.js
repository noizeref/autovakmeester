
  /*
  *  new_map
  *
  *  This function will render a Google Map onto the selected jQuery element
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param $el (jQuery element)
  *  @return  n/a
  */

  function new_map( $el ) {

    var str_map_settings = JSON.stringify(map_settings);
    map_settings = JSON.parse(str_map_settings);


    // var
    var mapResults = $el.find('.map__result.marker');

    //var mapCanvas = $el.find('.map-canvas');
    var mapCanvas = document.getElementById('map-canvas')

    // vars
    var mapOptions = {
        zoom: 10,
        center    : new google.maps.LatLng(0, 0),
        minZoom: 2,
        zoomControl: true,
        scaleControl: false,
        scrollwheel: false,
        streetViewControl: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDoubleClickZoom: false,
        styles: [
          {
              "featureType": "administrative",
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "color": "#444444"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "#f2f2f2"
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "lightness": 45
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "labels.icon",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "#c0e4f3"
                  },
                  {
                      "visibility": "on"
                  }
              ]
          }
      ]
    };

    // create map
    var map = new google.maps.Map( mapCanvas, mapOptions);
    markerIconDefault = new google.maps.MarkerImage(map_settings.marker, null, null, null, new google.maps.Size(60, 60));
    markerIconActive = new google.maps.MarkerImage(map_settings.marker, null, null, null, new google.maps.Size(80, 80));

    // add a markers reference
    map.markers = [];


    // add markers
    mapResults.each(function(){
        add_marker( $(this), map );
    });


    // center map
    center_map( map );
    $("#world-map-filter").fadeIn(600);


    // return
    return map;

  }

  /*
  *  add_marker
  *
  *  This function will add a marker to the selected Google Map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param mapResult (jQuery element)
  *  @param map (Google Map object)
  *  @return  n/a
  */

  function add_marker( mapResult, map ) {

    // var
    var latlng = new google.maps.LatLng( mapResult.attr('data-lat'), mapResult.attr('data-lng') );

    // create marker
    marker = new google.maps.Marker({
      position    : latlng,
      map         : map,
      animation   : google.maps.Animation.DROP,
      icon        : markerIconDefault,
      pageID      : mapResult.attr('data-page-id')
    });

    // add to array
    map.markers.push( marker );

    // if marker contains HTML, add it to an infoWindow
    if( mapResult.html() ){
      // create info window
      infowindow = new google.maps.InfoWindow({
        content   : mapResult.html()
      });

      // show info window when marker is clicked
      google.maps.event.addListener(marker, 'click', function() {
        show_marker(mapResult.attr('data-page-id'));
      });
    }
/*
    google.maps.event.addListener(map, 'click', function() {
        map.setZoom(map_settings.zoom);
    });
*/
  }



  function show_marker( pageID ) {
    var marker = false;
    for (var i = 0; i < map.markers.length; i++){
      map.markers[i].setIcon(markerIconDefault); // reset icon to default size, http://jsfiddle.net/kV6Ys/54/.
      var m = map.markers[i];
      if (m.pageID == pageID){
        marker = m;
        marker.setIcon(markerIconActive); // set to icon active size
      }
    }

    if (!marker){
      console.log("Marker " + pageID + " not found!")
      return;
    }
    //console.log(marker.pageID);

    // get the html
    detailHTML = $('.map__result.marker[data-page-id="'+marker.pageID+'"]').find('article').html();

    // set the html in the detail box
    $('.map__detail article').html(detailHTML);
    $('.map__overlay').addClass('map__overlay--detail');
    //infowindow.open( map, marker );
    map.panTo(marker.getPosition());
    map.setZoom(map_settings.zoom + 1);
    map.panBy(-200,0); // (x,y)
  }

  $(document).on("click", ".map__result.marker", function(e) {
    var pageID = $(e.currentTarget).data('page-id');
    show_marker(pageID);
  });


  /*
  *  center_map
  *
  *  This function will center the map, showing all markers attached to this map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 4.3.0
  *
  *  @param map (Google Map object)
  *  @return  n/a
  */

  function center_map( map ) {
    // vars
    var bounds = new google.maps.LatLngBounds();


    if( $('#location-input').data('latlng')){
      var input = $('#location-input').data('latlng');
      var latlngStr = input.split(',', 2);
      var latlng_pos = {
        lat: parseFloat(latlngStr[0]),
        lng: parseFloat(latlngStr[1])
      };
      initialLocation = new google.maps.LatLng(latlng_pos.lat, latlng_pos.lng);
      map.setCenter(initialLocation);
      map.setZoom(map_settings.zoom + 1);
    } else {
      // loop through all markers and create bounds
      $.each( map.markers, function( i, marker ){
        var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
        bounds.extend( latlng );
      });

      // only 1 marker?
      if( map.markers.length == 1 ){
        // set center of map
        map.setCenter( bounds.getCenter() );
        map.setZoom( 16 );
      } else {
        // fit to bounds
        map.fitBounds( bounds );
      }
    }




    //console.log('Center map' + bounds);
  }





  /*
  *  document ready
  *
  *  This function will render each map when the document is ready (page has loaded)
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since 5.0.0
  *
  *  @param n/a
  *  @return  n/a
  */

  // global var
  var map = null;
  $(document).ready(function(){

    $('.acf-map').each(function(){
      //console.log('Make map');
      // create map
      map = new_map( $(this) );
    });
  });


  function SetMapToNewCenter(latlng_pos){
    // if google map is loaded
    if ($('#map-canvas').length && typeof map !== ('undefined' || null) ){
      //console.log(latlng_pos)
      initialLocation = new google.maps.LatLng(latlng_pos.lat, latlng_pos.lng);
      map.setCenter(initialLocation);
      map.setZoom(12);
    }
  }



