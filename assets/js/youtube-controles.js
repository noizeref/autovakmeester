
jQuery(function($) {
  $('.youtube-playToggle').click(function(e){
      e.preventDefault();
      playToggleYoutubePlayer()
  });
  $('.youtube-muteToggle').click(function(e){
    e.preventDefault();
    muteToggleYoutubePlayer();
  });
}); //doc ready




$(document).ready(function(){
  sizeTheVideo();
  $(window).resize(function(){
    sizeTheVideo();
  });
});
function sizeTheVideo(){
  // - 1.78 is the aspect ratio of the video
// - This will work if your video is 1920 x 1080
// - To find this value divide the video's native width by the height eg 1920/1080 = 1.78
  var aspectRatio = 1.78;

    var video = $('.youtube-embed.fullscreen iframe');
    var videoHeight = video.outerHeight();
    var newWidth = videoHeight*aspectRatio;
		var halfNewWidth = newWidth/2;

  //Define the new width and centrally align the iframe
  video.css({"width":newWidth+"px","left":"50%","margin-left":"-"+halfNewWidth+"px"});
}




//load the IFrame Player API code asynchronously
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//will be youtube player references once API is loaded
var players = [];

//gets called once the player API has loaded
function onYouTubeIframeAPIReady() {
  $('.youtube-embed iframe[data-enablejsapi="1"]').each(function() {
    var frame = $(this);
    //create each instance using the individual iframe id
    var player = new YT.Player(frame.attr('id'));

    players.push(player);
  });
}

function playToggleYoutubePlayer(){
  //loop through each Youtube player instance and call stopVideo()
  for (var i in players) {
    var player = players[i];
    if(player.getPlayerState() === 1){
      player.pauseVideo();
    } else {
      player.playVideo();
    }
  }
}
function muteToggleYoutubePlayer(){
  //loop through each Youtube player instance and call muteVideo()
  for (var i in players) {
    var player = players[i];
    if(player.isMuted()){
      player.unMute();
    }else{
      player.mute();
    }
  }
}
function restartYoutubePlayer(){
  //loop through each Youtube player instance and call muteVideo()
  for (var i in players) {
    var player = players[i];
    player.playVideoAt(0);
  }
}