  <footer class="footer">
      <div class="footer__container container">
        <div class="footer__flex">
          <div class="footer__col">
            <a href="<?php echo get_site_url(); ?>" class="footer__logo"></a>
          </div>
          <div class="footer__col">
            <h4 class="footer__title js-expand"><span><?php echo get_theme_location_menu_name('footer-menu-1'); ?></span><svg class="footer__arrow"><use xlink:href="#icon-faq" /></svg></h4>
            <?php bem_menu('footer-menu-1','footer__list footer__expand','footer'); ?>
          </div>
          <div class="footer__col">
            <h4 class="footer__title js-expand"><span><?php echo get_theme_location_menu_name('footer-menu-2'); ?></span><svg class="footer__arrow"><use xlink:href="#icon-faq" /></svg></h4>
            <?php bem_menu('footer-menu-2','footer__list footer__expand','footer'); ?>
          </div>
          <div class="footer__col">
            <h4 class="footer__title js-expand"><span><?php echo get_theme_location_menu_name('footer-menu-3'); ?></span><svg class="footer__arrow"><use xlink:href="#icon-faq" /></svg></h4>
            <?php bem_menu('footer-menu-3','footer__list footer__expand','footer'); ?>
          </div>
        </div>
      </div>
  </footer>
  <section class="copyright">
    <div class="container">
      <div class="copyright__flex">
        <div class="copyright__col">
          <?php bem_menu('footer-meta-menu','copyright__list','copyright'); ?>
        </div>
        <div class="copyright__col">
          <a href="https://www.bovag.nl/home" target="_blank" class="copyright__logo"></a>
        </div>
      </div>
    </div>
  </section>
  <?php
  get_template_part('parts/popup');
  wp_footer();
  if ($_SERVER['REMOTE_ADDR'] == "80.61.241.98") {
    // do_action("hexon_parser_cron");
  }
  ?>

</body>
</html>
