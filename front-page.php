<?php
/**
 * Main template file
 * Template Name: Homepagina
 */

get_header();
?>

    <header class="hero">
		<?php
		if ( has_post_thumbnail() ):
			$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' );
			$featured_image_url = $featured_image[0];
		else:
			$featured_image_url = get_stylesheet_directory_uri() . '/assets/img/avm-header.jpg';
		endif;
		?>
        <div class="hero__image" style="background-image:url(<?php echo $featured_image_url; ?>)"></div>
        <div class="hero__art"></div>
        <div class="hero__container container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2">
                    <div class="hero__search-caption">
                        <h1 class="hero__search-title">
							<?php
							if ( get_field( 'page_title' ) ):
								the_field( 'page_title' );
							else:
								echo 'Zoek een vestiging in de buurt';
							endif;
							?>
                        </h1>
                        <div class="hero__search-form-animation">
                        <form class="hero__search-form" action="<?php echo get_post_type_archive_link( 'garage' ); ?>"
                              method="post">
                            <div class="hero__search-input-wrapper">
                                <input id="location-input" class="hero__search-input" type="search" placeholder=""
                                       autofocus>
                                <a href="" class="gps get_gps_location">
                                    <svg class="gps-icon">
                                        <use xlink:href="#icon-gps"/>
                                    </svg>
                                </a>
                            </div>
                            <button type="submit" class="hero__search-button">
                                <svg class="hero__search-icon">
                                    <use xlink:href="#icon-search"/>
                                </svg>
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php
// Partials
include( 'includes/partials/partials.php' );
?>

<?php get_footer();
