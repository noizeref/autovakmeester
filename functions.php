<?php
// includes
include( 'functions/navigation.php' );
include( 'functions/acf-options.php' );
include( 'functions/enqueue.php' );
include( 'functions/klantenvertellenv3.php' );
include( 'functions/ajax-posts.php' );
include( 'functions/ajax-rdw.php' );
include( 'functions/flow.php' );
//include('includes/hexon/doorlinkenvoorraad.php');
include( 'includes/gravity-hexon/hexon.php' );
include( 'includes/geo_query.php' );
include( 'functions/nav-menu-anchors.php' );
include( 'functions/list-pages-bem.php' );
include( 'functions/yoast-breadcrumbs.php' );
include( 'functions/custom-login.php' );

use Gravity\Hexon\Parser\HexonParser;

// formulieren met address field tonen hiermee eerst postcode dan de stad
add_filter( 'gform_address_display_format', 'address_format' );
function address_format( $format ) {
	return 'zip_before_city';
}

add_filter( 'post_type_archive_link', 'blog_post_type_archive_link', 10, 2 );
function blog_post_type_archive_link( $link, $post_type ) {
	if ( $post_type === 'post' ) {
		$link = home_url( '/nieuws/' );
	}

	return $link;
}


//hide customfields metabox for garagist
add_action( 'do_meta_boxes', 'remove_default_custom_fields_meta_box', 1, 3 );
function remove_default_custom_fields_meta_box( $post_type, $context, $post ) {
	if ( ! current_user_can( 'edit_others_posts' ) ) {
		remove_meta_box( 'postcustom', $post_type, $context );
	}
}


add_filter( 'acf/prepare_field/name=occasions_link', 'my_acf_prepare_field' );
add_filter( 'acf/prepare_field/name=hexon_klantennummer', 'my_acf_prepare_field' );
add_filter( 'acf/prepare_field/name=klantenvertellen_clientid', 'my_acf_prepare_field' );
function my_acf_prepare_field( $field ) {
	$user = wp_get_current_user();
	if ( in_array( 'garagist', (array) $user->roles ) && $field['value'] ) {
		$field['disabled'] = true;
		$field['readonly'] = true;

		return false; // diabled get removed by conditional logic
	}

	return $field;
}


// user role
function get_user_roles_by_user_id( $user_id ) {
	$user = get_userdata( $user_id );

	return empty( $user ) ? array() : $user->roles;
}

// check if is user role
function is_user_in_role( $user_id, $role ) {
	return in_array( $role, get_user_roles_by_user_id( $user_id ) );
}


//return the array with flow urls or a specific url if a key is provided
function get_flow_urls( $key_arg = '' ) {
	$flow_menu_items = wp_get_nav_menu_items( 'Flow stappen menu' );
	foreach ( $flow_menu_items as &$item ) {
		if ( $item->url ) {
			$menu_urls_array[] = $item->url;
		}
	}
	if ( isset( $key_arg ) && array_key_exists( $key_arg, $menu_urls_array ) ) {
		return $menu_urls_array[ $key_arg ];
	} else {
		return $menu_urls_array;
	}

	return false;
}

//return post id of the highest ancestor page
function get_garage_home_id() {
	global $post, $wp_query;
	if ( isset( $post->post_parent ) && $post->post_parent != 0 ) {
		$ancestors      = $post->ancestors ?? '';
		$garage_home_id = end( $ancestors );

		//pre_print_r('i am sub of: '. $garage_home_id);
		return $garage_home_id;

	} elseif ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
		$search = new Gravity\Hexon\Search\HexonSearch();
		$garage = $search->get_page_by_post_name( $garage_slug, OBJECT, 'garage' );

		return $garage->ID;
	} else {
		//pre_print_r('i am home: '. $post->ID);
		return $post->ID;
	}
}


// add the grage email and set it as TO in gravityforms for the contact form
add_filter( 'gform_field_value_to-email', 'populate_contact_to_email_field', 10, 3 );
function populate_contact_to_email_field( $value, $field, $name ) {
	if ( get_post_type() == 'garage' ) {
		$user_id  = get_the_author_meta( 'ID' );
		$to_email = get_the_author_meta( 'email', $user_id );
		$values   = array(
			'to-email' => $to_email
		);

		return $values[ $name ] ?? $value;
	} else {
		return $value;
	}
}


// Register Custom Post Type
function garage_template_post_type() {

	$labels = array(
		'name'                  => _x( 'Garages templates', 'Post Type General Name', 'gravity_theme' ),
		'singular_name'         => _x( 'Garage template', 'Post Type Singular Name', 'gravity_theme' ),
		'menu_name'             => __( 'Garages templates', 'gravity_theme' ),
		'name_admin_bar'        => __( 'Garage templates', 'gravity_theme' ),
		'archives'              => __( 'Garage templates overzicht', 'gravity_theme' ),
		'attributes'            => __( 'Garage template attributes', 'gravity_theme' ),
		'parent_item_colon'     => __( 'Hoofdgarage:', 'gravity_theme' ),
		'all_items'             => __( 'Alle Pagina\'s', 'gravity_theme' ),
		'add_new_item'          => __( 'Template toevoegen', 'gravity_theme' ),
		'add_new'               => __( 'Template toevoegen', 'gravity_theme' ),
		'new_item'              => __( 'Template toevoegen', 'gravity_theme' ),
		'edit_item'             => __( 'Template wijzigen', 'gravity_theme' ),
		'update_item'           => __( 'Update Template', 'gravity_theme' ),
		'view_item'             => __( 'Template bekijken', 'gravity_theme' ),
		'view_items'            => __( 'Template\'s bekijken', 'gravity_theme' ),
		'search_items'          => __( 'Template zoeken', 'gravity_theme' ),
		'not_found'             => __( 'Geen resultaat', 'gravity_theme' ),
		'not_found_in_trash'    => __( 'Geen resultaat in de prullenbak', 'gravity_theme' ),
		'featured_image'        => __( 'Uitgelichte afbeelding', 'gravity_theme' ),
		'set_featured_image'    => __( 'Uitgelichte afbeelding kiezen', 'gravity_theme' ),
		'remove_featured_image' => __( 'Uitgelichte afbeelding verwijderen', 'gravity_theme' ),
		'use_featured_image'    => __( 'Uitgelichte afbeelding kiezen', 'gravity_theme' ),
		'insert_into_item'      => __( 'Invoegen in item', 'gravity_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded naar dit item', 'gravity_theme' ),
		'items_list'            => __( 'Itemlijst', 'gravity_theme' ),
		'items_list_navigation' => __( 'Itemlijst navigatie', 'gravity_theme' ),
		'filter_items_list'     => __( 'Filter itemlijst', 'gravity_theme' ),
	);
	$args   = array(
		'label'                 => __( 'Garage Templates', 'gravity_theme' ),
		'description'           => __( 'Post Type Description', 'gravity_theme' ),
		'labels'                => $labels,
		'supports'              => array(
			'title',
			'editor',
			'thumbnail',
			'revisions',
			'custom-fields',
			'page-attributes',
			'author',
			'excerpt'
		),
		'hierarchical'          => true,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'show_in_rest'          => true,
		'rest_base'             => 'garage_template',
		'rest_controller_class' => 'WP_REST_Garages_Controller',
		'capability_type'       => 'page',
	);
	register_post_type( 'garage_template', $args );

}

add_action( 'init', 'garage_template_post_type', 0 );


// Register Custom Post Type
function garage_post_type() {

	$labels       = array(
		'name'                  => _x( 'Garages', 'Post Type General Name', 'gravity_theme' ),
		'singular_name'         => _x( 'Garage', 'Post Type Singular Name', 'gravity_theme' ),
		'menu_name'             => __( 'Garages', 'gravity_theme' ),
		'name_admin_bar'        => __( 'Garage pagina', 'gravity_theme' ),
		'archives'              => __( 'Garage overzicht', 'gravity_theme' ),
		'attributes'            => __( 'Garage attributes', 'gravity_theme' ),
		'parent_item_colon'     => __( 'Hoofdgarage:', 'gravity_theme' ),
		'all_items'             => __( 'Alle Pagina\'s', 'gravity_theme' ),
		'add_new_item'          => __( 'Pagina toevoegen', 'gravity_theme' ),
		'add_new'               => __( 'Pagina toevoegen', 'gravity_theme' ),
		'new_item'              => __( 'Pagina toevoegen', 'gravity_theme' ),
		'edit_item'             => __( 'Pagina wijzigen', 'gravity_theme' ),
		'update_item'           => __( 'Update Pagina', 'gravity_theme' ),
		'view_item'             => __( 'Pagina bekijken', 'gravity_theme' ),
		'view_items'            => __( 'Pagina\'s bekijken', 'gravity_theme' ),
		'search_items'          => __( 'Garage zoeken', 'gravity_theme' ),
		'not_found'             => __( 'Geen resultaat', 'gravity_theme' ),
		'not_found_in_trash'    => __( 'Geen resultaat in de prullenbak', 'gravity_theme' ),
		'featured_image'        => __( 'Uitgelichte afbeelding', 'gravity_theme' ),
		'set_featured_image'    => __( 'Uitgelichte afbeelding kiezen', 'gravity_theme' ),
		'remove_featured_image' => __( 'Uitgelichte afbeelding verwijderen', 'gravity_theme' ),
		'use_featured_image'    => __( 'Uitgelichte afbeelding kiezen', 'gravity_theme' ),
		'insert_into_item'      => __( 'Invoegen in item', 'gravity_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded naar dit item', 'gravity_theme' ),
		'items_list'            => __( 'Itemlijst', 'gravity_theme' ),
		'items_list_navigation' => __( 'Itemlijst navigatie', 'gravity_theme' ),
		'filter_items_list'     => __( 'Filter itemlijst', 'gravity_theme' ),
	);
	$rewrite      = array(
		'slug'       => 'vestiging',
		'with_front' => false,
		'pages'      => true,
		'feeds'      => true,
	);
	$capabilities = array(
		'read_post'          => 'read_garage',
		'delete_post'        => 'delete_garage',
		'delete_posts'       => 'delete_garages',
		'edit_post'          => 'edit_garage',
		'edit_posts'         => 'edit_garages',
		'edit_others_posts'  => 'edit_others_garages',
		'publish_posts'      => 'publish_garages',
		'read_private_posts' => 'read_private_garages',
	);
	$args         = array(
		'label'                 => __( 'Garage', 'gravity_theme' ),
		'description'           => __( 'Post Type Description', 'gravity_theme' ),
		'labels'                => $labels,
		'supports'              => array(
			'title',
			'editor',
			'thumbnail',
			'revisions',
			'custom-fields',
			'page-attributes',
			'author',
			'excerpt'
		),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'vestigingen',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'show_in_rest'          => true,
		'rest_base'             => 'garage',
		'rest_controller_class' => 'WP_REST_Garages_Controller',
		'capability_type'       => 'page',
		'capabilities'          => $capabilities,
	);
	register_post_type( 'garage', $args );

}

add_action( 'init', 'garage_post_type', 0 );


//grant access for garagist roll
function grant_garage_access() {
	$role         = get_role( 'garagist' );
	$capabilities = array(
		//--- post caps:
		//'delete_posts',
		//'delete_published_posts',
		//'edit_posts',
		//'edit_published_posts',
		//'publish_posts',
		'upload_files',
		'read',

		//--- garage caps:
		'read_garage',
		'edit_garage',
		'read_private_garages',
		'edit_garages',
		'level_1',// fix for dropdown bug
		//'edit_others_garages',
		//'delete_garage',
		//'publish_garages',
	);
	foreach ( $capabilities as $cap ) {
		$role->add_cap( $cap );
	}
}

// Tie into the 'after_switch_theme' hook
add_action( 'after_switch_theme', 'grant_garage_access' );
add_action( 'admin_init', 'grant_garage_access' );


// show content from only the current user
function posts_for_current_author( $query ) {
	global $pagenow;

	if ( 'edit.php' != $pagenow || ! $query->is_admin ) {
		return $query;
	}

	if ( ! current_user_can( 'edit_others_posts' ) ) {
		global $user_ID;
		$query->set( 'author', $user_ID );
	}

	return $query;
}

add_filter( 'pre_get_posts', 'posts_for_current_author' );


//only show own content when creating a wp link
function my_wp_link_query_args( $args ) {
	if ( ! current_user_can( 'edit_others_posts' ) ) {
		$user_id        = get_current_user_id();
		$args['author'] = $user_id;
	}

	return $args;
}

add_filter( 'wp_link_query_args', 'my_wp_link_query_args' );


// create the user roll garagist
add_action( 'after_switch_theme', 'create_garagist_role' );
function create_garagist_role() {
	add_role(
		'garagist', __( 'Garagist' ),
		array(
			'read' => true,  // true allows this capability
		)
	);
}


//of als er in de admin url een GET zit
if ( isset( $_GET['create_garagist_role'] ) ) {
	remove_role( 'garagist' );
	create_garagist_role();
	print_r( get_role( 'garagist' ) );
}


// email naar partspoint als een garage post pending is
add_action( 'transition_post_status', 'pending_post_status', 10, 3 );
function pending_post_status( $new_status, $old_status, $post ) {
	if ( $new_status === "pending" && $post->post_type === 'garage' ) {
		$emails  = "jouwehand@partspoint.com, "; //If you want to send to site administrator, use $emails = get_option('admin_email');
		$title   = wp_strip_all_tags( get_the_title( $post->ID ) );
		$author  = get_the_author_meta( 'display_name', $post->post_author );
		$url     = get_permalink( $post->ID );
		$message = "Link naar bericht: \n{$url}";

		wp_mail( $emails, "Nieuwe Garage pagina: {$title} door {$author}", $message );
	}
}

function shapeSpace_filter_search( $query ) {
	if ( ! $query->is_admin &&
	     $query->is_search &&
	     ! in_array( $query->get( 'post_type' ), [ 'voorraad' ] ) ) {
		$query->set( 'post_type', array( 'post', 'page' ) );
	}

	return $query;
}

add_filter( 'pre_get_posts', 'shapeSpace_filter_search' );


// Filter to fix the Post Author Dropdown
add_filter( 'wp_dropdown_users', 'theme_post_author_override' );
function theme_post_author_override( $output ) {
	global $post, $user_ID;

	// return if this isn't the theme author override dropdown
	if ( ! preg_match( '/post_author_override/', $output ) ) {
		return $output;
	}

	// return if we've already replaced the list (end recursion)
	if ( preg_match( '/post_author_override_replaced/', $output ) ) {
		return $output;
	}

	// replacement call to wp_dropdown_users
	$output = wp_dropdown_users( array(
		'echo'             => 0,
		'name'             => 'post_author_override_replaced',
		'selected'         => empty( $post->ID ) ? $user_ID : $post->post_author,
		'include_selected' => true
	) );

	// put the original name back
	$output = preg_replace( '/post_author_override_replaced/', 'post_author_override', $output );

	return $output;
}


// setup a function to check if these pages exist
function the_slug_exists( $post_name ) {
	global $wpdb;
	if ( $wpdb->get_row( "SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A' ) ) {
		return true;
	} else {
		return false;
	}
}


if ( isset( $_GET['create_garagist_pages'] ) ) {
	//for testing purpoes
	gravity_create_default_garage_pages( 53 );
}


//setup default pages on user registration
add_action( 'user_register', 'gravity_create_default_garage_pages', 10, 1 );
function gravity_create_default_garage_pages( $user_id ) {

	$user_info = get_userdata( $user_id );

	if ( ! is_user_in_role( $user_id, 'garagist' ) ) {
		return;
	}

	$home_page_title = $user_info->display_name;
	$home_page_slug  = sanitize_title( $home_page_title );

	$data['home_page_title'] = $home_page_title;
	$data['home_page_slug']  = $home_page_slug;
	$data['user_id']         = $user_id;

	$global_pages = get_default_pages( $data );

	if ( ! the_slug_exists( $home_page_slug ) ) {
		foreach ( $global_pages as $menu_order => $properties ) {
			insert_page( $menu_order, $properties );
		}
	}
}

//create default pages on user registration
function insert_page( $page_order, Array $properties, $post_parent = 0 ) {
	$ancestors      = get_post_ancestors( $post_parent );
	$garage_home_id = ! empty( $ancestors ) ? end( $ancestors ) : $post_parent;

	if ( isset( $properties['post_author'] ) ) {
		update_user_meta( $properties['post_author'], 'homepage_id', $garage_home_id );
	}

	$post_author_id = get_post_field( 'post_author', $garage_home_id );

	$data = array(
		'post_title'    => $properties['post_title'] ?? 'Geen title',
		'post_name'     => $properties['post_name'] ?? '',
		'post_content'  => $properties['content'] ?? 'Deze pagina moet nog gevuld worden.',
		'post_author'   => $properties['post_author'] ?? $post_author_id,
		'page_template' => $properties['page_template'] ?? '',
		'menu_order'    => $properties['menu_order'],
		'post_status'   => 'draft',
		'post_type'     => 'garage',
		'post_parent'   => $post_parent,
	);
	$id   = wp_insert_post( add_magic_quotes( $data ) );

	$custom_fields = get_post_custom( $properties['template_post_id'] );
	foreach ( $custom_fields as $key => $value ) {
		add_post_meta( $id, $key, $value[0] );
	}


	if ( ! empty ( $properties['children'] ) ) {
		foreach ( $properties['children'] as $child_title => $child_properties ) {
			insert_page( $child_title, $child_properties, $id );
		}
	}
}

function buildTree( array $elements, $parentId = 0 ) {
	$branch = array();

	foreach ( $elements as $element ) {
		if ( $element['post_parent'] == $parentId ) {
			$children = buildTree( $elements, $element['template_post_id'] );
			if ( $children ) {
				$element['children'] = $children;
			}
			$branch[] = $element;
		}
	}

	return $branch;
}

//Get default pages
function get_default_pages( $data ) {
	$wp_query = new WP_Query();
	$posts    = $wp_query->query( [
		'post_type'      => 'garage_template',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC'
	] );

	$pages = array_map(
		function ( $post ) {
			return (array) $post;
		},
		$posts
	);

	foreach ( $pages as $pageKey => $page ) {
		$page = [
			'post_title'       => $page['post_title'],
			'post_parent'      => $page['post_parent'],
			'post_name'        => $page['post_name'],
			'content'          => $page['post_content'],
			'post_author'      => $data['user_id'],
			'template_post_id' => $page['ID'],
			'menu_order'       => $page['menu_order'],
			'page_template'    => ( get_page_template_slug( $page['ID'] ) ?: 'default' ),
		];

		$pages[ $pageKey ] = $page;
	}

	$pages[0]['post_title']  = $data['home_page_title'];
	$pages[0]['post_name']   = $data['home_page_slug'];
	$pages[0]['post_author'] = $data['user_id'];

	return buildTree( $pages );
}


// On update of a users display_name change their homepage title and slug
add_action( 'profile_update', 'my_profile_update', 10, 2 );
function my_profile_update( $user_id, $old_user_data ) {
	$user_info       = get_userdata( $user_id );
	$user_meta       = get_user_meta( $user_id );
	$home_page_title = $user_info->display_name;
	$home_page_slug  = sanitize_title( $home_page_title );

	// Set the title and slug of garage
	$home_page_args = array(
		'ID'         => $user_meta['homepage_id'][0],
		'post_title' => $home_page_title,
		'post_name'  => $home_page_slug,
	);
	// Update homepage of the garage
	wp_update_post( $home_page_args );

}

// save the latlng data in metafields so the wp_query can use them
add_action( 'acf/save_post', 'set_latlng_fields', 20 );
function set_latlng_fields( $post_id ) {
	//update the geo location
	$location = get_field( 'business_location', $post_id );
	if ( ! empty( $location ) ) {
		update_post_meta( $post_id, '_latitude', $location['lat'] );
		update_post_meta( $post_id, '_longitude', $location['lng'] );
	}
}


// Only allow user with rol garagist and up to move pages as subpage of a different user
add_filter( 'page_attributes_dropdown_pages_args', 'filter_dropdown_so_14880043', 10, 2 );
function filter_dropdown_so_14880043( $dropdown_args, $post ) {
	if ( ! current_user_can( 'edit_others_posts' ) ) {
		//print_r($dropdown_args);
		global $user_ID;
		$dropdown_args = array(
			'post_type'        => $post->post_type,
			'exclude_tree'     => $post->ID,
			'selected'         => $post->post_parent,
			'name'             => 'parent_id',
			'show_option_none' => '',
			'echo'             => 0,
			'authors'          => $user_ID, //not official, but it works!
		);
	}

	return $dropdown_args;
}


// Show ONLY top level pages in archive loops of the garage postype
add_filter( 'pre_get_posts', 'garage_pre_get_posts' );
function garage_pre_get_posts( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {

		//check if the query matches this post type
		if ( is_post_type_archive( 'garage' ) ) {
			$query->set( 'post_parent', 0 ); //only show parent posts
			$query->set( 'posts_per_page', - 1 ); //Show all

			$current_location = get_flow_info( 'current-location' ); // lat lng
			if ( isset( $current_location['lat'] ) && $current_location['lng'] ) {
				$latlng = get_flow_info( 'current-location' );
				$query->set( 'geo_query', array(
					'lat_field' => '_latitude',
					// this is the name of the meta field storing latitude
					'lng_field' => '_longitude',
					// this is the name of the meta field storing longitude
					'latitude'  => $current_location['lat'],
					// this is the latitude of the point we are getting distance from
					'longitude' => $current_location['lng'],
					// this is the longitude of the point we are getting distance from
					'distance'  => 200,
					// this is the maximum distance to search
					'units'     => 'km'
					// this supports options: miles, mi, kilometers, km
				) );
				$query->set( 'orderby', 'distance' ); // this tells WP Query to sort by distance
				$query->set( 'order', 'ASC' );
			}
		}
	}
}


// filter post in the backend by user with the roll garagist
function admin_filter_by_garage() {
	if ( current_user_can( 'edit_others_posts' ) ) {
		$params = array(
			'name'            => 'author', // this is the "name" attribute for filter <select>
			'show_option_all' => 'Alle Garages', // label for all authors (display posts without filter)
			'role'            => 'garagist'
		);

		if ( isset( $_GET['user'] ) ) {
			$params['selected'] = $_GET['user'];
		} // choose selected user by $_GET variable

		wp_dropdown_users( $params ); // print the ready author list
	}
}

add_action( 'restrict_manage_posts', 'admin_filter_by_garage' );


// Make child pages inherit the template of the parent page on the front-end
//add_filter( 'template_include', 'portfolio_page_template', 99 );
function portfolio_page_template( $template ) {
	global $post;
	$post_id     = $post->ID;
	$ancestors   = $post->ancestors;
	$post_parent = $post->post_parent;
	// Checks if current post type is a page or garage, rather than a post
	if ( 'garage' == get_post_type() ) {
		// Checks if page is parent, if so, return same template
		if ( $post->post_parent === 0 ) {
			//print_r('use set template');
			return $template; //do nothing
			//Checks if post_parent is not toplevel
		} elseif ( $post_parent !== $post_id && $post_parent !== end( $ancestors ) ) {
			//print_r('use parent template');
			$parent_page_template = get_post_meta( $post_parent, '_wp_page_template', true );
			$new_template         = locate_template( array( $parent_page_template ) );
			if ( ! empty( $new_template ) ) {
				return $new_template;
			} else {
				return $template; //do nothing
			}
		}
	}

	return $template; //do nothing
}


//Extend the custom excerpt just for Autovakmeester
function wpse_custom_wp_trim_excerpt( $custom_excerpt ) {

	$permalink                     = get_permalink();
	$GRAVITY_CUSTOM_EXCERPT_LENGTH = 200;

	if ( ! empty( $custom_excerpt ) ) { // is the REAL excerpt field not the_content()
		$raw_excerpt = $custom_excerpt;
	} else {
		$raw_excerpt = get_the_content();
	}

	$custom_excerpt = preg_replace( " (\[.*?\])", '', $raw_excerpt );
	$custom_excerpt = strip_shortcodes( $custom_excerpt );
	$custom_excerpt = apply_filters( 'the_content', $custom_excerpt );

	if ( strlen( $custom_excerpt ) > $GRAVITY_CUSTOM_EXCERPT_LENGTH ) {
		$custom_excerpt = substr( $custom_excerpt, 0, $GRAVITY_CUSTOM_EXCERPT_LENGTH );
		$custom_excerpt = substr( $custom_excerpt, 0, strripos( $custom_excerpt, " " ) );// end op heel woord
		$custom_excerpt = trim( preg_replace( '/\s+/', ' ', $custom_excerpt ) );// remove trailing whitespace
		$custom_excerpt = $custom_excerpt . '... <a href="' . $permalink . '" class="btn btn--readmore">Lees meer</a>';
	}

	return $custom_excerpt;

}

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' ); // stop de default trimming from happening
add_filter( 'get_the_excerpt', 'wpse_custom_wp_trim_excerpt' );

//Show only attachments from user AND for that Garage
function users_own_attachments( $query ) {

	global $current_user, $pagenow;

	$is_attachment_request = ( $query->get( 'post_type' ) == 'attachment' );

	if ( ! $is_attachment_request ) {
		return;
	} else {
		if ( ! defined( 'DOING_CRON' ) && is_admin() && ! isset( $_POST['clear_images'] ) ) {
			$query->set( 'meta_query', [
				[
					'key'     => 'HexonCode',
					'compare' => 'NOT EXISTS',
				]
			] );
		}
	}

	if ( ! is_a( $current_user, 'WP_User' ) ) {
		return;
	}

	if ( ! in_array( $pagenow, array( 'upload.php', 'admin-ajax.php' ) ) ) {
		return;
	}

	if ( ! current_user_can( 'edit_others_posts' ) ) {
		$query->set( 'author', $current_user->ID );
	}

	return;
}

add_action( 'pre_get_posts', 'users_own_attachments' );

// Wordpress entrypoint for the parser.
function parser_cron() {
    error_log('Running cron');
	$pending   = get_stylesheet_directory() . '/includes/gravity-hexon/items/pending';
	$done      = get_stylesheet_directory() . '/includes/gravity-hexon/items/done';
	$error      = get_stylesheet_directory() . '/includes/gravity-hexon/items/error';
	$parser    = new HexonParser( 'v2', $output );
	$files     = array_diff( scandir( $pending ), [ '.', '..', '.gitkeep' ] );

	// This is the "Lock variable. Makes sure that the cron doesn't run twice?"
    // Also breaks the application.
	$is_active = get_option( 'hexon_parser' );

	$limit = 0;
	foreach ( $files as $file ) {
		$car = new \SimpleXMLElement( preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', file_get_contents( $pending . '/' . $file ) ) );
		if ($parser->process_car($car, 'voorraad', $file)) {
			rename( $pending . '/' . $file, $done . '/' . $file );
		} else {
			rename( $pending . '/' . $file, $error . '/' . $file );
		}
		
		$limit = $limit + 1;
		
		if ($limit == 5) {
			break;
		}
	}

	if ( function_exists( 'rocket_clean_domain' ) ) {
		rocket_clean_domain();
	}


	//TODO: This doesn't happen when an image get fails?
//	delete_option( 'hexon_parser' );
	return 0;
}

add_action( 'hexon_parser_cron', 'parser_cron' );




// functions.php
// prioritetize pagination over displaying custom post type content
add_action('init', function() {
//    add_rewrite_rule('(.?.+?occasions\/zoeken)\/page\/?([0-9]{1,})\/?.*$', 'index.php?pagename=$matches[1]&paged=$matches[2]&car_search=page%2F2&hexon_search=true&car_post_type=voorraad', 'top');
//    add_rewrite_rule('(.?.+?occasions)\/page\/?([0-9]{1,})\/?.*$', 'index.php?pagename=$matches[1]&paged=$matches[2]&car_search=page%2F2&hexon_search=true&car_post_type=voorraad', 'top');
//    add_rewrite_rule( '.?.+?occasions([^&]+)\/page\/([0-9]{1,})\/?.*$', 'index.php?car_search=$matches[1]&paged=$matches[2]&hexon_search=true&car_post_type=voorraad', 'top' );



    if ( get_option( 'hexon_slug' ) != '' ) {
        $slug = get_option( 'hexon_slug' );
    } else {
        $slug = 'voorraad';
    }

    //Rewrite rule for single occasions page
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/auto/([^&]+)/?$', 'index.php?garage_slug=$matches[1]&car_slug=$matches[2]&hexon_search=false&car_post_type=voorraad', 'top' );
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/([^&]+)/page/([0-9]{1,})/?$', 'index.php?garage_slug=$matches[1]&car_search=$matches[2]&paged=$matches[3]&hexon_search=true&car_post_type=voorraad', 'top' );
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/([^&]+)/?$', 'index.php?garage_slug=$matches[1]&car_search=$matches[2]&hexon_search=true&car_post_type=voorraad', 'top', 1 );
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/?$', 'index.php?garage_slug=$matches[1]&hexon_search=true&car_post_type=voorraad&car_post_request=true&hexon=true', 'top', 1 );
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/page/([0-9]{1,})/?$', 'index.php?garage_slug=$matches[1]&paged=$matches[2]&hexon_search=false&car_post_type=voorraad', 'top' );
    add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/?$', 'index.php?garage_slug=$matches[1]&hexon_search=false&car_post_type=voorraad', 'top' );






    //Rewrite rule for global occasions page
    add_rewrite_rule( $slug . '/zoeken\/([^&]*)\/?page\/([0-9]{1,})\/?$', 'index.php?car_search=$matches[1]&paged=$matches[2]&hexon_search=true&car_post_type=voorraad', 'top' );
    add_rewrite_rule( $slug . '/zoeken/([^&]+)/?$', 'index.php?car_search=$matches[1]&hexon_search=true&car_post_type=voorraad', 'top' );
    add_rewrite_rule( $slug . '/zoeken/?$', 'index.php?hexon_search=true&car_post_type=voorraad&car_post_request=true&hexon=true', 'top' );


}, 1);



