<?php

function load_after_parent_option_pages_are_set() {
  // Options
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_sub_page( [
      'parent_slug' => 'gravitycore',
      'page_title'  => 'Thema instellingen',
      'menu_title'  => 'Thema',
      'menu_slug'   => 'theme-general-settings',
      'position'    => false,
      'icon_url'    => 'dashicons-art',
      'redirect'    => false
    ] );

    acf_add_options_sub_page( [
      'parent_slug' => 'gravitycore',
      'page_title'  => __( 'Klanten Vertellen' ),
      'menu_title'  => __( 'Klanten Vertellen' ),
      'menu_slug'   => 'options-theme-klantenvertellen',
      'capability'  => 'edit_posts',
    ] );

    acf_add_options_sub_page( [
      'parent_slug' => 'gravitycore',
      'page_title'  => __( 'Popup' ),
      'menu_title'  => __( 'Popup' ),
      'menu_slug'   => 'options-theme-popup',
      'capability'  => 'edit_posts',
    ] );

  }
}
add_action( 'init', 'load_after_parent_option_pages_are_set', 15 );


function acf_set_google_api_key() {
  acf_update_setting('google_api_key', 'AIzaSyDQTKWoMScsLucFnTZzKE6-ersHE3a3TMw');
}
add_action('acf/init', 'acf_set_google_api_key');
add_filter('acf/settings/remove_wp_meta_box', '__return_false');



// ACF show content from only the current user
add_filter( 'acf/fields/post_object/query', 'acf_limit_to_own_content', 10, 3 );
add_filter( 'acf/fields/relationship/query', 'acf_limit_to_own_content', 10, 3 );
add_filter( 'acf/fields/page_link/query', 'acf_limit_to_own_content', 10, 3 );

// filter for a specific field based on it's name
add_filter( 'acf/fields/page_link/query/name=link', 'garage_features_query', 10, 3 );
add_filter( 'acf/fields/page_link/query/name=occasions_link', 'garage_features_query', 10, 3 );



function acf_limit_to_own_content( $args, $field, $post_id ) {

  if ( ! current_user_can( 'edit_others_posts' ) ) {
    $user_id        = get_current_user_id();
    $args['author'] = $user_id;
  }

  return $args;
}

//Pre filter the garage features by post parent
function garage_features_query( $args, $field, $post_id ) {

  $wp_query = new WP_Query();
  $pages    = $wp_query->query( [ 'post_type' => 'garage', 'posts_per_page' => '-1' ] );
  $children = get_page_children( $post_id, $pages );

  $pagesArray = [];
  foreach ( $children as $child ) {
    $pagesArray[] = $child->ID;
  }

  $args['post__in'] = $pagesArray;

  return $args;

}