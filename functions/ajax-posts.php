<?php
function load_ajax_posts_scripts() {

  // make sure you have $wp_query
  global $wp_query;

  // register our main script but do not enqueue it yet
  wp_register_script( 'load_ajax_posts', get_stylesheet_directory_uri() . '/assets/js/ajax-posts.js', array('jquery') );

  // Get current paged value, for pagination
  if ( get_query_var('paged') ) {
     $paged = get_query_var('paged');
  } else if ( get_query_var('page') ) {
     $paged = get_query_var('page');
  } else {
     $paged = 1;
  }

  // now the most interesting part
  // we have to pass parameters to ajax-posts.js script but we can get the parameters values only in PHP
  // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
  wp_localize_script( 'load_ajax_posts', 'load_ajax_posts_params', array(
    'ajaxurl'       => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
    'wp_query'      => json_encode( $wp_query->query_vars ), // everything about your loop is here
    'current_page'  => $paged,
    'max_page'      => $wp_query->max_num_pages,
    'is_archive'    => is_archive(),
    'search_val'    => get_search_query(),
    'noposts'       => esc_html__('No older posts found', 'gravity_theme'),
    'loadmore'      => esc_html__('Load more', 'gravity_theme')
  ) );

  wp_enqueue_script( 'load_ajax_posts' );
}

add_action( 'wp_enqueue_scripts', 'load_ajax_posts_scripts' );



function load_ajax_posts_handler(){
  $search_val           = $_POST['search_val'] ?? '';
  $latlng               = $_POST['latlng'] ?? '';
  $is_archive           = $_POST['is_archive'] ?? '';
  $args                 = isset($_POST['query']) ? json_decode( stripslashes( $_POST['query'] ), true ) : '';
  $args['paged']        = $_POST['page'] + 1; // we need next page to be loaded
  $is_html = $_POST['is_html'];

  $post_type            = !empty($args['post_type']) ? $args['post_type'] : null;

  if( $search_val ){
    //console_log($_POST['search_val']);
    $args = array(
        'post_status'     => 'publish',
        's'               =>  $search_val
    );
    $post_type = 'search';
  }

  if( $latlng ){
    $args = array(
        'posts_per_page'  => $is_archive ? 100 : 5,
        'post_type'       => 'garage',
        'post_parent'     => 0,
        'geo_query'       => array(
            'lat_field' => '_latitude',           // this is the name of the meta field storing latitude
            'lng_field' => '_longitude',          // this is the name of the meta field storing longitude
            'latitude'  => $latlng[0],            // this is the latitude of the point we are getting distance from
            'longitude' => $latlng[1],            // this is the longitude of the point we are getting distance from
            'distance'  => $is_archive ? 200 : 50,                    // this is the maximum distance to search
            'units'     => 'km'                   // this supports options: miles, mi, kilometers, km
        ),
        'orderby' => 'distance',                  // this tells WP Query to sort by distance
        'order'   => 'ASC',
        'paged' => $_POST['page']
    );
  }
  
  $args['post_status']  = 'publish';

  // it is always better to use WP_Query but not here
  $wp_query = new WP_Query( $args );

  if($is_html) {
    if( $wp_query->have_posts() ) :

      // run the loop
      while( $wp_query->have_posts() ): $wp_query->the_post();

        $post_type = $post_type ?? get_post_type(get_the_ID());

        //console_log($post_type);

        include locate_template( 'template-parts/loop/content-'.$post_type.'.php', false, false );

        // for the test purposes comment the line above and uncomment the below one
        //the_title();

      endwhile;

    endif;

    // Pagination here
    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){
        echo paginate_links(array(
            'base' => get_flow_urls(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $_POST['page'],
            'total' => $total_pages,
						'prev_text'    => __('«'),
						'next_text'    => __('»'),
        ));
    }
  }
  die; // here we exit the script and even no wp_reset_query() required!
}



add_action('wp_ajax_load_ajax_posts_handler',        'load_ajax_posts_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_load_ajax_posts_handler', 'load_ajax_posts_handler'); // wp_ajax_nopriv_{action}