<?php
function load_ajax_rdw_scripts() {

	// register our main script but do not enqueue it yet
	wp_register_script( 'load_ajax_rdw_data', get_stylesheet_directory_uri() . '/assets/js/ajax-rdw.js', array( 'jquery' ) );

	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'load_ajax_rdw_data', 'load_ajax_rdw_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'session' => json_encode( $_SESSION['flow'] ) // everything in the flow session is here
	) );

	wp_enqueue_script( 'load_ajax_rdw_data' );
}

add_action( 'wp_enqueue_scripts', 'load_ajax_rdw_scripts' );


function load_ajax_rdw_handler() {

	// prepare our arguments for the query
	$flow_session = json_decode( stripslashes( $_POST['session'] ), true );
	$vehicle      = json_decode( stripslashes( $_POST['vehicle'] ), true );

	//pre_print_r($flow_session);

	// Pas op de opendata gasten hebben het data format al een keer aangepast en dan breekt de code
	$newDateString = date_format( date_create_from_format( 'Ymd', $vehicle['vehicle']['apkdate'] ), 'Y-m-d' );

	$curentdate = new DateTime();  //current date or any date
	$apkdate    = new DateTime( $newDateString );   //Future date
	$diff       = $apkdate->diff( $curentdate );  //find difference
	$months     = $diff->m + 12 * $diff->y;
	?>


	<?php
	$flow_progress = get_flow_info( 'progress' );
	if ( isset( $flow_progress['intention'] ) && $flow_progress['intention'] == 'appointment' ) {
		$flow_intention_text = 'afspraak maken';
	} else {
		$flow_intention_text = 'offerte opvragen';
	}
	?>

    <div class="license-plate__card" style="">
	<?php if ( $months > 2 ): ?>
        <svg class="license-plate__card-icon license-plate__card-icon--ok">
            <use xlink:href="#icon-ok"/>
        </svg>
	<?php else: ?>
        <svg class="license-plate__card-icon license-plate__card-icon--warning">
            <use xlink:href="#icon-warning"></use>
        </svg>
	<?php endif; ?>
    <div class="license-plate__card-caption">
        <p>
            Op <strong><?php echo date_i18n( 'l j F Y', strtotime( $newDateString ) ); ?></strong> vervalt de apk van uw
            <strong><?php echo strtolower( $vehicle['vehicle']['brand'] . ' ' . $vehicle['vehicle']['type'] ) ?></strong>.<br>
			<?php if ( $months == 0 ) { ?>
                Deze maand moet je gekeurd worden<?php if ( $months <= 2 ): ?>, dus wees er snel bij<?php endif; ?>.
                <br><br>
			<?php } else { ?>
                Je hebt nog <?php printf( _n( '%s maand', '%s maanden', $months, 'text-domain' ), number_format_i18n( $months ) ); ?> om gekeurd te worden<?php if ( $months <= 2 ): ?>, dus wees er snel bij<?php endif; ?>.
                <br><br>
			<?php } ?>
        </p>
        <a href="<?php echo get_flow_urls()[1]; ?>" class="link">Wil je misschien
            een <?php echo $flow_intention_text; ?>?</a>
    </div>
	<?php

	die; // here we exit the script and even no wp_reset_query() required!
}


add_action( 'wp_ajax_load_ajax_rdw_handler', 'load_ajax_rdw_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_load_ajax_rdw_handler', 'load_ajax_rdw_handler' ); // wp_ajax_nopriv_{action}
