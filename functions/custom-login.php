<?php

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (/* !current_user_can('administrator') &&  */!is_admin()) {
      show_admin_bar(false);
    }
}



add_filter('authenticate','custom_authenticate', 31, 3);
function custom_authenticate( $user, $username, $password ) {
    // if logout go home!
    if (isset($_GET['loggedout'])) {
        wp_redirect( home_url() );
    // forcefully capture login failed to forcefully open wp_login_failed action,
    } else if ( empty( $username ) || empty( $password ) ) {
        do_action( 'wp_login_failed', $user );
    }
    return $user;
}


add_action( 'wp_login_failed', 'gravity_login_failed' ); // hook failed login
function gravity_login_failed( $user ) {
    // check what page the login attempt is coming from
    $referrer = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null);
    if( is_wp_error( $user ) ) {
        //echo $user->get_error_message();
        //print_r($user);
    }
    // check that were not on the default login page
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
        // make sure we don't already have a failed login attempt
        if ( !strstr($referrer, '?login=failed' )) {
            $error_types = (!empty($user->errors) ? array_keys($user->errors) : '');
            if (is_array($error_types) && !empty($error_types)) {
                $error_type = $error_types[0];
            }
            // Redirect to the login page and append a querystring of login failed
            wp_redirect( $referrer . '?login=failed&reason=' . (empty($error_type)?'unknown': $error_type) );
            //wp_die('login fail.... je kwam van ' . $referrer);
        } else {
            wp_redirect( $referrer );
        }
        exit;
    }
}