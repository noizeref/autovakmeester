<?php
$my_theme = wp_get_theme();
$child_themeversion = $my_theme->get( 'Version' );

function enqueue_child_scripts() {
    global $child_themeversion;
    wp_register_script( 'child-all-min-js' , get_stylesheet_directory_uri() . '/assets/js/scripts.min.js', '', $child_themeversion, true);
    wp_enqueue_script(  'child-all-min-js');

    wp_register_script( 'youtube-controles-js' , get_stylesheet_directory_uri() . '/assets/js/youtube-controles.js', '', 'null', true);
    wp_enqueue_script(  'youtube-controles-js');

    wp_register_script( 'geolocate-js' , get_stylesheet_directory_uri() . '/assets/js/geolocate.js', '', 'null', true);
    wp_enqueue_script(  'geolocate-js');

    wp_register_style( 'child-all-min-css' , get_stylesheet_directory_uri() . '/assets/css/style.min.css', '', $child_themeversion, 'all');
    wp_enqueue_style(  'child-all-min-css');

    wp_register_style( 'wordpress-child-css' , get_stylesheet_directory_uri() . '/assets/css/wordpress.css', '', $child_themeversion, 'all');
    wp_enqueue_style(  'wordpress-child-css');

    wp_register_script('google-map', "https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTKWoMScsLucFnTZzKE6-ersHE3a3TMw&libraries=places", false, null, false);
    wp_enqueue_script('google-map');
    wp_register_script('garage-map', get_stylesheet_directory_uri() . '/assets/js/map.js', '', 'null', true);
    wp_enqueue_script('garage-map');

//    wp_register_script('address-autocomplete', get_stylesheet_directory_uri() . '/assets/js/address-autocomplete.js', '', 'null', true);
//    wp_enqueue_script('address-autocomplete');

	wp_register_script('picturefill', get_stylesheet_directory_uri() . '/assets/js/picturefill.min.js', '', '', false);
	wp_enqueue_script('picturefill');

}
if (!is_admin()) add_action("wp_enqueue_scripts", "enqueue_child_scripts", 12);
