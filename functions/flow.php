<?php
add_action( 'init', 'start_flow_session', 1 );
function start_flow_session() {
	if ( ! session_id() ) {
		session_start();
		if ( ! isset( $_SESSION['flow'] ) ) {
			$_SESSION['flow'] = array();
		} else {
      if ( $flow_location = get_flow_info( 'location' ) ):
        $homepage_id      = $flow_location['page-id'];
        $garage_location  = get_field( 'business_location', $homepage_id);
        $garage_phone     = get_field( 'garage_phone', $homepage_id );
        $garage_mail      = get_field( 'garage_mail', $homepage_id );

        $_SESSION['flow']['location']['name']     = get_the_title( $flow_location['page-id'] );
        $_SESSION['flow']['location']['address']  = $garage_location['address'];
        $_SESSION['flow']['location']['phone']    = $garage_phone;
        $_SESSION['flow']['location']['email']    = $garage_mail;
      endif;
		}
	}
}

add_action( 'parse_query', 'prefill_flow_session' );
function prefill_flow_session() {
	//catch session vars
	if ( get_query_var( 'location' ) ) {
		$_SESSION['flow']['location']            = array();
		$_SESSION['flow']['location']['page-id'] = get_query_var( 'location' );
		$_SESSION['flow']['progress']['step-2']  = true;
	}
	if ( get_query_var( 'intention' ) ) {
		$_SESSION['flow']['progress']['intention'] = get_query_var( 'intention' );
	}
}

// set new var to be accepted in the query var
function add_query_vars_filter( $vars ) {
	$vars[] = "location";
	$vars[] = "intention";

	return $vars;
}

add_filter( 'query_vars', 'add_query_vars_filter' );

//unset($_SESSION['flow']);

function member_redirects() {
	if ( is_page_template( 'page-flow-3.php' ) && ! isset( get_flow_info( 'location' )['page-id'] ) ) {
		//pre_print_r($_SESSION['flow']);
		wp_redirect( get_flow_urls( 1 ) ); //go back to step 2
		exit;
	}
	if ( is_page_template( 'page-flow-4.php' ) && empty( get_flow_info( 'tasks' ) ) ) {
		wp_redirect( get_flow_urls( 2 ) ); //go back to step 3
		exit;
	}
}

add_action( 'template_redirect', 'member_redirects' );

$form_id_flow = get_field( 'flow_form', 'options' );

// print get_flow_info('licenseplate');
function get_flow_info( $varname, $default = null ) {
	if ( array_key_exists( 'flow', $_SESSION ) ) {
		if ( array_key_exists( $varname, $_SESSION['flow'] ) ) {
			return $_SESSION['flow'][ $varname ];
		}
	}

	return $default;
}


add_action( 'wp_enqueue_scripts', 'load_ajax_flow_scripts' );
function load_ajax_flow_scripts() {
	// register our main script but do not enqueue it yet
	wp_register_script( 'load_ajax_flow_session', get_stylesheet_directory_uri() . '/assets/js/ajax-flow.js', array( 'jquery' ) );

	// now the most interesting part
	// we have to pass parameters to ajax-posts.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'load_ajax_flow_session', 'load_ajax_flow_session_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'session' => json_encode( $_SESSION['flow'] ), // everything about your loop is here
	) );
	wp_enqueue_script( 'load_ajax_flow_session' );
}


add_action( 'wp_ajax_load_ajax_flow_session_handler', 'load_ajax_flow_session_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_load_ajax_flow_session_handler', 'load_ajax_flow_session_handler' ); // wp_ajax_nopriv_{action}
function load_ajax_flow_session_handler() {
	$flow_session = json_decode( stripslashes( $_POST['session'] ), true );
	$flow_array   = json_decode( stripslashes( $_POST['new_data'] ), true );
	//pre_print_r($flow_session);
	//pre_print_r($flow_array);

	$updated_flow_array = array_merge( $flow_session, $flow_array );

	//pre_print_r($updated_flow_array);
	$_SESSION['flow'] = $updated_flow_array;
	die; // here we exit the script
}


// vul het gravity form met data uit de sessie
add_filter( 'gform_field_value', 'populate_fields', 10, 3 ); //TODO: afvangen op flow form //TODO: Zorg dat dat timeslot array ter alle tijden 4 items heeft ivm jquery live vullen
function populate_fields( $value, $field, $name ) {
	$flow_array = $flow_array ?? $_SESSION['flow'];

	$timeslots        = $flow_array['timeslots'] ?? '';
	$empty_timeslots  = array(
		'empty_1' => ' ',
		'empty_2' => ' ',
		'empty_3' => ' ',
		'empty_4' => ' ',
	);
	$application_type = ( empty( $flow_array['timeslots'] ) ) ? 'Offerte aanvraag' : 'Afspraak aanvraag';
	$car_type         = ( isset( $flow_array['vehicle']['brand'] ) ? $flow_array['vehicle']['brand'] . ' ' : null );
	$car_type         .= $flow_array['vehicle']['type'] ?? null;


	$values = array(
		'car-type'         => $car_type,
		'licence-plate'    => $flow_array['vehicle']['licenseplate'] ?? '',
		'apk-date'         => $flow_array['vehicle']['apkdate'] ? date_i18n( 'd-m-Y', strtotime( $flow_array['vehicle']['apkdate'] ) )  : '',
		'tasks'            => $flow_array['tasks'] ?? '',
		'timeslots'        => ! empty( $timeslots ) ? $timeslots : $empty_timeslots,
		'location-name'    => $flow_array['location']['name'] ?? '',
		'location-address' => $flow_array['location']['address'] ?? '',
		'location-email'   => $flow_array['location']['email'] ?? '',
		'application-type' => $application_type ?? ''
	);

	return isset( $values[ $name ] ) ? $values[ $name ] : $value;
}

//Remove the default submit button
add_filter( 'gform_submit_button_' . $form_id_flow, '__return_false' ); //TODO: afvangen op flow form


// grrrr dit werkt nog niet goed, strings gaan goed maar geneste arrays niet
//add_action( 'gform_pre_submission_'.$form_id_flow, 'pre_submission_handler' );
function pre_submission_handler( $form ) {
	//wp_die( pre_print_r($form) );
	$flow_array       = ( isset( $flow_array ) ) ? $flow_array : $_SESSION['flow'];
	$application_type = ( empty( $flow_array['timeslots'] ) ) ? 'Offerte aanvraag' : 'Afspraak aanvraag';

	$values = array(
		'car-type'         => $flow_array['vehicle']['brand'] . ' ' . $flow_array['vehicle']['type'],
		'licence-plate'    => $flow_array['vehicle']['licenseplate'],
		'apk-date'         => $flow_array['vehicle']['apkdate'],
		'tasks'            => $flow_array['tasks'],
		'timeslots'        => $flow_array['timeslots'],
		'location-name'    => $flow_array['location']['name'],
		'location-email'   => $flow_array['location']['email'],
		'application-type' => $application_type
	);

	foreach ( $form['fields'] as &$field ) {
		if ( isset( $field->inputName ) && isset( $values[ $field->inputName ] ) ) {// if the field inputName == to the custom values key
			$_POST[ 'input_' . $field->id ] = $values[ $field->inputName ];
		}
	}
}

