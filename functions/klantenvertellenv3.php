<?php
/*
Plugin Name: Klantenvertellen json API
Plugin URI: https://gravity.nl
Description: Klantenvertellen data ophalen dmv de json API
Version: 3.1
Author: Gravity b.v.
Author URI: https://gravity.nl
*/

function file_get_contents_curl( $url, $token ) {
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_HTTPHEADER => [
            'X-Publication-Api-Token: ' . $token,
        ],
    ]);
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        return $err;
    } else {
        return $response;
    }
}


// Build the klantenvertellen locations array
function get_klantenvertellen_list( $params = null ) {

  //delete_transient('location_array');
  $location_array = get_transient( 'location_array' );
  if ( empty( $location_array ) || array_key_exists( 'error', $location_array ) ) {

    $url            = 'https://www.klantenvertellen.nl/v1/publication/review/locations/latest?dateSince=1910-01-01T08:34:10.531Z';
    $token          = '462a2ffa-2069-4a45-afff-b263f3d6094a';
    $data           = file_get_contents_curl( $url, $token );
    $location_array = json_decode( $data, true );
    //pre_print_r($location_array);
    if ( empty( $location_array ) || array_key_exists( 'error', $location_array ) ) {
      //pre_print_r( $location_array );
      return;
    }
    $location_array_transient = array_merge(["cached" => time()], $location_array);
    set_transient( 'location_array', $location_array_transient, 15 * MINUTE_IN_SECONDS ); // set cache persistent
  }
  return $location_array;
} // end function get_klantenvertellen_list()


// print tabel with locations
function the_klantenvertellen_list( $params = null ) {

    $location_array = get_klantenvertellen_list();
    if ( empty( $location_array ) || array_key_exists( 'errorCode', $location_array ) ) {
      return;
    }

    if(isset($location_array['cached'])){
        $time = round((time() - $location_array['cached']) / 60);
        $time = 15 - $time;
        unset($location_array['cached']);
        
        echo '<p>Er zijn '.count($location_array).' locaties gecached deze data zal over '. $time . ' minuten ververst worden.</p>';
    }


    //pre_print_r($location_array);
    $allowed_keys = array('locationId', 'locationName', 'updatedSince', 'crmId');
    foreach($location_array as $location){
        $locations[] = array_intersect_key($location, array_flip($allowed_keys));
    }

    $tbody = array_reduce($locations, function($a, $b){return $a.="<tr><td>".implode("</td><td>",$b)."</td></tr>";});
    $thead = "<tr><th>" . implode("</th><th>", array_keys($locations[0])) . "</th></tr>";

    echo "
      <style>
      #klantenvertellen-list{
        border-collapse: collapse;
        width: 100%;
      }
      #klantenvertellen-list th, #klantenvertellen-list td {
        padding: 0.5rem;
        text-align: left;
        border: 1px solid #ccc;
      }
      #klantenvertellen-list th{
        background: #49B170;
        color: #fff;
      }
      #klantenvertellen-list tbody tr:nth-child(odd) {
        background: #eee;
      }
      </style>
    ";
    echo "<table id='klantenvertellen-list'>\n$thead\n$tbody\n</table>";


}

// Build the klantenvertellen combined rating array
function get_klantenvertellen_combined( $params = null ) {
  //$rating_array = '';
  $rating_array = get_transient( 'rating_array_combined' );

  // Get json if cache is empty
  if ( empty( $rating_array ) || array_key_exists( 'error', $rating_array ) ) {
    $url          = 'https://www.klantenvertellen.nl/v1/publication/review/external/group/statistics';
    $token        = '462a2ffa-2069-4a45-afff-b263f3d6094a';
    $data         = file_get_contents_curl( $url, $token );
    $rating_array = json_decode( $data, true );
    $rating_array = $rating_array[0];

    if ( empty( $rating_array ) || array_key_exists( 'errorCode', $rating_array ) ) {
      //console_log( $rating_array );
      return;
    }

    //strip 'Group' from key's to match the regular
    foreach($rating_array as $key => $value){
      $newkey = str_replace('Group', '', $key);
      if($newkey != $key){
          $rating_array[$newkey] = $rating_array[$key];
          unset($rating_array[$key]);
      }
    }
    //console_log( $rating_array );
    set_transient( 'rating_array_combined', $rating_array, 30 * MINUTE_IN_SECONDS ); // set cache persistent
  }

  return $rating_array;
} // end function get_klantenvertellen_combined()


// Build the klantenvertellen rating array
function get_klantenvertellen( $params ) {
  if ( is_array( $params ) ) {
    $locationId = $params[ 'clientid' ];
  } else {
    $locationId = $params; //1049230
  }
  if ( ! isset( $locationId ) ) {
    return;
  }

  //$rating_array = '';
  $rating_array = get_transient( 'rating_array_' . $locationId );

  // Get json if cache is empty
  if ( empty( $rating_array ) || array_key_exists( 'error', $rating_array ) ) {
    $url          = 'https://www.klantenvertellen.nl/v1/publication/review/external/location/statistics?locationId='.$locationId;
    $token        = '462a2ffa-2069-4a45-afff-b263f3d6094a';
    $data         = file_get_contents_curl( $url, $token );
    $rating_array = json_decode( $data, true );
    //pre_print_r($rating_array);
    if ( empty( $rating_array ) || array_key_exists( 'errorCode', $rating_array ) ) {
      //console_log( $rating_array );
      return;
    }
    //console_log( $rating_array );
    set_transient( 'rating_array_' . $locationId, $rating_array, 30 * MINUTE_IN_SECONDS ); // set cache persistent
  }
  //pre_print_r($rating_array);

  return $rating_array;
} // end function get_klantenvertellen()


function the_klantenvertellen_total_score( $params = null ) {
  if(empty($params)){
      $rating_array = get_klantenvertellen_combined();
  } else{
      $rating_array = get_klantenvertellen( $params );
  }
  if ( empty( $rating_array ) ) return;
  $rating_average          = $rating_array[ 'last12MonthAverageRating' ];
  $rating_average_whole    = floor( $rating_average );
  $rating_average_fraction = ltrim( $rating_average - $rating_average_whole, '0' );

  ob_start();
  ?>
    <span><?php echo $rating_average_whole ?></span><sup><?php echo $rating_average_fraction ?></sup>
  <?php
  echo ob_get_clean();

}// end function the_klantenvertellen_total_score();


function the_klantenvertellen( $params = null ) {

  if(empty($params) || !isset($params['clientid'])){
      $rating_array = get_klantenvertellen_combined();
  } else{
      $rating_array = get_klantenvertellen( $params );
  }
  if ( empty( $rating_array ) ) return;
  //pre_print_r($rating_array);

  $rating_name             = $rating_array[ 'locationName'];


  $rating_average          = $rating_array[ 'last12MonthAverageRating' ];
  $rating_question         = $rating_array[ 'locationQuestionAverage' ] ?? null;
  $rating_count            = $rating_array[ 'numberReviews' ] ?? null;
  $rating_link             = $rating_array[ 'viewReviewUrl' ] ?? null;
  $star_rating             = floor( $rating_average / 2 );
  $rating_average_whole    = floor( $rating_average );
  $rating_average_fraction = ltrim( $rating_average - $rating_average_whole, '0' );
  ob_start();
  ?>

    <section class="rating" itemscope itemtype="http://schema.org/WebPage">
        <div class="rating__art-wrapper">
            <div class="rating__pattern"></div>
        </div>
        <div class="container rating__container" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <div class="row rating__row">
                <div class="col-12 col-md-5 col-lg-4 rating__col" data-animate-scroll>
                    <h2 class="rating__title">Beoordelingen <br>van onze klanten</h2>
                    <div class="rating__stars">
                        <?php
                        for ( $i = 0; $i < $star_rating; $i ++ ) {
                          echo( '<svg class="rating__icon active"><use xlink:href="#icon-star" /></svg>' );
                        }
                        ?>
                        <?php
                        for ($i = $i;  $i < 5; $i++ ) {
                          echo( '<svg class="rating__icon"><use xlink:href="#icon-star" /></svg>' );
                        }
                        ?>
                        <?php if(isset($rating_count)): ?>
                            <div class="rating__meta" >(<span itemprop="ratingCount"><?php echo $rating_count ?></span> beoordelingen)</div>
                        <?php endif; ?>
                    </div>
                    <table class="rating__table">
                        <?php
                          if(is_array($rating_question)):
                            $rating_question = array_reverse($rating_question);
                            foreach($rating_question as $question): ?>
                        <tr>
                            <td><?php echo $question['questionTranslations'][0] ?></td>
                            <td><?php echo number_format( $question['averageRating'], 1, '.', '' ); ?></td>
                        </tr>
                        <?php
                            endforeach;
                          endif;
                        ?>
                    </table>
                </div>
                <div class="col-12 col-md-7 col-lg-5 offset-lg-0 rating__col" data-animate-scroll>
                    <div class="rating__image-wrapper">
                        <div class="rating__pointer">
                            <svg class="rating__pointer-svg">
                                <use xlink:href="#icon-pointer"/>
                            </svg>
                        </div>
                        <div class="rating__number-wrapper">
                            <div class="rating__number" itemprop="ratingValue">
                                <span><?php echo $rating_average_whole ?></span><sup><?php echo $rating_average_fraction ?></sup>
                            </div>
                            <span itemprop="bestRating" style="display: none">10</span>
                            <span itemprop="worstRating" style="display: none">1</span>
                            <?php if(isset($rating_link)): ?>
                                <a href="<?php echo( $rating_link ); ?>" target="_blank" class="link rating__link">Bekijk reviews</a>
                            <?php endif; ?>
                        </div>
                        <img class="rating__image"
                             src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/volkswagen.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="rating__logo-wrapper" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-garage.png)">
                        <span class="rating__logo-name"><?php echo str_replace('Autovakmeester', '', $rating_name); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
  <?php
  echo ob_get_clean();
} // end function the_klantenvertellen();







// create custom plugin settings menu
add_action('admin_menu', 'klantenvertellen_backend_menu');
function klantenvertellen_backend_menu() {
	//create new top-level menu
	add_menu_page('Klantenvertellen', 'Klantenvertellen', 'administrator', __FILE__, 'klantenvertellen_backend_page' , get_stylesheet_directory_uri().'/assets/img/klantenvertellen_menu.png' );
}
function klantenvertellen_backend_page() {
?>
  <div class="wrap">
  <h1>Klantenvertellen ID's</h1>
      <?php the_klantenvertellen_list(); ?>
  </div>
<?php }

add_action( 'admin_head', 'adminmenu_icon' );
function adminmenu_icon() {
  ?>
  <style>
    #adminmenu .wp-menu-image img {
        height: 20px;
    }
  </style>
<?php }





// Make it a shortcode!
add_shortcode( 'klantenvertellen', 'the_klantenvertellen' );