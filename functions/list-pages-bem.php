<?php


// extend the wp_list_pages() function to suport BEM
// just add bem=menuname as arg, wp_list_pages( 'bem=navbar&sort_column=menu_order&title_li=&depth=1);


function page_css_class_bem( $css_class, $page, $depth, $args ) {

	if ( isset( $args['bem'] ) ) {
		//pre_print_r($args);
		$css_class[] = $args['bem'] . '__item';
		if ( $args['has_children'] ) {
			$css_class[] = $args['bem'] . '__item--parent';
		}
	}

	return $css_class;
}

add_filter( 'page_css_class', 'page_css_class_bem', 10, 4 );


function page_menu_link_attributes_bem( $atts, $page, $depth, $args ) {

	if ( isset( $args['bem'] ) ) {
		//pre_print_r($args);
		$atts['class'] = $args['bem'] . '__link';
	}

	return $atts;
}

add_filter( 'page_menu_link_attributes', 'page_menu_link_attributes_bem', 10, 4 );


function wp_list_pages_bam( $content ) {
	$content     = preg_replace( '/li class\=\"/', 'li class="side-nav__item ', $content );
	$content_rev = preg_replace( '/\<a /', '<a class="side-nav__link" ', $content );

	return $content_rev;
}
//add_filter('wp_list_pages','wp_list_pages_bam');
