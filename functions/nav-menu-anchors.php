<?php

/**
 * Add the anchor hash to the front-end wp_nav_menu()
*/

add_filter('wp_nav_menu_objects', 'anchor_nav_menu_objects', 10, 2);
function anchor_nav_menu_objects( $items, $args ) {
  //pre_print_r($items);
	// loop
	foreach( $items as &$item ) {
		// append icon
		if( $item->anchor ) {
			$item->url .= '#'.$item->anchor;
		}
	}
	// return
	return $items;
}




/**
 * Add custom anchor field to $item nav object in order to be used in custom Walker
*/
add_filter( 'wp_setup_nav_menu_item', 'gravity_add_anchor_nav_fields' );
function gravity_add_anchor_nav_fields( $menu_item ) {
    $menu_item->anchor = get_post_meta( $menu_item->ID, '_menu_item_anchor', true );
    return $menu_item;
}

/**
 * Save menu custom anchor field
*/
add_action( 'wp_update_nav_menu_item', 'gravity_update_anchor_nav_fields', 10, 3 );
function gravity_update_anchor_nav_fields( $menu_id, $menu_item_db_id, $args ) {
    // Check if element is properly sent
    if ( is_array( $_REQUEST['menu-item-anchor']) ) {
      //pre_print_r($_REQUEST['menu-item-anchor']);
      $anchor_value = $_REQUEST['menu-item-anchor'][$menu_item_db_id];
      update_post_meta( $menu_item_db_id, '_menu_item_anchor', $anchor_value );
    }
}


/**
 * extend the Walker_Nav_Menu_Edit with the HTML for the backend
*/

if ( ! class_exists( 'Walker_Nav_Menu_Edit' ) ) {
	global $wp_version;
	if ( version_compare( $wp_version, '4.4', '>=' ) ) {
		require_once ABSPATH . 'wp-admin/includes/class-walker-nav-menu-edit.php';
	} else {
		require_once ABSPATH . 'wp-admin/includes/nav-menu.php';
	}
}


class Walker_Nav_Menu_Edit_Anchor extends Walker_Nav_Menu_Edit {
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
  		parent::start_el($item_output, $item, $depth, $args);
      //pre_print_r($item_output);

      // Inject $new_fields before: <div class="menu-item-actions description-wide submitbox">
     ob_start();
     $item_id = intval( $item->ID );
      ?>
        <p class="field-custom description description-wide">
            <label for="edit-menu-item-anchor-<?php echo $item_id; ?>">
                <?php _e( 'Anchor #' ); ?><br />
                <input type="text" id="edit-menu-item-anchor-<?php echo $item_id; ?>" class="widefat code edit-menu-item-custom" name="menu-item-anchor[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->anchor ); ?>" />
            </label>
        </p>
      <?php
      $new_fields = ob_get_clean();

  		if ($new_fields) {
        $item_output = preg_replace('/(?=<div[^>]+class="[^"]*submitbox)/', $new_fields, $item_output);
  		}
  		$output .= $item_output;

    }

}
add_filter( 'wp_edit_nav_menu_walker', function() {
    return 'Walker_Nav_Menu_Edit_Anchor';
});