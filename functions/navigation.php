<?php
/*
*   Description: Setting default nav menu's
*   Args: none
*   Expected return:
*   Usage: Filter after_setup_theme
*/
  add_action('after_setup_theme', 'init_navigation_menus');
  function init_navigation_menus() {
      register_nav_menus(array(
          'main-menu'             => 'Hoofd menu',
          'meta-menu'             => 'Meta menu',
          'flow-menu'             => 'Flow stappen menu',
          'footer-menu-1'         => 'footer menu first col',
          'footer-menu-2'         => 'footer menu second col',
          'footer-menu-3'         => 'footer menu third col',
          'footer-meta-menu'      => 'Footer meta menu',
      ));
  }


function atg_menu_classes($classes, $item, $args) {
  if($args->theme_location == 'flow-menu') {
    $classes[] = 'flow__tab';
  }
  return $classes;
}
add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);