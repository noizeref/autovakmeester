<?php

//add_filter( 'wpseo_breadcrumb_links', 'filter_wpseo_breadcrumb_links' );
function filter_wpseo_breadcrumb_links( $links ) {
	//pre_print_r($links);
	//pk_print( sizeof($links) );
/*
	if( sizeof($links) > 1 ){
		array_pop($links);
	}
*/
	return $links;
}


add_filter( 'wpseo_breadcrumb_separator', 'filter_wpseo_breadcrumb_separator', 10, 1 );
function filter_wpseo_breadcrumb_separator( $this_options_breadcrumbs_sep ) {
    return;
}

add_filter( 'wpseo_breadcrumb_output', 'filter_wpseo_breadcrumb_output', 10, 1 );
function filter_wpseo_breadcrumb_output( $wrapper ) {
    $wrapper = str_replace(array('<span>', '</span>'), '', $wrapper);
    return $wrapper;
}

add_filter( 'wpseo_breadcrumb_single_link', 'filter_wpseo_breadcrumb_single_link' , 10, 2);
function filter_wpseo_breadcrumb_single_link( $output, $crumb){
	//pre_print_r($output);
	//pre_print_r($crumb);

	$output = '<li class="breadcrumbs__item"><a href="'. $crumb['url']. '" class="breadcrumbs__link" >';
	$output.= $crumb['text'];
	$output.= '</a></li>';
	return $output;
}
