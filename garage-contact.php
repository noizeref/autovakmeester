<?php
/*
Template Name: Garage Contact
Template Post Type: garage, garage_template
*/
get_header(); ?>

  <?php
    // Header
    get_template_part( 'template-parts/header/garage/header', 'contact' );

    // Faq
    include( STYLESHEETPATH . '/includes/partials/garage/faq.php' );

    // Kickstarter
    //include( STYLESHEETPATH . '/includes/partials/kickstarter.php' );

    // Modal
    include( STYLESHEETPATH . '/parts/garage-modal.php' );
  ?>

<?php get_footer(); ?>