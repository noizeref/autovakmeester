<?php
/*
Template Name: Garage Home
Template Post Type: garage, garage_template
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

  <?php
    // Content
    get_template_part( 'template-parts/page/content', 'page' );

    // Company
    include( STYLESHEETPATH . '/includes/partials/garage/company.php' );

    // Get In Touch
    include( STYLESHEETPATH . '/includes/partials/garage/get-in-touch.php' );

    // Occasions
    include( STYLESHEETPATH . '/includes/partials/garage/occasions.php' );

    // Features
    include( STYLESHEETPATH . '/includes/partials/garage/features.php' );

    // Klantvertellen
    include( STYLESHEETPATH . '/includes/partials/garage/klantvertellen.php' );

    // Modal
    include( STYLESHEETPATH . '/parts/garage-modal.php' );
  ?>

<?php endwhile; ?>

<?php get_footer(); ?>