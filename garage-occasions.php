<?php
/*
Template Name: Garage Occasions
Template Post Type: garage, garage_template
*/
get_header(); ?>

<div class="wrap">
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <h1>Occasions</h1>
      <?php
      while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/page/content', 'page' );

      endwhile; // End of the loop.
      ?>

    </main>
  </div>
</div>

<?php get_footer(); ?>