<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="child-theme no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="child-theme no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="child-theme no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="child-theme no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php wp_title( '|' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/assets/img/favicon.png"
          type="image/x-icon"/>
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
    <meta name="google-site-verification" content="tfwF6YqMN21IRg-lPlXdoir7FXc3BSxc6GpDmHIDLM4" />
    <meta name="google-site-verification" content="C1z_A94GelDr63EVC_-Ou9aGJdS0JKZqeU5-Rasl9_o" />
    <script type="text/javascript">
        var flowUrls = <?php echo json_encode( get_flow_urls() ) ?>;
        var parentTemplateDir = "<?php bloginfo( 'template_directory' ) ?>";
        var childTemplateDir = "<?php bloginfo( 'stylesheet_directory' ) ?>";
    </script>
	<?php wp_head(); ?>
	<?php
	global $wp_query;
	if ( $wp_query->get( 'car_slug' ) ) {
		?>
        <link rel="canonical" href="<?php echo get_the_permalink( $post->ID ); ?>"/>
		<?php
	}
	?>
</head>
<?php
$body_classes = array();
if ( is_post_type_archive( array( 'post' ) ) || is_page_template( array(
		'page-contact.php',
		'page-flow-1.php',
		'page-flow-2.php',
		'page-flow-3.php',
		'page-flow-4.php'
	) ) ) {
	$body_classes[] = 'dark-header';
}
?>
<body <?php body_class( $body_classes ); ?>>
<?php include( STYLESHEETPATH . '/parts/header-html.php' ); ?>
