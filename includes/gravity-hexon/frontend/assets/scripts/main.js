/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

function parseQuery(query) {
    var setValue = function (root, path, value) {
        if (path.length > 1) {
            var dir = path.shift();
            if (typeof root[dir] === 'undefined') {
                root[dir] = path[0] === '' ? [] : {};
            }

            arguments.callee(root[dir], path, value);
        } else {
            if (root instanceof Array) {
                root.push(value);
            } else {
                root[path] = value;
            }
        }
    };
    var nvp = query.split('&');
    var data = {};
    for (var i = 0; i < nvp.length; i++) {
        var pair = nvp[i].split('=');
        var name = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);

        var path = name.match(/(^[^\[]+)(\[.*\]$)?/);
        var first = path[1];
        if (path[2]) {
            //case of 'array[level1]' || 'array[level1][level2]'
            path = path[2].match(/(?=\[(.*)\]$)/)[1].split('][');
        } else {
            //case of 'name'
            path = [];
        }
        path.unshift(first);

        setValue(data, path, value);
    }
    return data;
}

function startSearching($this) {

    var $data = parseQuery($this.serialize());
    var $results = $('.hexon-results');

    $results.addClass('loading');

    $.ajax({
        url: hexon.ajaxURL,
        type: 'POST',
        dataType: 'html',
        data: {
            'action': 'search_query_handler',
            'data': $data,
        },
        success: function (response) {
            var data = JSON.parse(response);
            window.history.pushState(null, null, data.url);
            if (data.results) {
                $results.removeClass('loading');
                $results.html(data.results);
                $('.number-of-objects').html(data.objects);
                $('.occassions__content .content').html(data.term);
            } else {
                $results.html('<div class="no-results">Geen resultaten gevonden</div>');
                $('.number-of-objects').html(0);
            }
        }
    });

    return false;
}

function initFilterVoorraad() {
    console.log("sadasd");
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#hexon-auto-complete');

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function () {

            console.log("Gamer");
            startSearching($('.hexon-filters'));
        }, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    $('.hexon-filters').on('change', function () {
        startSearching($(this));
    });

}

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Hexon = {
        // All pages
        'common': {
            init: function () {
                // JavaScript to be fired on all pages
                initFilterVoorraad();
            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var HEXON = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Hexon;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            HEXON.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                HEXON.fire(classnm);
                HEXON.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            HEXON.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(HEXON.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
