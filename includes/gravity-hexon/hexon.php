<?php

namespace Gravity;

use Gravity\Hexon\Parser\HexonParser;

/**
 * @package Hexon
 * @version 1.0
 */
/*
Plugin Name: Hexon
Plugin URI: http://www.gravity.nl
Description: A wordpress plugin for Hexon
Author: Gravity - Roel Beerens
Version: 1.0
Author URI: http://www.gravity.nl

Copyright 2017 - Gravity (email : roel@gravity.nl)

This program is especially built for our customers and may not be distributed.
If this program is distributed without our approval, you are in violation of our
policy. If this happens, we are forced to take action.

For questions, please contact us at info@gravity.nl or roel@gravity.nl.
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

define( 'HEXON_FOLDER', dirname( __FILE__ ) . '/' );
define( 'HEXON_BASENAME', basename( HEXON_FOLDER ) );
define( 'HEXON_PLUGIN_URL', get_stylesheet_directory_uri() . '/includes/' . basename( __DIR__ ) );
define( 'HEXON_IPS', [ '82.94.237.8', '82.94.240.8', '::1', '127.0.0.1', '172.19.0.1' ] );

class Hexon {

	protected $prefix = 'hexon';

	private $hexon_includes = [
		'vendor/autoload.php',
		'lib/hexon.php',
		'lib/routes.php',
		'lib/post-type.php',
		'lib/taxonomy.php',
		'lib/handlers.php',
		'lib/templates.php',
		'lib/parser.php',
		'lib/api.php',
		'lib/assets.php',
		'lib/car.php',
		'lib/search.php',
		'lib/shortcodes.php',
		'lib/functions.php',
	];

	public function getPrefix() {
		return $this->prefix;
	}

	public function __construct() {
		foreach ( $this->hexon_includes as $file ) {
			require_once plugin_dir_path( __FILE__ ) . $file;
		}
	}
}

// initiate the plugin class
new Hexon();
