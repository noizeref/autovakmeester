<?php

namespace Gravity\Hexon\Api;

use Gravity\Hexon;

class HexonApi extends Hexon {

    public function __construct() {
        add_action( 'template_redirect', [ $this, 'init_hexon_json_api' ] );
        add_action( 'wp_ajax_search_query_handler', [ $this, 'search_query_handler' ] );
        add_action( 'wp_ajax_nopriv_search_query_handler', [ $this, 'search_query_handler' ] );
    }

    public function init_hexon_json_api() {
        global $wp_query;

        $hexon = (boolean) $wp_query->get( 'hexon' );

        if (!$hexon) { return; }

        $car_id           = $wp_query->get( 'car_id' );
        $car_type         = $wp_query->get( 'car_type' );
        $car_search       = $wp_query->get( 'car_search' );
        $hexon_search     = $wp_query->get( 'hexon_search' );
        $post_type        = $wp_query->get( 'car_post_type' );
        $car_post_request = (boolean) $wp_query->get( 'car_post_request' );

        $sort = isset( $_REQUEST['sortering'] ) ? $_REQUEST['sortering'] : '';

        if ( $car_post_request ) {
            $search = new Hexon\Search\HexonSearch();
            $search->get_search_results( $post_type );
        } else if ( $car_id ) {
            $this->get_json_by_car_id( $car_id, $post_type );
        } else if ( $car_type ) {
            $this->get_json_from_all_cars_by_type( $car_type, $post_type );
        } else if ( $hexon_search ) {
            $this->get_json_from_search_cars( $car_search, $sort, $post_type );
        } else if ( $car_search ) {
            $this->get_json_from_all_cars_by_search_query( $car_search, $post_type );
        } else {
            $this->get_json_from_all_cars( $post_type );
        }

    }

    public function get_json_from_all_cars( $post_type ) {
        $car_data = [];

        $args = [
            'post_type'      => $post_type,
            'posts_per_page' => - 1
        ];

        $car_query = new \WP_Query( $args );
        if ( $car_query->have_posts() ) :
            while ( $car_query->have_posts() ) : $car_query->the_post();
                $car_data['results'][] = $this->car_data_builder( get_the_ID() );
            endwhile;
            wp_reset_postdata();
        endif;

        if ( ! empty( $car_data ) ) {
            wp_send_json( $car_data );
        } else {
            wp_send_json( [] );
        }
    }

    public function get_json_from_search_cars( $car_search, $sort, $post_type ) {
        global $wpdb;

        $sortering = null;

        if ( isset( $sort ) ) {
            parse_str( ltrim( $sort, '?' ), $sortering );
        }

        $hexon_customer_id = null;

        global $wp_query;

        $search_cars = explode( "/", $car_search );

        //Set empty tax and meta query car
        $tax_query  = [];
        $meta_query = [];
        $post_in    = [];

        //Set standard meta_query, override later
        $meta_query['prijs'] = [
            'key'     => '_prijs',
            'value'   => - 1,
            'compare' => '>=',
            'type'    => 'NUMERIC'
        ];

        $meta_query['merk'] = [
            'key'     => 'CarSearch',
            'value'   => - 1,
            'compare' => '>=',
        ];

        $meta_query['bouwjaar'] = [
            'key'	  => 'BouwJaar',
            'value'	  => - 1,
            'compare' => '>=',
            'type'	  => 'NUMERIC'
        ];

        //Get search term
        $search_query         = '';
        $search_address_query = '';
        $search_term          = isset($search_cars[0]) ? $search_cars[0] : '';


        $search_address_term  = isset($search_cars[1]) ? $search_cars[1] : '';

        if ( $search_term != 'alles' ) {
            $search_query = str_replace( '+', ' ', $search_term );
        }

        if ( $search_address_term != 'heel-nederland' ) {
            if(empty($search_address_term)){
                $search_address_term = $_POST['hexon_radius']['term'];
            }
            $search_address_query = str_replace( '+', ' ', $search_address_term );
        }

        $search_soorten = [];
        $soorten_term   = 'soort_' . $post_type;
        $soorten        = get_terms( [
            'taxonomy' => $soorten_term
        ] );

        if ( ! isset( $soorten->errors ) ) {
            foreach ( $soorten as $soort ) {
                $search_soorten[] = $soort->slug;
            }
        }

        $search_types = [];
        $types_term   = 'financiering_' . $post_type;
        $types        = get_terms( [
            'taxonomy' => $types_term
        ] );
        if ( ! isset( $types->errors ) ) {
            foreach ( $types as $type ) {
                $search_types[] = $type->slug;
            }
        }

        if ( ! empty( $search_cars ) ) {

            //Combine the array
            $combined_array = $search_cars;
            foreach ( $combined_array as $search_value ) {

                $search_key = '';

                $price_range = false;
                $mileage_range = false;
                $year_range = false;

                if ( ! strpos( $search_value, 'voorraad' ) ) {
                    $price_value = explode( '-', $search_value );
                    if ( is_numeric( $price_value[0] ) && isset( $price_value[1] ) && is_numeric( $price_value[1] ) && strpos($price_value[2], 'deuren') === false ) {
                        $price_range = true;
                    } else if ($price_value[1] == 'km' && $price_value[3] == 'km' ) {
                        $mileage_range = true;
                    } else if ($price_value[1] == 'year' && $price_value[3] == 'year') {
                        $year_range = true;
                    }
                }

                if ( $price_range ) {
                    $price_from = (int) $price_value[0];
                    $price_to   = (int) $price_value[1];
                    $search_key = 'prijs';
                } else if ( $mileage_range ) {
                    $mileage_from = (int) $price_value[0];
                    $mileage_to   = (int) $price_value[2];
                    $search_key = 'kilometerstand';
                } else if ( $year_range ) {
                    $year_from = (int) $price_value[0];
                    $year_to   = (int) $price_value[2];
                    $search_key = 'BouwJaar';
                } else if ( strpos( $search_value, 'km-afstand' ) !== false ) {
                    $search_key = 'straal';
                } else if ( in_array( $search_value, $search_soorten ) ) {
                    $search_key = 'soort';
                } else if ( in_array( $search_value, $search_types ) ) {
                    $search_key = 'financiering';
                } else if ( strpos( $search_value, 'deuren' ) !== false ) {
                    $search_key = 'deuren';
                }

                if ( $search_value ) {

                    if ( $search_key == 'straal' ) {
                        if ( $search_address_term != 'heel-nederland' ) {

                            $search       = new Hexon\Search\HexonSearch();
                            $ids          = $search->get_property_radius_ids( $post_type );
                            $search_value = preg_replace( '/[^\d]+/i', '', $search_value );





                            // Set query var
                            set_query_var( 'hexon_radius', $search_value );

                            // Force Google to search within our beautiful country
                            if ( ! stripos( $search_address_query, 'nederland' ) && ! stripos( $search_address_query, 'holland' ) && ! stripos( $search_address_query, 'netherlands' ) ) {
                                $google_search_query = $search_address_query . ', netherlands';
                            } else {
                                $google_search_query = $search_address_query;
                            }



                            // get address field lat and long
                            $jsonurl  = "https://maps.googleapis.com/maps/api/geocode/json?address="
                                . urlencode( $google_search_query )
                                . "&sensor=false&key=AIzaSyDQTKWoMScsLucFnTZzKE6-ersHE3a3TMw";
                            $response = file_get_contents( $jsonurl );
                            if ( ! empty( $response ) ) {
                                $output = json_decode( $response );
                                error_log(print_r($jsonurl, true));
                                if ( ! empty( $output->results ) ) {
                                    $lat = $output->results[0]->geometry->location->lat;
                                    $lng = $output->results[0]->geometry->location->lng;

                                    $garage_ids = [];
                                    if ( ! empty( $ids ) && is_array( $ids ) ) {
                                        foreach ( $ids as $id ) {
                                            $dist = $search->check_distance( $lat, $lng, $id['lat'], $id['long'] );

                                            if ( ! empty( $dist ) ) {
                                                if ( $dist <= $search_value ) {
                                                    $garage_ids[] = $id['id'];
                                                }
                                            }
                                        }
                                    }

                                    $post_in = empty($garage_ids) ? [0] : $garage_ids;

                                    //Set new search_query
                                    $hexon_radius = $search_address_query;

                                    //Clear the search term because we are searching on radius
                                    $search_address_query = '';

                                }

                            }

                        }

                    } else if ( $search_key == 'soort' || $search_key == 'financiering' ) {

                        $search_key = $search_key . '_' . $post_type;

                        $search_value = explode( ',', $search_value );
                        if ( sizeof( $search_value ) > 1 ) {
                            array_shift( $search_value );
                        }
                        $tax_query[] = [
                            'taxonomy' => $search_key,
                            'field'    => 'slug',
                            'terms'    => $search_value
                        ];

                    } else if ( $search_key == 'prijs' ) {
                        $meta_query[ $search_key ] = [
                            'key'     => '_' . $search_key,
                            'value'   => [ $price_from, $price_to ],
                            'compare' => 'BETWEEN',
                            'type'    => 'NUMERIC'
                        ];
                    } else if ( $search_key == 'BouwJaar') {
                        $meta_query[ $search_key ] = [
                            'key'     => 'BouwJaar',
                            'value'   => [ $year_from, $year_to ],
                            'compare' => 'BETWEEN',
                            'type'    => 'NUMERIC'
                        ];
                    } else if ($search_key == 'kilometerstand') {
                        $meta_query[ $search_key ] = [
                            'key'     => '_' . $search_key,
                            'value'   => [ $mileage_from, $mileage_to ],
                            'compare' => 'BETWEEN',
                            'type'    => 'NUMERIC'
                        ];
                    } else if ($search_key == 'deuren') {
                        $search_value = explode( ',', $search_value );
                        $merk_model   = get_term_by( 'slug', 'aantal-deuren', 'filters_voorraad' );
                        $parents      = [];

                        foreach ( $search_value as $value ) {
                            $term = get_term_by( 'slug', $value, 'filters_voorraad' );
                            if ( $term->parent == $merk_model->term_id ) {
                                $parents[] = $term->term_id;
                            }
                        }

                        foreach ( $search_value as $value ) {
                            $term = get_term_by( 'slug', $value, 'filters_voorraad' );
                            if ( in_array( $term->parent, $parents ) ) {
                                $parent = get_term_by( 'id', $term->parent, 'filters_voorraad' );
                                if ( ( $key = array_search( $parent->slug, $search_value ) ) !== false ) {
                                    unset( $search_value[ $key ] );
                                }
                            }
                        }

                        $tax         = 'filters_' . $post_type;
                        $tax_query[] = [
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $search_value
                        ];

                    } else {
                        $search_value = explode( ',', $search_value );
                        $merk_model   = get_term_by( 'slug', 'merk-model', 'filters_voorraad' );
                        $parents      = [];

                        foreach ( $search_value as $value ) {
                            $term = get_term_by( 'slug', $value, 'filters_voorraad' );
                            if ( $term->parent == $merk_model->term_id ) {
                                $parents[] = $term->term_id;
                            }
                        }

                        foreach ( $search_value as $value ) {
                            $term = get_term_by( 'slug', $value, 'filters_voorraad' );
                            if ( in_array( $term->parent, $parents ) ) {
                                $parent = get_term_by( 'id', $term->parent, 'filters_voorraad' );
                                if ( ( $key = array_search( $parent->slug, $search_value ) ) !== false ) {
                                    unset( $search_value[ $key ] );
                                }
                            }
                        }

                        $tax         = 'filters_' . $post_type;
                        $tax_query[] = [
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $search_value
                        ];
                    }
                }
            }

            if ( sizeof( $meta_query ) > 1 ) {
                $meta_query['relation'] = 'AND';
            }

            if ( sizeof( $tax_query ) > 1 ) {
                $tax_query['relation'] = 'AND';
            }

        }

        $orderby = [
            'merk' => 'ASC'
        ];

        if ( $sortering && isset( $sortering['sortering'] ) ) {
            if ( $sortering['sortering'] == 'prijs-oplopend' ) {
                $orderby = ['prijs' => 'ASC'];
            } else if ( $sortering['sortering'] == 'prijs-aflopend' ) {
                $orderby = ['prijs' => 'DESC'];
            } else if ( $sortering['sortering'] == 'datum-aflopend' ) {
                $orderby = ['date' => 'DESC'];
            } else if ( $sortering['sortering'] == 'datum-oplopend' ) {
                $orderby = ['date' => 'ASC'];
            } else if ( $sortering['sortering'] == 'bouwjaar-oplopend') {
                $orderby = ['BouwJaar' => 'ASC'];
            } else if ( $sortering['sortering'] == 'bouwjaar-aflopend') {
                $orderby = ['BouwJaar' => 'DESC'];
            } else if ( $sortering['sortering'] == 'merken-aflopend') {
                $orderby = ['merk' => 'DESC'];
            } else if ( $sortering['sortering'] == 'merken-oplopend') {
                $orderby = ['merk' => 'DESC'];
            }
        }

        if ( isset( $_POST['garage_slug'] ) && ! empty( $_POST['garage_slug'] ) ) {
            set_query_var( 'garage_slug', $_POST['garage_slug'] );

            $search = new Hexon\Search\HexonSearch();
            $garage = $search->get_page_by_post_name( $_POST['garage_slug'], OBJECT, 'garage' );
            if ( $garage ) {
                $hexon_customer_id = get_field( 'hexon_klantennummer', $garage->ID );
            }

            if ( $hexon_customer_id ) {
                $meta_query['hexon_klantennummer'] = [
                    'key'     => 'Klantnummer',
                    'value'   => $hexon_customer_id,
                    'compare' => '=',
                ];
            }
        }

        $args = [
            'post_type'  => $post_type,
            'tax_query'  => $tax_query,
            'meta_query' => $meta_query,
            'orderby'    => $orderby,
            'post__in'   => $post_in
        ];


        query_posts( $args );

        $car_query                = new \WP_Query( $args );
        $car_data['search_query'] = $car_search . $sort;

        if ( $car_query->have_posts() ) :
            $car_data['results'] = '';
            $car_data['objects'] = $car_query->found_posts;
            $car_data['term']    = '';

            foreach ( $tax_query as $tax ) {
                if ( isset( $tax['taxonomy'] ) && $tax['taxonomy'] == 'financiering_voorraad' ) {
                    $var   = $tax['taxonomy'];
                    $value = $tax['terms'][0];
                    $term  = get_term_by( 'slug', $value, $var );
                    ob_start();
                    $car_data['term'] .= '<h1>' . $term->name . '</h1>' . wpautop( $term->description );
                    $car_data['term'] .= ob_get_clean();
                }
            }

            while ( $car_query->have_posts() ) : $car_query->the_post();
                if ( file_exists( locate_template( 'template-parts/loop/content-occasion.php' ) ) ) {
                    ob_start();

                    $post = $car_query->post;
                    include( locate_template( 'template-parts/loop/content-occasion.php' ) );
                    $car_data['results'] .= ob_get_clean();
                } else {
                    $car_data['results'] .= '<div>' . get_the_title() . '</div>';
                }
            endwhile;
            // Previous/next page navigation.
            if ( file_exists( locate_template( 'template-parts/loop/pagination.php' ) ) ) {
                ob_start();
                include( locate_template( 'template-parts/loop/pagination.php' ) );
                $car_data['results'] .= ob_get_clean();
            } else {
                $car_data['results'] .= get_the_posts_pagination( [
                    'prev_text'          => __( 'Vorige', 'twentyfifteen' ),
                    'next_text'          => __( 'Volgende', 'twentyfifteen' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                ] );
            }
            wp_reset_postdata();
        endif;



        if(!empty(rtrim( $car_search, '/' ))){
            if ( isset( $_POST['garage_slug'] ) && ! empty( $_POST['garage_slug'] ) ) {
                $url = get_bloginfo( 'url' ) . '/vestiging/' . $_POST['garage_slug'] . '/occasions/zoeken/' . rtrim( $car_search, '/' );
            } else {
                $url = get_bloginfo( 'url' ) . '/occasions/zoeken/' . rtrim( $car_search, '/' );
            }
        }
        else{
            if ( isset( $_POST['garage_slug'] ) && ! empty( $_POST['garage_slug'] ) ) {
                $url = get_bloginfo( 'url' ) . '/vestiging/' . $_POST['garage_slug'] . '/occasions';
            } else {
                $url = get_bloginfo( 'url' ) . '/occasions/';
            }
        }


        $car_data['results'] = str_replace( admin_url( 'admin-ajax.php' ), $url, $car_data['results'] );
        $car_data['url']     = $url;

        wp_send_json( $car_data );

    }

    public function get_json_from_all_cars_by_search_query( $search_term = '', $post_type ) {
        $car_data = [];

        if ( $search_term != 'alles' ) {
            $search_query = str_replace( '+', ' ', urldecode( $search_term ) );
        }

        $args = [
            'post_type'      => $post_type,
            'posts_per_page' => - 1,
            'meta_query'     => [
                'relation' => 'OR',
                [
                    'key'     => 'ObjectSearch',
                    'value'   => $search_query,
                    'compare' => 'LIKE',
                ],
            ]
        ];

        $car_query = new \WP_Query( $args );
        if ( $car_query->have_posts() ) :
            while ( $car_query->have_posts() ) : $car_query->the_post();
                $car_data['results'][] = $this->car_data_builder( get_the_ID() );
            endwhile;
            wp_reset_postdata();
        endif;

        $data = empty($car_data) ? [] : $car_data;
        wp_send_json($data);
    }

    public function get_json_by_car_id( $car_id = null, $post_type ) {
        $car_data = [];

        $args = [
            'post_type'      => $post_type,
            'posts_per_page' => 1,
            'meta_query'     => [
                [
                    'key'     => 'HexonCode',
                    'value'   => $car_id,
                    'compare' => '='
                ]
            ]
        ];

        $car_query = new \WP_Query( $args );
        if ( $car_query->have_posts() ) :
            while ( $car_query->have_posts() ) : $car_query->the_post();
                $car_data['results'] = $this->car_data_builder( get_the_ID() );
            endwhile;
            wp_reset_postdata();
        endif;

        $data = empty($car_data) ? [] : $car_data;
        wp_send_json($data);
    }

    public function get_json_from_all_cars_by_type( $car_type = null, $post_type ) {
        $car_data = [];
        $args = ['post_type' => $post_type, 'posts_per_page' => - 1, 'type' => [ $car_type ]];

        $car_query = new \WP_Query( $args );
        if ( $car_query->have_posts() ) :
            while ( $car_query->have_posts() ) : $car_query->the_post();
                $car_data['results'][] = $this->car_data_builder( get_the_ID() );
            endwhile;
            wp_reset_postdata();
        endif;

        $data = empty($car_data) ? [] : $car_data;
        wp_send_json($data);
    }

    public function car_data_builder( $post_id = null ) {

        // Set price in pretty format
        if ( get_post_meta( $post_id, '_prijs', true ) ) {
            $car_price = '€ ' . number_format( get_post_meta( $post_id, '_prijs', true ), 0, ',', '.' ) . ' ,- ';
        } else {
            $car_price = 'N.n.b.';
        }

        $car_data = [
            'id'       => $post_id,
            'car_code' => get_post_meta( $post_id, 'HexonCode', true ),
            'title'    => get_the_title( $post_id ),
            'url'      => get_the_permalink( $post_id ),
            'img'      => get_the_post_thumbnail_url( $post_id, 'large' ),
            'price'    => $car_price
        ];

        //All other post data
        $post_meta = get_post_meta( $post_id );

        foreach ( $post_meta as $meta_key => $meta_value ) {
            if ( $this->json_validate( $meta_value[0] ) ) {
                $car_data[ $meta_key ] = json_decode( $meta_value[0], true );
            } else {
                $car_data[ $meta_key ] = $meta_value[0];
            }
        }

        return $car_data;
    }

    public function search_query_handler() {
        $_POST = $_POST['data'];

        $map       = false;
        $echo      = true;
        $post_type = 'voorraad';

        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;



        if ( isset( $_POST['hexon_set_search'] ) && $_POST['hexon_set_search'] == 1 ) { // Fetch the search $_POST
            $search_query = '';

            if ( isset( $_POST['hexon_search']['term'] ) && ! empty( $_POST['hexon_search']['term'] ) ) {
                $search_term  = $_POST['hexon_search']['term'];
                $search_term  = str_replace( ' ', '+', $search_term );
                $search_query .= str_replace( ',', '', $search_term ) . '/';

            } else {
                $search_query .= 'alles' . '/';
            }

            if ( isset( $_POST['hexon_radius']['term'] ) && ! empty( $_POST['hexon_radius']['term'] ) ) {
                $search_radius_term = $_POST['hexon_radius']['term'];
                $search_radius_term = str_replace( ' ', '+', $search_radius_term );
                $search_query       .= str_replace( ',', '', $search_radius_term ) . '/';
            } else {
                $search_query .= 'heel-nederland' . '/';
            }

            if ( isset( $_POST[ 'hexon_soort_' . $post_type ] ) ) {
                $soorten = $_POST[ 'hexon_soort_' . $post_type ];

                if ( ! empty( $soorten ) ) {
                    if ( is_array( $soorten ) ) {
                        foreach ( $soorten as $type_key => $type_filter ) {
                            if ( is_array( $type_filter ) ) {
                                $filter_terms = implode( ',', $type_filter );
                                $search_query .= $type_key . ',' . $filter_terms . '/';
                            } else {
                                $search_query .= $type_key . '/';
                            }
                        }
                    } else {
                        $search_query .= $soorten . '/';
                    }
                }
            }
            error_log("Soort");
            error_log(print_r($search_query, true));

            if ( isset( $_POST[ 'hexon_financiering_' . $post_type ] ) ) {
                $types = $_POST[ 'hexon_financiering_' . $post_type ];

                if ( ! empty( $types ) ) {
                    if ( is_array( $types ) ) {
                        foreach ( $types as $type_key => $type_filter ) {
                            if ( is_array( $type_filter ) ) {
                                $filter_terms = implode( ',', $type_filter );
                                $search_query .= $type_key . ',' . $filter_terms . '/';
                            } else {
                                $search_query .= $type_key . '/';
                            }
                        }
                    } else {
                        $search_query .= $types . '/';
                    }
                }
            }
            error_log("Financering");
            error_log(print_r($search_query, true));

            if ( isset( $_POST['hexon_price'] ) && ! empty( $_POST['hexon_price'] ) ) {
                if ( is_array( $_POST['hexon_price'] ) ) {
                    $start = ! isset ( $_POST['hexon_price']['prijs_van'] ) ? 'Van' : $_POST['hexon_price']['prijs_van'][0];
                    $end = ! isset ($_POST['hexon_price']['prijs_tot'] ) ? 'Tot' : $_POST['hexon_price']['prijs_tot'][0];
                    $search_query = '';

                    if ($start != 'Van' && $end != 'Tot') {
                        foreach ( $_POST['hexon_price'] as $type_key => $type_filter ) {
                            $search_query .= $type_key == 'prijs_van' ? $type_filter[0] . '-' : $type_filter[0];
                        }
                        $search_query .= '/';
                    }

                } else {
                    $search_query .= $_POST['hexon_price'] . '/';
                }
            }
            error_log("Prijs");
            error_log(print_r($search_query, true));
            if ( isset( $_POST['hexon_output'] ) && ! empty( $_POST['hexon_output'] ) ) {
                if ( is_array( $_POST['hexon_output'] ) ) {
                    $start = ! isset ( $_POST['hexon_output']['vermogen_van'] ) ? 'Van' : $_POST['hexon_output']['vermogen_van'][0];
                    $end = ! isset ($_POST['hexon_output']['vermogen_tot'] ) ? 'Tot' : $_POST['hexon_output']['vermogen_tot'][0];
                    $output_query = '';

                    if ($start != 'Van' && $end != 'Tot') {
                        for ($i = $start; $i <= $end; $i++) {
                            $output_query .= "$i-pk,";
                        }
                    }

                    if (!empty($output_query)) {
                        $search_query .= substr($output_query, 0, -1) . '/';
                    }
                }
            }
            error_log("Output");
            error_log(print_r($search_query, true));
            if ( isset( $_POST['hexon_construction_year'] ) && ! empty( $_POST['hexon_construction_year'] ) ) {
                if ( is_array( $_POST['hexon_construction_year'] ) ) {
                    $start = ! isset ( $_POST['hexon_construction_year']['bouwjaar_van'] ) ? 'Van' : $_POST['hexon_construction_year']['bouwjaar_van'][0];
                    $end = ! isset ($_POST['hexon_construction_year']['bouwjaar_tot'] ) ? 'Tot' : $_POST['hexon_construction_year']['bouwjaar_tot'][0];
                    $output_query = '';

                    if ($start != 'Van' && $end != 'Tot') {
                        $output_query .= "$start-year-$end-year,";
                    }

                    if (!empty($output_query)) {
                        $search_query .= substr($output_query, 0, -1) . '/';
                    }
                }
            }
            error_log("Construct");
            error_log(print_r($search_query, true));
            if ( isset( $_POST['hexon_mileage'] ) && ! empty( $_POST['hexon_mileage'] ) ) {
                if ( is_array( $_POST['hexon_mileage'] ) ) {
                    $start = ! isset ( $_POST['hexon_mileage']['kilometerstand_van'] ) ? 'Van' : $_POST['hexon_mileage']['kilometerstand_van'][0];
                    $end = ! isset ($_POST['hexon_mileage']['kilometerstand_tot'] ) ? 'Tot' : $_POST['hexon_mileage']['kilometerstand_tot'][0];
                    $output_query = '';

                    if ($start != 'Van' && $end != 'Tot') {
                        $output_query .= "$start-$end,";
                    }

                    if (!empty($output_query)) {
                        $search_query .= substr($output_query, 0, -1) . '/';
                    }
                }
            }
            error_log("Mile");
            error_log(print_r($search_query, true));
            if ( isset( $_POST[ 'hexon_filters_' . $post_type ] ) && ! empty( $_POST[ 'hexon_filters_' . $post_type ] ) ) {
                $hexon   = new Hexon\Admin\HexonAdmin();
                $filters = $hexon->get_all_filters();

                $combined_filters = [];
                foreach ( $filters as $index => $key ) {
                    $combined_filters[ $key ] = isset( $_POST[ 'hexon_filters_' . $post_type ][ $key ] ) ? $_POST[ 'hexon_filters_' . $post_type ][ $key ] : null;
                }

                foreach ( $combined_filters as $combined_filter ) {
                    if ( ! empty( $combined_filter ) ) {
                        if ( $combined_filter[0] != '+0 km' ) {
                            $filter_terms = implode( ',', $combined_filter );
                            if (!empty($filter_terms)) {
                                $search_query .= $filter_terms . '/';
                            }
                        }
                    } else {
                        $search_query .= '';
                    }
                }

            }

            if ( isset( $_POST['hexon_radius'] ) && ! empty( $_POST['hexon_radius'] ) ) {
                $hexon   = new Hexon\Admin\HexonAdmin();
                $filters = $hexon->get_all_filters();

                $combined_filters = [];
                foreach ( $filters as $index => $key ) {
                    $combined_filters[ $key ] = isset( $_POST['hexon_radius'][ $key ] ) ? $_POST['hexon_radius'][ $key ] : null;
                }
                foreach ( $combined_filters as $combined_filter ) {
                    if ( ! empty( $combined_filter ) ) {
                        if ( $combined_filter[0] != '+0km-afstand' ) {
                            $filter_terms = implode( ',', $combined_filter );
                            if ($filter_terms != 'Afstand' && !empty($filter_terms)) {
                                $search_query .= $filter_terms . '/';
                            }
                        }
                    } else {
                        $search_query .= '';
                    }
                }
            }

            $sort = isset( $_POST['hexon_set_sort'] ) ? '?sortering=' . $_POST['hexon_set_sort'] : '';
            $this->get_json_from_search_cars( $search_query, $sort, $post_type );

            exit;
        }
    }

    public function json_validate( $string ) {
        // decode the JSON data
        json_decode( $string );

        // switch and check possible JSON errors
        switch ( json_last_error() ) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        return $error === '';
    }

}

new HexonApi();
