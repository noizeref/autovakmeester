<?php

namespace Gravity\Hexon\Assets;

use Gravity\Hexon;

class HexonAssets {

	private $manifest;

	public function __construct( $manifest_path ) {
		if ( file_exists( $manifest_path ) ) {
			$this->manifest = json_decode( file_get_contents( $manifest_path ), true );
		} else {
			$this->manifest = [];
		}
	}

	public function get() {
		return $this->manifest;
	}

	public function getPath( $key = '', $default = null ) {
		$collection = $this->manifest;
		if ( is_null( $key ) ) {
			return $collection;
		}
		if ( isset( $collection[ $key ] ) ) {
			return $collection[ $key ];
		}
		foreach ( explode( '.', $key ) as $segment ) {
			if ( ! isset( $collection[ $segment ] ) ) {
				return $default;
			} else {
				$collection = $collection[ $segment ];
			}
		}

		return $collection;
	}
}


class LoadHexonAssets extends Hexon {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, $this->prefix . '_assets' ] );
		add_action( 'wp_footer', [ $this, $this->prefix . '_templates' ] );
	}

	public function hexon_assets() {
		wp_enqueue_script( 'hexon/js', hexon_frontend_asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );

		$config_array = [
			'homeUrl'    => get_bloginfo( 'url' ),
			'pluginUrl'  => plugins_url(),
			'ajaxURL'    => admin_url( 'admin-ajax.php' ),
			'hexonNonce' => wp_create_nonce( 'hexon-nonce' ),
			'voorraad'   => 'voorraad',
			'slug'       => get_option( 'hexon_slug' )
		];

		wp_localize_script( 'hexon/js', 'hexon', $config_array );
	}

	public function hexon_templates() {

	}
}

new LoadHexonAssets();

function hexon_backend_asset_path( $filename ) {

	$dist_path = HEXON_PLUGIN_URL . '/backend/dist/';
	$directory = dirname( $filename ) . '/';
	$file      = basename( $filename );

	static $manifest;

	if ( empty( $manifest ) ) {
		$manifest_path = HEXON_FOLDER . '/backend/dist/' . 'assets.json';
		$manifest      = new HexonAssets( $manifest_path );
	}

	if ( array_key_exists( $file, $manifest->get() ) ) {
		return $dist_path . $directory . $manifest->get()[ $file ];
	} else {
		return $dist_path . $directory . $file;
	}
}

function hexon_frontend_asset_path( $filename ) {

	$dist_path = HEXON_PLUGIN_URL . '/frontend/dist/';
	$directory = dirname( $filename ) . '/';
	$file      = basename( $filename );

	static $manifest;

	if ( empty( $manifest ) ) {
		$manifest_path = HEXON_FOLDER . '/frontend/dist/' . 'assets.json';
		$manifest      = new HexonAssets( $manifest_path );
	}

	if ( array_key_exists( $file, $manifest->get() ) ) {
		return $dist_path . $directory . $manifest->get()[ $file ];
	} else {
		return $dist_path . $directory . $file;
	}
}
