<?php

namespace Gravity\Hexon\HexonObject;

use Gravity\Hexon;

class HexonObject extends Hexon {

	public function parse_car( $post_id = null ) {
	    $post_meta = get_post_meta( $post_id );
		$data = [];

		foreach ( $post_meta as $meta_key => $meta_value ) {
			$data[$meta_key] = $this->json_validate($meta_value[0]) ? json_decode($meta_value[0], true) : $meta_value[0];
		}

		if ( ! empty( $data ) ) {
			$data['Prijs'] = $this->parse_price($data);
			return $data;
		}
	}

	public function parse_gallery( $post_id = null ) {
		$media     = get_posts(array(
							'post_parent' => $post_id,
							'post_type' => 'attachment',
							'post_excerpt' => 'afbeelding',
							'orderby' => 'ID',
							'order' => 'ASC',
							'numberposts' => -1));
		return $media;
	}

	public function parse_downloads( $post_id = null ) {
		$media     = get_attached_media( 'application/pdf', $post_id );
		$gallery   = [];
		$gallery[] = array_filter( $media, function ( $post ) {
			return $post->post_excerpt == 'document';
		} );

		$gallery = array_merge( $gallery[0] );

		return $gallery;
	}

	public function parse_videos( $post_id = null ) {
		$media     = get_attached_media( 'video', $post_id );
		$gallery   = [];
		$gallery[] = array_filter( $media, function ( $post ) {
			return $post->post_excerpt == 'video';
		} );

		$gallery = array_merge( $gallery[0] );

		return $gallery;
	}

	public function parse_price( $data ) {
		if ( isset( $data['Car']['kosten_rijklaar'] ) && ! empty( $data['Car']['kosten_rijklaar'] ) ) {
			$calculated_price = $data['Prijs'] - $data['Car']['kosten_rijklaar'];

			$price = $data['Prijs'] < $calculated_price 
					? intval($data['Prijs']) + intval($data['Car']['kosten_rijklaar'])
					: $data['Prijs'];
		} else {
			$price = $data['Prijs'];
		}

		return $price;
	}

	public function json_validate( $string ) {
		// decode the JSON data
		json_decode( $string );

		// switch and check possible JSON errors
		switch ( json_last_error() ) {
			case JSON_ERROR_NONE:
				$error = ''; // JSON is valid // No error has occurred
				break;
			case JSON_ERROR_DEPTH:
				$error = 'The maximum stack depth has been exceeded.';
				break;
			case JSON_ERROR_STATE_MISMATCH:
				$error = 'Invalid or malformed JSON.';
				break;
			case JSON_ERROR_CTRL_CHAR:
				$error = 'Control character error, possibly incorrectly encoded.';
				break;
			case JSON_ERROR_SYNTAX:
				$error = 'Syntax error, malformed JSON.';
				break;
			// PHP >= 5.3.3
			case JSON_ERROR_UTF8:
				$error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
				break;
			// PHP >= 5.5.0
			case JSON_ERROR_RECURSION:
				$error = 'One or more recursive references in the value to be encoded.';
				break;
			// PHP >= 5.5.0
			case JSON_ERROR_INF_OR_NAN:
				$error = 'One or more NAN or INF values in the value to be encoded.';
				break;
			case JSON_ERROR_UNSUPPORTED_TYPE:
				$error = 'A value of a type that cannot be encoded was given.';
				break;
			default:
				$error = 'Unknown JSON error occured.';
				break;
		}

		return $error === '';
	}

}
