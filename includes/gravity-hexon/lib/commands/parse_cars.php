<?php

namespace App\Command;

require __DIR__ . '/../../../../../../../wp-load.php';

use Gravity\Hexon\Parser\HexonParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCarsCommand extends Command {

	protected static $defaultName = 'parse:cars';

	protected function configure() {
		$this->setDescription( 'Parses all the cars available in the "pending" folder' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$output->writeln( [
			'Parsing cars that are pending 🚙',
			'============',
		] );

		$pending   = __DIR__ . '/../../items/pending';
		$done      = __DIR__ . '/../../items/done';
		$parser    = new HexonParser( 'v2', $output );
		$files     = array_diff( scandir( $pending ), [ '.', '..', '.gitkeep' ] );
		$is_active = get_option( 'hexon_parser' );

		if ( $is_active ) {
			$output->writeln( [
				'Parser already running ⌛️',
			] );

			return 0;
		}

		if ( ! empty( $files ) ) {
			add_option( 'hexon_parser', true );
		} else {
			$output->writeln( [
				'All cars are parsed 🎉',
			] );

			return 0;
		}

		foreach ( $files as $file ) {
			$car = new \SimpleXMLElement( preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', file_get_contents( $pending . '/' . $file ) ) );
			if ( $parser->process_car( $car, 'voorraad' ) ) {
				rename( $pending . '/' . $file, $done . '/' . $file );
			}
		}

		if ( function_exists( 'rocket_clean_domain' ) ) {
			rocket_clean_domain();
		}

		delete_option( 'hexon_parser' );

		return 0;
	}
}
