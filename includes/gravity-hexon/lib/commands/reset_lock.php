<?php

namespace App\Command;

require __DIR__ . '/../../../../../../../wp-load.php';

use Gravity\Hexon\Parser\HexonParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetParseLock extends Command {

	protected static $defaultName = 'parse:unlock';

	protected function configure() {
		$this->setDescription( 'Reset parse lock' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		delete_option( 'hexon_parser' );

		return 0;
	}
}
