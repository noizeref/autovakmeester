<?php

namespace Gravity\Hexon\Functions;

use Gravity\Hexon;

class Functions extends Hexon {

	public function getOccasionsByCustomerId( $customer_id, $limit = 8 ) {
		$args = [
			'post_type'      => 'voorraad',
			'posts_per_page' => $limit,
			'meta_query'     => [
				[
					'key'     => 'Klantnummer',
					'value'   => $customer_id,
					'compare' => '='
				]
			]
		];

		$data = $query = new \WP_Query( $args );

		return $data;
	}

}