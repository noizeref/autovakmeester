<?php

namespace Gravity\Hexon\Handlers;

use Gravity\Hexon;

class HexonHandlers extends Hexon {

	public function __construct() {
		// Functions
		add_action( 'init', [ $this, 'output_buffer' ] );
		register_activation_hook( __FILE__, [ $this, 'create_hexon_folder' ] );

	}

	public static function create_hexon_folders() {
		$upload     = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/hexon';
		if ( ! is_dir( $upload_dir ) ) {
			wp_mkdir_p( $upload_dir );
		}

		$media_dir = $upload_dir . '/media';
		if ( ! is_dir( $media_dir ) ) {
			wp_mkdir_p( $media_dir );
		}
	}

	//allow redirection, even if my theme starts to send output to the browser
	public function output_buffer() {
		ob_start();
	}
}

new HexonHandlers();
