<?php

namespace Gravity\Hexon\Admin;

use Gravity\Hexon;
use Gravity\Hexon\Handlers;
use Gravity\Hexon\Assets;

class HexonAdmin extends Hexon {

	private $hexon_options = [];

	public function __construct() {
		$this->hexon_options = [
			[
				'Dashboard',
				[
					'server',
					'separator',
					[ $this->prefix . '_slug', 'voorraad', 'Slug', '', '' ],
					[ $this->prefix . '_ips', '157.97.115.90', 'IP\'s', '', '' ]
				]
			],
		];

		foreach ( $this->hexon_options as $hexon_option ) {
			foreach ( $hexon_option[1] as $option ) {
				add_option( $option[0], $option[1] );
			}
		}

		add_action( 'admin_menu', [ $this, $this->prefix . '_plugin_settings' ] );
		add_action( 'add_meta_boxes', [ $this, 'meta_box_hexon' ] );
		add_action( 'save_post', [ $this, 'save_voorraad_meta' ], 10, 3 );
		add_image_size( 'hexon_large', 1024 );

	}

	public function meta_box_hexon() {
		add_meta_box( 'clear_images', 'Afbeeldingen', [ $this, 'clear_images' ], 'voorraad', 'side', 'low' );
	}

	public function clear_images( $post ) {
		global $post;
		wp_nonce_field( 'meta_box_nonce', 'meta_box_nonce' );

		?>
        <input name="clear_images" type="submit" class="button button-primary button-large" id="publish"
               value="Opnieuw ophalen">
		<?php
	}

	public function save_voorraad_meta( $post_id, $post, $update ) {

		/*
		 * In production code, $slug should be set only once in the plugin,
		 * preferably as a class property, rather than in each function that needs it.
		 */
		$slug = 'voorraad';

		// If this isn't a 'book' post, don't update it.
		if ( $slug != $post->post_type ) {
			return;
		}

		if ( isset( $_POST['clear_images'] ) ) {
			$parser = new Hexon\Parser\HexonParser();
			$parser->delete_images( $post_id, 'afbeelding' );
			$parser->delete_images( $post_id, 'document' );

			delete_post_meta( $post_id, '_thumbnail_id' );
			delete_post_meta( $post_id, 'parsed_images' );
			delete_post_meta( $post_id, 'parsed_documents' );

			$car_code = get_post_meta( $post_id, 'HexonCode', true );

			$pending = __DIR__ . '/../items/pending';
			$done    = __DIR__ . '/../items/done';

			rename( $done . '/' . $car_code . '.xml', $pending . '/' . $car_code . '.xml' );
		}
	}

	// Create admin panel
	public function hexon_plugin_settings() {

		// Add a new top-level menu:
		$hexon_options = add_menu_page(
			__( 'Hexon Opties', 'hexon' ),
			__( 'Hexon', 'hexon' ),
			'manage_options',
			'hexon.php',
			[ $this, 'hexon_plugin_settings_options' ],
			'dashicons-chart-area',
			10000
		);

		// Load the needed resources for the admin
		add_action( "load-{$hexon_options}", [
			$this,
			"hexon_plugin_settings_admin_resources"
		] ); //Double quotes for variable inject
	}

	public function hexon_plugin_settings_admin_resources() {
		wp_enqueue_style( 'hexon/css', Assets\hexon_backend_asset_path( 'styles/main.css' ), false, null );
		wp_enqueue_script( 'hexon/js', Assets\hexon_backend_asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );
	}

	public function hexon_plugin_settings_options() {

		$message = '';

		if ( ! empty( $_POST['save_hexon_options'] ) ) {

			if ( isset( $_POST['create_hexon_folder'] ) ) {
				Handlers\HexonHandlers::create_hexon_folders();
			}

			foreach ( $this->hexon_options as $hexon_option ) {
				foreach ( $hexon_option[1] as $option ) {
					if ( is_array( $option ) ) {
						update_option( $option[0], stripslashes( $_POST[ $option[0] ] ) );
					}
				}
			}
			$message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Hexon</strong> Opties opgeslagen.
        </div>';
		}


		if(!empty($_POST['force_hexon_cron'])){
		    if(get_option('hexon_parser'))
                delete_option( 'hexon_parser' );

            wp_schedule_single_event(time(), 'hexon_parser_cron');


            $message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Hexon</strong> Cron geforceerd. De cron draait nu in de achtergrond.
        </div>';
        }

		?>
        <div class="wrap hexon-admin">
            <header>
                <h1 class="wp-heading-inline">Hexon Opties</h1>
            </header>

            <form method="post" action="admin.php?page=hexon.php" id="hexon_form">
                <div class="row">
                    <div class="col-md-9">

						<?php if ( $message ) { ?>
							<?php echo $message; ?>
						<?php } ?>

						<?php if ( sizeof( $this->hexon_options ) > 1 ) { ?>
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-justified" role="tablist">
								<?php $i = 0;
								foreach ( $this->hexon_options as $section ) {
									$i ++; ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php if ( $i == 1 ) { ?>active<?php } ?>" data-toggle="tab"
                                           href="#<?php echo sanitize_title( $section[0] ); ?>"
                                           role="tab"><?php echo $section[0]; ?></a>
                                    </li>
								<?php } ?>
                            </ul>
						<?php } ?>
                        <div class="tab-content"
						     <?php if ( sizeof( $this->hexon_options ) < 2 ) { ?>style="margin-top: 0"<?php } ?>>
							<?php
							$t = 0;
							foreach ( $this->hexon_options as $section ) {
								$t ++;
								?>
                                <div class="tab-pane <?php if ( $t == 1 ) { ?>active<?php } ?>"
                                     id="<?php echo sanitize_title( $section[0] ); ?>" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
											<?php echo $section[0]; ?>
                                        </div>
                                        <div class="card-block">
											<?php foreach ( $section[1] as $option ) { ?>
												<?php if ( $option == 'info' ) { ?>
												<?php } else if ( $option == 'server' ) { ?>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Versie</label>
                                                        <div class="col-sm-10 col-form-label">
															<?php
															$plugin_data = get_plugin_data( HEXON_FOLDER . 'hexon.php' );
															echo $plugin_data['Version'];
															?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">PHP</label>
                                                        <div class="col-sm-10 col-form-label">
															<?php echo phpversion(); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Schrijfrechten</label>
                                                        <div class="col-sm-10 col-form-label">
															<?php
															$upload     = wp_upload_dir();
															$upload_dir = $upload['basedir'];
															$upload_dir = $upload_dir . '/hexon';
															if ( ! is_dir( $upload_dir ) ) {
																?>
                                                                De 'hexon' map bestaat niet in de 'uploads' map.
                                                                <button class="btn btn-success btn-sm" type="submit"
                                                                        name="save_hexon_options" value="1">Maak aan
                                                                </button>
                                                                <input type="hidden" value="1"
                                                                       name="create_hexon_folder"/>
																<?php
															} else if ( is_writable( $upload_dir ) ) {
																?>
                                                                De 'hexon' map bestaat en is schrijfbaar.
																<?php
															}
															?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <?php
                                                        $pending   = get_stylesheet_directory() . '/includes/gravity-hexon/items/pending';
                                                        $files     = array_diff( scandir( $pending ), [ '.', '..', '.gitkeep' ] );

                                                        ?>
                                                        <label class="col-sm-2 col-form-label">Pending items</label>
                                                        <div class="col-sm-10 col-form-label">
                                                            <p><?= sizeof($files); ?></p>
                                                        </div>


                                                        <label class="col-sm-2 col-form-label">Is cron locked?</label>
                                                        <div class="col-sm-10 col-form-label">
                                                            <?php $cron_locked = get_option('hexon_parser'); ?>
                                                            <button  disabled class="btn btn-<?= $cron_locked ? "danger": "success" ?> btn-sm">
                                                                <?=  $cron_locked ? "Ja": "Nee" ?>
                                                            </button>
                                                        </div>

                                                        <label class="col-sm-2 col-form-label">Handmatige Cron reset/run</label>
                                                        <div class="col-sm-10 col-form-label">
                                                            <button type="submit" name="force_hexon_cron" value="1" class="btn btn-success btn-sm">
                                                                Force cron.
                                                            </button>
                                                        </div>
                                                    </div>

												<?php } else if ( $option == 'separator' ) { ?>
                                                    <hr>
												<?php } else if ( is_array( $option ) ) { ?>
                                                    <div class="form-group row">
                                                        <label for="<?php echo $option[0]; ?>"
                                                               class="col-sm-2 col-form-label"><?php echo $option[2]; ?></label>
                                                        <div class="col-sm-10">

															<?php
															if ( $option[4] == 'radio' ) {
																$i     = 0;
																$state = '';
																foreach ( $option[5] as $key => $radio ) {
																	$i ++;
																	if ( get_option( $option[0] ) == '' && $i == 1 ) {
																		$state = 'checked="checked"';
																	}
																	?>
                                                                    <label class="custom-control custom-radio">
                                                                        <input id="radio<?php echo $key; ?>"
                                                                               value="<?php echo $key; ?>"
                                                                               name="<?php echo $option[0]; ?>"
                                                                               type="radio"
																			<?php if ( get_option( $option[0] ) == $key ) { ?>
                                                                                checked="checked"
																			<?php } else {
																				echo $state;
																			} ?>
                                                                               class="custom-control-input">
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description"><?php echo $radio; ?></span>
                                                                    </label><br/>
																	<?php
																}

															} else if ( $option[4] == 'select_page' ) {
																?>
                                                                <select class="form-control"
                                                                        name="<?php echo $option[0]; ?>">

																	<?php
																	$args  = [
																		'sort_order'   => 'asc',
																		'sort_column'  => 'post_title',
																		'hierarchical' => 1,
																		'exclude'      => '',
																		'include'      => '',
																		'meta_key'     => '',
																		'meta_value'   => '',
																		'authors'      => '',
																		'child_of'     => 0,
																		'parent'       => - 1,
																		'exclude_tree' => '',
																		'number'       => '',
																		'offset'       => 0,
																		'post_type'    => 'page',
																		'post_status'  => 'publish'
																	];
																	$pages = get_pages( $args );

																	foreach ( $pages as $page ) {
																		if ( $page->ID == get_option( 'hexon_bedankt' ) ) {
																			$selected = 'selected="selected"';
																		} else {
																			$selected = '';
																		}
																		?>
                                                                        <option value="<?php echo $page->ID; ?>" <?php echo $selected; ?>><?php echo $page->post_title; ?></option>
																		<?php
																	}
																	?>
                                                                </select>
																<?php
															} else if ( $option[4] == 'onoff' ) {
																$yes = '';
																$no  = '';
																if ( get_option( $option[0] ) == 'on' ) {
																	$yes = 'selected="selected"';
																} else {
																	$no = 'selected="selected"';
																}
																?>
                                                                <select class="form-control"
                                                                        name="<?php echo $option[0]; ?>">
                                                                    <option value="on" <?php echo $yes; ?>>Aan</option>
                                                                    <option value="off" <?php echo $no; ?>>Uit</option>
                                                                </select>
																<?php
															} else if ( $option[4] == 'textarea' ) {
																?>
                                                                <textarea class="form-control" cols="50" rows="6"
                                                                          id="<?php echo $option[0]; ?>"
                                                                          name="<?php echo $option[0]; ?>"><?php echo get_option( $option[0] ); ?></textarea>
																<?php
															} else {
																?>
                                                                <input class="form-control" type="text"
                                                                       value="<?php if ( $get_option = get_option( $option[0] ) ) {
																	       echo $get_option;
																       } else {
																	       echo $option[1];
																       } ?>"
                                                                       id="<?php echo $option[0]; ?>"
                                                                       name="<?php echo $option[0]; ?>">
																<?php
															}
															if ( $option[3] ) {
																?>
                                                                <small class="form-text text-muted"><?php echo $option[3]; ?></small>
															<?php } ?>
                                                        </div>
                                                    </div>
												<?php } ?>
											<?php } ?>
                                        </div>
                                    </div>
                                </div>
								<?php
							}
							?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                Save
                            </div>
                            <div class="card-block">
                                <button class="btn btn-primary" type="submit" name="save_hexon_options" value="1">
                                    Opslaan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
		<?php
	}

	public function get_search_filters() {

		$filters   = [];
		$filters[] = 'soort';
		$filters[] = 'financiering';
		$filters[] = 'merk-model';
		$filters[] = 'prijs';
		$filters[] = 'bouwjaar';
		$filters[] = 'kleur';
		$filters[] = 'kilometerstand';
		$filters[] = 'brandstof';
		$filters[] = 'transmissie';
		$filters[] = 'carrosserie';
		$filters[] = 'apk';
		$filters[] = 'vermogen';
		$filters[] = 'motorinhoud';
		$filters[] = 'aantal-deuren';
		$filters[] = 'nap';
		$filters[] = 'btw-verrekenbaar';
		$filters[] = 'straal';

		return $filters;
	}

	public function get_filters() {

		$filters   = [];
		$filters[] = 'soort';
		$filters[] = 'financiering';
		$filters[] = 'merk-model';
		$filters[] = 'prijs';
		$filters[] = 'bouwjaar';
		$filters[] = 'kleur';
		$filters[] = 'kilometerstand';
		$filters[] = 'brandstof';
		$filters[] = 'transmissie';
		$filters[] = 'carrosserie';
		$filters[] = 'apk';
		$filters[] = 'vermogen';
		$filters[] = 'motorinhoud';
		$filters[] = 'aantal-deuren';
		$filters[] = 'nap';
		$filters[] = 'btw-verrekenbaar';
		$filters[] = 'straal';

		return $filters;
	}

	public function get_all_filters() {

		$filters   = [];
		$filters[] = 'soort';
		$filters[] = 'financiering';
		$filters[] = 'merk-model';
		$filters[] = 'prijs';
		$filters[] = 'bouwjaar';
		$filters[] = 'kleur';
		$filters[] = 'kilometerstand';
		$filters[] = 'brandstof';
		$filters[] = 'transmissie';
		$filters[] = 'carrosserie';
		$filters[] = 'apk';
		$filters[] = 'vermogen';
		$filters[] = 'motorinhoud';
		$filters[] = 'aantal-deuren';
		$filters[] = 'nap';
		$filters[] = 'btw-verrekenbaar';
		$filters[] = 'straal';

		return $filters;
	}
}

new HexonAdmin();
