<?php

namespace Gravity\Hexon\Parser;

use Gravity\Hexon;

ini_set( 'max_execution_time', '0' );
set_time_limit( 0 );

class HexonParser extends Hexon {
	private $_media = false;
	private $_batch;
	private $_car_code;
	private $_xml;
	private $_version;
	private $_output;

	public function __construct($version = 'v1', $output = null) {
		// Functions
		add_action( 'init', [ $this, 'output_buffer' ] );
		add_action( 'template_redirect', [ $this, 'init_hexon_parser' ] );

		$this->_batch   = 10;
		$this->_xml     = file_get_contents( 'php://input' );
		$this->_version = $version;
		$this->_output  = $output;

		if ($this->_version == 'v2') {
			$this->_media = true;
		}
	}

	public function log($string) {
		$log_file = get_stylesheet_directory() . '/includes/gravity-hexon/lib/log/hexon.log';
		$log      = '[' . date( 'Y-m-d H:i:s', time() ) . '] ' . $string . PHP_EOL;

		file_put_contents( $log_file, $log, FILE_APPEND );
	}

	public function cron_log($message) {
		if (!$this->_version == 'v2') {
			$message = date( "Y-m-d H:i:s" ) . " - $message" . PHP_EOL;
			print( $message );
			flush();
			ob_flush();
		} else {
			// $this->_output->writeln( [ $message ] );
		}
	}

	public function output_buffer() {
		ob_start();
	}

	public function init_hexon_parser() {
		global $wp_query;

		$hexon = (boolean) $wp_query->get( 'hexon_parser' );

		$myIps = explode( ',', get_option( 'hexon_ips' ) );
		$myIps = array_merge( HEXON_IPS, $myIps );

		if (!$hexon) { return; }

		if (in_array($_SERVER['REMOTE_ADDR'], $myIps)) {
			$this->move_input();

			ob_end_flush();
		} else {
			die('No access!');
		}
	}

	public function get_upload_dir() {
		$upload     = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/hexon';

		return $upload_dir;
	}

	public function move_input() {
		$car      = new \SimpleXMLElement( preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', $this->_xml ) );
		$car_code = $car->voertuignr->__toString();
		$car_action = $car->actie->__toString();

		if (!$car_code) {
			$this->log( 'No vehicle number, stop parsing' );
			$this->parseResponse( 'ERROR', 'No vehicle number' );

			return;
		}

		$file = __DIR__ . '/../items/pending/' . $car_code . '_' . $car_action . '.xml';

		if (file_put_contents($file, $this->_xml)) {
			$this->parseResponse('OK', null);
		} else {
			$this->parseResponse('ERROR', 'Could not add file to queue'); // New error, check if they do get to pending
		}

		exit;
	}

	public function process_cars() {
		if ($this->_xml) {
			$car = new \SimpleXMLElement( preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', $this->_xml ) );

			// Start buffering
			ob_start();

			$this->log(print_r($this->_xml, true));
			$car_processed = $this->process_car($car, 'voorraad');

			if ($car_processed) {
				$this->log('Done');
				// Get value of buffering so far and stop buffering
				$buffered_content = ob_get_clean();

				// Use it
				print $buffered_content;
			}
		} else {
			$this->parseResponse( 'ERROR', 'NO XML' );
		}
	}

	public function process_car($car = null, $car_type = null, $file = null) {
		if (!$car) { 
            $payload = "<?xml version='1.0' encoding='UTF-8'?>
						<response>
							<result>ERROR</result>
							<voertuignr>$file</voertuignr>
							<error_message>NO XML</error_message>
						</response>";
			// Send to Hexon
            $this->sendPayload($payload);

			return false;
		 }

		$car_code = $car->voertuignr->__toString();

		if (!$car_code) {
            $payload = "<?xml version='1.0' encoding='UTF-8'?>
						<response>
							<result>ERROR</result>
							<voertuignr>$car_code</voertuignr>
							<error_message>No vehicle number</error_message>
						</response>";
			// Send to Hexon
            $this->sendPayload($payload);

			return false;
		}

		$this->_car_code = $car_code;

		$leasevorm         = ( ! empty( $car->lease ) && ! empty( $car->lease->leasevorm ) ) ? $car->lease->leasevorm->__toString() : '';
		$lease_maandbedrag = ( ! empty( $car->lease ) && ! empty( $car->lease->maandbedrag ) ) ? $car->lease->maandbedrag->__toString() : 0;

		if (!empty($lease_maandbedrag) && empty($leasevorm)) { $leasevorm = 'private'; }

		$car_existing = $this->check_if_car_exists($car_code, $car_type);

		if ($car_existing) {
			if ($car->actie->__toString() == 'delete' || $car->actie->__toString() == 'remove') {
				$deleteSuccessful = $this->delete($car_existing, $car_code);
				
				return true;
			} else {
				$post_id = $this->edit( $car_existing, $car, $car_code, $car_type, $leasevorm, $lease_maandbedrag );
			}
		} else {
			if ($car->actie->__toString() == 'delete' || $car->actie->__toString() == 'remove') {
				$deleteSuccessful = $this->delete($car_existing, $car_code);
				
				return true; // Exit the function
			} else {
				$post_id = $this->add($car, $car_code, $car_type, $leasevorm, $lease_maandbedrag);
			}
		}

		if (isset($post_id)) {
			$post_url = get_permalink($post_id);
            $payload = "<?xml version='1.0' encoding='UTF-8'?>
						<response>
							<result>OK</result>
							<voertuignr>$car_code</voertuignr>
							<url>$post_url</url>
						</response>";

			$this->update_post_meta( $post_id, $car );

			if ($this->_media) {
				// reload all images
				$this->delete_images( $post_id, 'afbeelding' );
				$this->delete_images( $post_id, 'document' );

				$this->get_images( $post_id, $car_code );
				if ( $car->documenten ) {
					$this->get_images( $post_id, $car_code, 'documents' );
				}
			}

			//Set update time
			update_post_meta( $post_id, 'CarUpdated', time() );

			// Send to Hexon
            $this->sendPayload($payload);
			return true;
		}
		
		return false;
	}

	public function setLatLong( $post, $carId ) {
		$address = get_field( 'business_location', $post->ID );

		if ( isset( $address['lat'] ) && isset( $address['lng'] ) ) {
			update_post_meta( $carId, 'Latitude', $address['lat'] );
			update_post_meta( $carId, 'Longitude', $address['lng'] );
		}
	}

	public function alter_upload_dir( $upload ) {

		if ( ! is_dir( $upload['basedir'] . '/hexon/media/' . $this->_car_code ) ) {
			wp_mkdir_p( $upload['basedir'] . '/hexon/media/' . $this->_car_code );
		}

		$upload['subdir'] = '/hexon/media/' . $this->_car_code;
		$upload['path']   = $upload['basedir'] . $upload['subdir'];
		$upload['url']    = $upload['baseurl'] . $upload['subdir'];

		return $upload;
	}

	public function delete_images( $post_id, $type = 'afbeelding' ) {
		$images = get_attached_media( 'image', $post_id );

		if ( ! empty( $images ) ) {
			foreach ( $images as $image ) {
				if ( $image->post_excerpt == $type ) {
					wp_delete_attachment( $image->ID, true );
				}
			}
		}
	}

	public function remove_image_sizes( $sizes ) {
		$defaults = [ 'thumbnail', 'medium', 'medium_large', 'large' ];

		foreach ( $sizes as $key => $size ) {
			if ( ! in_array( $key, $defaults ) ) {
				unset( $sizes[ $key ] );
			}
		}

		return $sizes;
	}

	public function get_images( $post_id, $car_code, $media_type = 'images' ) {
		$this->_car_code = $car_code;
		add_filter( 'intermediate_image_sizes_advanced', [ $this, 'remove_image_sizes' ] );
		add_filter( 'upload_dir', [ $this, 'alter_upload_dir' ] );
		$message = 'Parsing ' . $media_type . ' - ' . $post_id . ' - ' . $car_code;

		if ( ! $this->_version == 'v2' ) {
			$this->log( $message );
		}

		$this->cron_log( $message );

		$author = 1;
		if ( $post_author = get_post_field( 'post_author', $post_id ) ) {
			$author = $post_author;
		}

		if ( $media_type == 'documents' ) {
			$car_media = get_post_meta( $post_id, 'car_documents', true );
		} else {
			$car_media = get_post_meta( $post_id, 'car_media', true );
		}

		if ( ! $car_media ) {
			$car_data = json_decode( get_post_meta( $post_id, 'Car', true ) );
			if ( $media_type == 'documents' ) {
				$car_media = json_encode( $car_data->documenten );
			} else {
				$car_media = json_encode( $car_data->afbeeldingen );
			}
		}

		if ( ! empty( $car_media = json_decode( $car_media, true ) ) ) {
			if ( is_array( $car_media ) ) {
				if ( count( $car_media ) == 1 ) {
					$car_media = reset( $car_media );
				}

				$i = 0;
				foreach ( $car_media as $media ) {

					$thumbnail_id = null;
					if ( isset( $media['url'] ) ) {
						$url = $media['url'];

						if ( $url && filter_var( $url, FILTER_VALIDATE_URL ) ) {
							$extension = pathinfo( parse_url( $url, PHP_URL_PATH ), PATHINFO_EXTENSION );
							$filename  = basename( parse_url( $url, PHP_URL_PATH ), '.' . $extension );

							if ( isset( $media['categorie'] ) ) {
								$media_name = $media['categorie'];
								if ( isset( $media['bestandsnaam'] ) ) {
									$name = $media['bestandsnaam'];
								}
							} elseif ( isset( $media['bestandsnaam'] ) ) {
								$media_name = basename( $media['bestandsnaam'], '.' . $extension );
								$name       = $media['bestandsnaam'];
							} else {
								$media_name = $car_code . '-' . $post_id . '-' . $filename;
								$name       = $media_name . '.' . $extension;
							}

							if ( ! $this->media( $media_name, $car_code ) ) {
								$this->cron_log( 'Fetching media item: ' . ( $i + 1 ) );

								$get = wp_remote_get( $url, [ 'timeout' => 120 ] );

								$mime_type = wp_remote_retrieve_header( $get, 'content-type' );

								if ( ! substr_count( $mime_type, 'image' ) ) {
									if ( ! substr_count( $mime_type, 'video' ) ) {
										if ( ! substr_count( $mime_type, 'pdf' ) ) {
											return false;
										}
									}
								}

								$bits = wp_upload_bits( $name, '', wp_remote_retrieve_body( $get ) );

								if ( $bits['error'] ) {
									return false;
								}

								if ( $media_type == 'documents' ) {
									$media_group = 'document';
								} else {
									$media_group = 'afbeelding';

									if ( substr_count( $mime_type, 'video' ) ) {
										$media_group = 'video';
									}

									if ( substr_count( $mime_type, 'image' ) ) {
										$media_group = 'afbeelding';
									}

									if ( substr_count( $mime_type, 'pdf' ) ) {
										$media_group = 'document';
									}
								}

								$thumb_data = [
									'post_title'     => $media_name,
									'post_mime_type' => $mime_type,
									'post_excerpt'   => $media_group,
									'post_author'    => $author,
								];

								$thumbnail_id = wp_insert_attachment( $thumb_data, $bits['file'], $post_id );

								if ( $thumbnail_id ) {
									require_once( ABSPATH . 'wp-admin/includes/image.php' );
									require_once( ABSPATH . 'wp-admin/includes/file.php' );
									require_once( ABSPATH . 'wp-admin/includes/media.php' );
									$metadata = wp_generate_attachment_metadata( $thumbnail_id, $bits['file'] );
									wp_update_attachment_metadata( $thumbnail_id, $metadata );

									update_post_meta( $thumbnail_id, 'HexonCode', $car_code );
									update_post_meta( $thumbnail_id, 'MediaType', $media_group );
									update_post_meta( $thumbnail_id, 'MediaName', $media_name );

									if ( ! empty( $media['@attributes'] ) && isset( $media['@attributes']['nr'] ) ) {
										update_post_meta( $thumbnail_id, 'MediaNumber', $media['@attributes']['nr'] );
									}

									if ( $i == 0 ) {
										update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
										$this->cron_log( 'Set featured media item for post: ' . $post_id );
									}

									$this->cron_log( 'Received media item: ' . ( $i + 1 ) );
								} else {
									$this->cron_log( 'Failed to fetch media item: ' . ( $i + 1 ) );
								}
							}
						}
					}

					$i ++;

					if ( $i == count( $car_media ) ) {
						update_post_meta( $post_id, 'parsed_' . $media_type, 1 );
					}
				}
			}
		} else {
			update_post_meta( $post_id, 'parsed_' . $media_type, 1 );
		}
	}

	public function check_if_car_exists($car_code = null, $car_type) {
		if (!$car_code) { return; }

		$args = [
			'post_status'    => [ 'any', 'trash' ],
			'post_type'      => strtolower( $car_type ),
			'posts_per_page' => 1,
			'meta_query'     => [
				[
					'key'     => 'HexonCode',
					'value'   => $car_code,
					'compare' => '=',
				],
			],
		];

		$query = new \WP_Query( $args );

		return $query->post_count > 0 ? $query->post->ID : false;
	}

	public function check_if_car_needs_update( $car_code, $car_date, $car_type ) {

		if ( ! $car_code ) {
			return;
		}

		$args = [
			'post_status'    => [ 'any', 'trash' ],
			'post_type'      => strtolower( $car_type ),
			'posts_per_page' => 1,
			'meta_query'     => [
				[
					'key'     => 'HexonCode',
					'value'   => $car_code,
					'compare' => '=',
				],
				[
					'key'     => 'CarDate',
					'value'   => strtotime( $car_date ),
					'compare' => '!=',
				],
			],
		];

		$query = new \WP_Query( $args );

		return $query->post_count > 0 ? $query->post->ID : false;
	}

	public function add( $car = [], $car_code = null, $car_type = null, $leasevorm = null, $lease_maandbedrag = null ) {
		$post_id = $this->handle_car_post( $car, $car_code, $car_type, null, $leasevorm, $lease_maandbedrag );

		return $post_id;

	}

	public function edit($post_id = null,
						 $car = [],
						 $car_code = null,
						 $car_type = null,
						 $leasevorm = null,
						 $lease_maandbedrag = null) {
		$this->handle_car_post( $car, $car_code, $car_type, $post_id, $leasevorm, $lease_maandbedrag );

		return $post_id;
	}

	public function handle_car_post($car,
									$car_code,
									$car_type,
									$post_id = null,
									$leasevorm = null,
									$lease_maandbedrag = null) {
		$car_title = $this->get_car_title($car);
		$car_text  = wpautop($car->opmerkingen->__toString());
		$car_date  = $this->get_car_date($car);

		$post = [
			'post_status'  => 'publish',
			'post_type'    => strtolower($car_type),
			'post_title'   => $car_title,
			'post_content' => $car_text,
			'post_date'    => $car_date,
		];

		if ($garage = $this->get_garage($car->klantnummer->__toString())) {
			$post['post_author'] = $garage->post_author;
		}

		// Update if post already exists
		if ($post_id) {
			$post['ID'] = $post_id;
			wp_update_post($post);
		} else { // Add if post doesn't exist yet
			$post_id = wp_insert_post( $post );
		}

		update_post_meta($post_id, 'HexonCode', $car_code);
		update_post_meta($post_id, 'Klantnummer', $car->klantnummer->__toString());
		update_post_meta($post_id, 'CarDate', strtotime($car_date));
		update_post_meta($post_id, 'BouwJaar', $car->bouwjaar->__toString());
		update_post_meta($post_id, '_kilometerstand', $car->tellerstand->__toString());

		if (!empty($leasevorm)) {
			update_post_meta($post_id, 'LeaseVorm', $leasevorm);
			update_post_meta($post_id, 'LeaseMaandbedrag', $lease_maandbedrag);

			$termName = $leasevorm == "short" ? "short-lease" : "private-lease";

			wp_delete_object_term_relationships($post_id, ['financiering_voorraad']);
			wp_set_object_terms($post_id, $termName, "financiering_voorraad");
		} else {
			delete_post_meta($post_id, 'LeaseVorm');
			delete_post_meta($post_id, 'LeaseMaandbedrag');

			wp_delete_object_term_relationships($post_id, ['financiering_voorraad']);
		}

		return $post_id;
	}


	public function sendPayload($payload) {
        $url  = 'https://autovakmeester.export.doorlinkenvoorraad.nl/published';
        $curl = curl_init($url);

        // Set content header to text/xml.
        curl_setopt ($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));

        // Set curl post to true
        curl_setopt($curl, CURLOPT_POST, true);

        // Attach xml payload string to post request
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //Execute the POST request and send our XML.
        $result = curl_exec($curl);

        //Do some basic error checking.
        if (curl_errno($curl)){
            // Log error
        }

        //Close the cURL handle.
        curl_close($curl);

        //Print out the response output.
        return $result;
    }

	public function get_car_date($car) {
		$time = empty($car->datum_binnenkomst) ? time() : strtotime($car->datum_binnenkomst->__toString());
		return date('Y-m-d H:i:s', $time);
	}

	public function get_car_title( $car ) {
		return wp_strip_all_tags(
			$car->merk->__toString() . ' ' . $car->model->__toString() . ' ' . $car->type->__toString() . ' ' . $car->kenteken->__toString()
		);
	}

	public function update_post_meta( $post_id = null, $car = [] ) {
		$this->save_post_meta($post_id, $car);
	}

	public function delete($post_id, $car_code) {
		if (!$post_id) { 
			$payload = "<?xml version='1.0' encoding='UTF-8'?>
			<response>
				<result>ERROR</result>
				<voertuignr>$car_code</voertuignr>
				<error_message>Car not found</error_message>
			</response>";
			// Send to Hexon
			$this->sendPayload($payload);
			return false;
		}

		$post_url 		  = get_permalink($post_id);
		$post_attachments = get_children(['post_parent' => $post_id, 'post_type' => 'attachment']);

		foreach ($post_attachments as $attachment) {
			wp_delete_attachment( $attachment->ID, true );
		}

		$post_children = get_children(['post_parent' => $post_id]);

		if (is_array($post_children)) {
			foreach ($post_children as $child) {
				wp_delete_post($child->ID, true);
			}
		}

		$this->delete_post_meta($post_id);
		wp_delete_post($post_id, true);

		$payload = "<?xml version='1.0' encoding='UTF-8'?>
					<response>
						<result>OK</result>
						<voertuignr>$car_code</voertuignr>
						<url>$post_url</url>
					</response>";
		// Send to Hexon
		$this->sendPayload($payload);

		return true;
	}

	public function delete_post_meta($post_id = null) {
		if (!$post_id) { return false; }

		$allmetas = get_post_meta($post_id);

		foreach ($allmetas as $metakey => $meta_value) {
			delete_post_meta($post_id, $metakey);
		}

		return true;
	}

	public function save_post_meta($post_id, $car) {
		// Process the search fields
		$car_price = 0;

		// Fill search car
		$car_search = wp_strip_all_tags(
			$car->merk->__toString() . ' ' . $car->model->__toString() . ' ' . $car->type->__toString() . ' ' . $car->kenteken->__toString() . ' ' . $car->voertuignr_klant->__toString() . ' ' . $car->voertuignr->__toString()
		);
		update_post_meta($post_id, 'CarSearch', $car_search);

		$kosten_rijklaar = intval($car->kosten_rijklaar->__toString());

		// Fill prijs car
		if (!empty($car->actieprijs)) {
			$car_price = $car->actieprijs->__toString();
		} else {
			if (!empty($car->verkoopprijs_particulier)) {
				$car_price = $car->verkoopprijs_particulier->__toString();
			} else {
				if (!empty($car->verhuurprijs_particulier)) {
					$car_price = $car->verhuurprijs_particulier->__toString();
				}
			}
		}

		$car_price = intval( $car_price ) + ( $kosten_rijklaar > 0 ? $kosten_rijklaar : 0 );

		// Set the price
		$this->set_car_price_term( $post_id, 'prijs', 'filters_voorraad', $car_price );
		update_post_meta( $post_id, 'Prijs', $car_price );
		update_post_meta( $post_id, '_prijs', $car_price ); // for filtering

		// Set tax
		if ( $car->nieuw_voertuig->__toString() == 'n' ) {
			$this->set_car_term( $post_id, 'occasions', 'soort_voorraad', 'Occasion', false );
		} else {
			if ( $car->nieuw_voertuig->__toString() == 'j' ) {
				$this->set_car_term( $post_id, 'nieuw', 'soort_voorraad', 'Nieuw', false );
			}
		}
		if ( $car->demovoertuig->__toString() == 'j' ) {
			$this->set_car_term( $post_id, 'demo', 'soort_voorraad', 'Demo', false );
		}
		if ( $car->voertuigsoort->__toString() == 'BEDRIJF' ) {
			$this->set_car_term( $post_id, 'bedrijfsauto', 'soort_voorraad', 'Bedrijfsauto', false );
		}

		//Set financiering
		if ( $car->verhuur->__toString() == 'j' ) {
			$this->set_car_term( $post_id, 'private-lease', 'financiering_voorraad', 'Private lease', false );
		} else {
			$this->set_car_term( $post_id, 'koop', 'financiering_voorraad', 'Koop', false );
		}

		$this->set_car_meta( $car, $post_id );

		if ( isset( $car->afbeeldingen ) ) {
			update_post_meta( $post_id, 'car_media', addslashes( json_encode( $car->afbeeldingen ) ) );
		}

		if ( isset( $car->documenten ) ) {
			update_post_meta( $post_id, 'car_documents', addslashes( json_encode( $car->documenten ) ) );
		}

		//Process the rest
		update_post_meta( $post_id, 'Car', addslashes( json_encode( $car ) ) );
	}

	public function set_car_meta( $car, $post_id ) {
		if ( $garage = $this->get_garage( $car->klantnummer->__toString() ) ) {
			$this->setLatLong( $garage, $post_id );
		}

		if ( ! empty( $car->bouwjaar ) ) {
			$this->set_car_term( $post_id, 'BouwJaar', 'filters_voorraad', $car->bouwjaar->__toString(), true );
		}

		if ( ! empty( $car->tellerstand)) {
			$this->set_car_term( $post_id, '_kilometerstand', 'filters_voorraad', $car->tellerstand->__toString(), true );
		}

		if ( ! empty( $car->brandstof ) ) {
			$brandstof_types = ['B' => 'Benzine',
								'D' => 'Diesel',
								'3' => 'LPG',
								'L' => 'LPG',
								'E' => 'Elektrisch of Hybride',
								'H' => 'Elektrisch of Hybride',
								'C' => 'CVT'];

			$brandstof = array_key_exists($car->brandstof->__toString(), $brandstof_types) 
						 ? $brandstof_types[$car->brandstof->__toString()]
						 : "Onbekend (" . $car->brandstof->__toString() . ")";

			$this->set_car_term( $post_id, 'brandstof', 'filters_voorraad', $brandstof, true );
		}

		if ( ! empty( $car->carrosserie ) ) {
			$this->set_car_term( $post_id, 'carrosserie', 'filters_voorraad', $car->carrosserie->__toString(), true );
		}

		if ( ! empty( $car->tellerstand ) ) {
			$this->set_car_km_term(
				$post_id,
				'kilometerstand',
				'filters_voorraad',
				$car->tellerstand->__toString(),
				'km'
			);
		}

		if ( isset( $car->apk ) ) {
			if ( $car->apk->attributes()->bij_aflevering->__toString() == 'j' ) {
				$apk = 'Ja';
			} else {
				$apk = 'N.V.T';
			}
			$this->set_car_term( $post_id, 'apk', 'filters_voorraad', $apk, true, 'apk' );
		}

		if ( ! empty( $car->vermogen_motor_pk ) ) {
			$this->set_car_term( $post_id,
				'vermogen',
				'filters_voorraad',
				$car->vermogen_motor_pk->__toString(),
				true,
				'pk' );
		}

		if ( ! empty( $car->basiskleur ) ) {
			$this->set_car_term( $post_id, 'kleur', 'filters_voorraad', $car->basiskleur->__toString(), true );
		}

		if ( ! empty( $car->cilinder_inhoud ) ) {
			$cilinder_inhoud = null;

			$inhoud = $car->cilinder_inhoud->__toString();
			if ( $inhoud < 1000 ) {
				$cilinder_inhoud = 'Tot 1.0 liter';
			} elseif ( $inhoud < 1300 ) {
				$cilinder_inhoud = '1.1 tot 1.3 liter';
			} elseif ( $inhoud < 2000 ) {
				$cilinder_inhoud = '1.4 tot 2.0 liter';
			} elseif ( $inhoud < 3000 ) {
				$cilinder_inhoud = '2.1 tot 3.0 liter';
			} else {
				$cilinder_inhoud = '3.1 of meer';
			}

			if ( $cilinder_inhoud ) {
				$this->set_car_term( $post_id, 'motor-inhoud', 'filters_voorraad', $cilinder_inhoud, true );
			}
		}

		if ( ! empty( $car->aantal_deuren ) ) {
			$aantal_deuren = null;

			switch ( $car->aantal_deuren->__toString() ) {
				case "2":
				case "3":
					$aantal_deuren = '2/3';
					break;
				case "4":
				case "5":
					$aantal_deuren = '4/5';
					break;
				case "6":
				case "7":
					$aantal_deuren = '6/7';
					break;
				default:
					$aantal_deuren = null;
			}

			if ( $aantal_deuren ) {
				$this->set_car_term( $post_id, 'aantal-deuren', 'filters_voorraad', $aantal_deuren, true, 'deuren' );
			}
		}

		if ( ! empty( $car->nap_weblabel ) ) {
			if ( $car->nap_weblabel->__toString() == 'j' ) {
				$nap_label = 'Ja';
			} else {
				$nap_label = 'NVT'; // <-- Pay attention: do caps matter?
			}
			$this->set_car_term( $post_id, 'nap', 'filters_voorraad', $nap_label, true, 'nap' );
		}

		if ( ! empty( $car->merk ) ) {
			$this->set_car_term( $post_id, 'merk-model', 'filters_voorraad', $car->merk->__toString(), true );

			if ( ! empty( $car->model ) ) {
				$this->set_car_term(
					$post_id,
					$car->merk->__toString(),
					'filters_voorraad',
					$car->model->__toString(),
					true
				);

				if ( ! empty( $car->type ) ) {
					$this->set_car_term(
						$post_id,
						$car->model->__toString(),
						'filters_voorraad',
						$car->type->__toString(),
						true
					);
				}
			}
		}

		if ( ! empty( $car->transmissie ) ) {
			$transmissie_types = ['S' => 'Semi-automaat',
								  'A' => 'Automaat',
								  'C' => 'CVT',
								 ];
			$transmissie = array_key_exists($car->transmissie->__toString(), $transmissie_types)
						   ? $transmissie_types[$car->transmissie->__toString()]
						   : 'Handgeschakeld';
			$this->set_car_term( $post_id, 'transmissie', 'filters_voorraad', $transmissie, true );
		}

		if ( ! empty( $car->zoekaccessoires ) ) {
			foreach ( $car->zoekaccessoires as $zoekaccessoire ) {
				foreach ( $zoekaccessoire as $accessoire ) {
					$this->set_car_term( $post_id,
						sanitize_title( $accessoire ),
						'opties_voorraad',
						$accessoire->__toString(),
						false );
				}
			}
		}
	}

	public function set_car_term(
		$post_id,
		$term,
		$taxonomy,
		$value,
		$child = true,
		$suffix = '',
		$include_suffix = false
	) {

		$filter = get_term_by( 'slug', $term, $taxonomy );
		if ( isset( $filter ) && $filter !== false ) {
			if ( $child ) {
				if ( ! empty( $suffix ) ) {
					if ( $value == '1' ) {
						$suffix = substr( $suffix, 0, - 1 );
					}

					$slug = $value . ' ' . $suffix;

					if ( $include_suffix ) {
						$value = $slug;
					}

				} else {
					$slug = $value;
				}
				if ( isset( $filter->term_id ) ) {
					$term_exists = term_exists( ucfirst( $value ), $taxonomy, $filter->term_id );
					if ( ! empty( $term_exists ) ) {
						$filter_child_id = (int) $term_exists['term_id'];
						$filter_ids      = [ $filter_child_id, (int) $filter->term_id ];
					} else {
						$filter_child    = wp_insert_term(
							ucfirst( $value ),
							$taxonomy,
							[
								'parent' => (int) $filter->term_id,
								'slug'   => strtolower( $slug ),
							]
						);
						$filter_child_id = (int) $filter_child['term_id'];
						$filter_ids      = [ $filter_child_id, (int) $filter->term_id ];
					}
				}
			} else {
				$filter_child_id = (int) $filter->term_id;
				$filter_ids      = [ $filter_child_id ];
			}
		} else {
			$term_exists = term_exists( ucfirst( $value ), $taxonomy );
			if ( ! empty( $term_exists ) ) {
				$filter_child_id = (int) $term_exists['term_id'];
				$filter_ids      = [ $filter_child_id ];
			} else {
				$filter_child    = wp_insert_term(
					ucfirst( $value ),
					$taxonomy,
					[
						'slug' => strtolower( $value ),
					]
				);
				$filter_child_id = (int) $filter_child['term_id'];
				$filter_ids      = [ $filter_child_id ];
			}
		}

		return wp_set_object_terms( $post_id, $filter_ids, $taxonomy, true );
	}

	public function set_car_price_term( $post_id, $term, $taxonomy, $value ) {

		$ranges = [
			'0',
			'2500',
			'5000',
			'7500',
			'10000',
			'12500',
			'15000',
			'17500',
			'20000',
			'22500',
			'25000',
			'27500',
			'30000',
			'32500',
			'35000',
			'37500',
			'40000',
			'45000',
			'50000',
			'55000',
			'60000',
			'65000',
			'70000',
			'75000',
			'80000',
			'90000',
			'100000',
			'125000',
			'150000',
			'200000',
		];

		$this->set_car_range( $post_id, $term, $taxonomy, $value, $ranges );
	}

	public function set_car_km_term( $post_id, $term, $taxonomy, $value, $suffix ) {

		$ranges = [
			'0',
			'2500',
			'5000',
			'7500',
			'10000',
			'12500',
			'15000',
			'17500',
			'20000',
			'22500',
			'25000',
			'27500',
			'30000',
			'32500',
			'35000',
			'37500',
			'40000',
			'45000',
			'50000',
			'55000',
			'60000',
			'65000',
			'70000',
			'75000',
			'80000',
			'90000',
			'100000',
			'125000',
			'150000',
			'200000',
		];

		$this->set_car_range( $post_id, $term, $taxonomy, $value, $ranges, $suffix );
	}

	public function set_car_range( $post_id, $term, $taxonomy, $value, $ranges, $suffix = '' ) {
		$ranges = array_reverse( $ranges );

		foreach ( $ranges as $range ) {
			if ( $value > $range ) {
				$this->set_car_term( $post_id, $term, $taxonomy, $range, true, $suffix );

				return;
			}
		}
	}

	public function flatten( $array, $prefix = '' ) {
		$result = [];
		foreach ( $array as $key => $value ) {
			if ( strtolower( $key ) != "medialijst" ) {
				if ( is_array( $value ) ) {
					$result = $result + $this->flatten( $value, $prefix . $key . '_' );
				} else {
					$result[ $prefix . $key ] = $value;
				}
			}
		}

		return $result;
	}

	public function media( $name, $car_code ) {

		if ( ! $name || ! $car_code ) {
			return;
		}

		$args = [
			'post_status'    => 'any',
			'post_type'      => 'attachment',
			'posts_per_page' => 1,
			'meta_query'     => [
				'relation' => 'AND',
				[
					'key'     => 'MediaName',
					'value'   => $name,
					'compare' => '=',
				],
				[
					'key'     => 'HexonCode',
					'value'   => $car_code,
					'compare' => '=',
				],
			],
		];

		$query = new \WP_Query( $args );

		return $query->post_count > 0;
	}

	public function abbreviate( $longname ) {
		$letters = [];
		$words   = explode( ' ', $longname );
		foreach ( $words as $word ) {
			$word = ( substr( $word, 0, 1 ) );
			array_push( $letters, $word );
		}

		return implode( '.', $letters ) . '.';
	}

	public function parseResponse( $status, $error = null, $url = null ) {
		header( 'Content-Type: application/xml' );

		$output = '<?xml version="1.0" encoding="UTF-8"?>';
		$output .= '<response>';
		$output .= '<result>' . $status . '</result>';
		if ( $error ) {
			$output .= '<error_message>' . $error . '</error_message>';
		}
		if ( $url ) {
			$output .= '<direct_url>' . htmlspecialchars( $url, ENT_XML1, 'UTF-8' ) . '</direct_url>';
		}
		$output .= '</response>';

		echo $output;
		exit;
	}

	public function get_garage($customer_id) {
		$args = [
			'post_status'    => 'any',
			'post_type'      => 'garage',
			'posts_per_page' => 1,
			'meta_query'     => [
				[
					'key'     => 'hexon_klantennummer',
					'value'   => $customer_id,
					'compare' => '=',
				]
			],
		];

		$query = new \WP_Query($args);

		return $query->post_count > 0 ? reset( $query->posts ) : false;
	}
}

new HexonParser();
