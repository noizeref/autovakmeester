<?php

namespace Gravity\Hexon\PostType;

use Gravity\Hexon;

class HexonPostType extends Hexon {

	public function __construct() {
		add_action( 'init', [ $this, $this->prefix . '_custom_post_type' ] );
		add_action( 'pre_get_posts', [ $this, $this->prefix . '_pre_get_posts' ] );

	}

	// Register Custom Post Type
	public function hexon_custom_post_type() {

		//Voorraad
		if ( get_option( 'hexon_slug' ) != '' ) {
			$slug = get_option( 'hexon_slug' );
		} else {
			$slug = 'voorraad';
		}

		$labels = [
			'name'               => _x( 'Voorraad', 'Post Type General Name', 'hexon' ),
			'singular_name'      => _x( 'Auto', 'Post Type Singular Name', 'hexon' ),
			'menu_name'          => __( 'Voorraad', 'hexon' ),
			'parent_item_colon'  => __( 'Parent auto:', 'hexon' ),
			'all_items'          => __( 'Alle auto\'s', 'hexon' ),
			'view_item'          => __( 'Bekijk auto', 'hexon' ),
			'add_new_item'       => __( 'Auto toevoegen', 'hexon' ),
			'add_new'            => __( 'Auto toevoegen', 'hexon' ),
			'edit_item'          => __( 'Wijzig auto', 'hexon' ),
			'update_item'        => __( 'Update auto', 'hexon' ),
			'search_items'       => __( 'Zoek auto', 'hexon' ),
			'not_found'          => __( 'Niet gevonden', 'hexon' ),
			'not_found_in_trash' => __( 'Niet gevonden in prullenbak', 'hexon' ),
		];
		$args   = [
			'label'               => __( 'voorraad', 'hexon' ),
			'description'         => __( 'Voorraad', 'hexon' ),
			'labels'              => $labels,
			'supports'            => [ 'title', 'editor', 'thumbnail', 'custom-fields', 'author' ],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-store',
			'can_export'          => true,
			'rewrite'             => [ 'slug' => $slug . '/auto', 'with_front' => true ],
			'has_archive'         => $slug,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		];

		register_post_type( 'voorraad', $args );

		$labels_aanvragen = [
			'name'               => _x( 'Aanvragen', 'Post Type General Name', 'hexon' ),
			'singular_name'      => _x( 'Aanvraag', 'Post Type Singular Name', 'hexon' ),
			'menu_name'          => __( 'Aanvragen', 'hexon' ),
			'parent_item_colon'  => __( 'Parent Item:', 'hexon' ),
			'all_items'          => __( 'Aanvragen', 'hexon' ),
			'view_item'          => __( 'Bekijk Aanvraag', 'hexon' ),
			'add_new_item'       => __( 'Voeg Nieuw Aanvraag Toe', 'hexon' ),
			'add_new'            => __( 'Voeg Toe', 'hexon' ),
			'edit_item'          => __( 'Pas Aan', 'hexon' ),
			'update_item'        => __( 'Update Item', 'hexon' ),
			'search_items'       => __( 'Zoek Item', 'hexon' ),
			'not_found'          => __( 'Niet gevonden', 'hexon' ),
			'not_found_in_trash' => __( 'Niet gevonden in prullenbak', 'hexon' ),
		];
		$args_aanvragen   = [
			'label'               => __( 'aanvragen', 'hexon' ),
			'description'         => __( 'Aanvragen', 'hexon' ),
			'labels'              => $labels_aanvragen,
			'supports'            => [ 'title', 'editor', 'custom-fields' ],
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-megaphone',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'post',
		];
		//register_post_type( 'aanvragen', $args_aanvragen ); //Not needed for now

	}

	public function hexon_pre_get_posts( $query ) {
		if ( ! is_admin() && $query->is_main_query() && in_array( $query->get( 'post_type' ), [ 'voorraad', ] ) && empty( $query->get( 'car_search' ) ) ) {

			$meta_query = [];

			//Set standard meta_query, override later
			$meta_query['prijs'] = [
				'key'     => '_prijs',
				'value'   => - 1,
				'compare' => '>=',
				'type'    => 'NUMERIC',
			];

			$meta_query['merk'] = [
				'key'     => 'CarSearch',
				'value'   => - 1,
				'compare' => '>=',
			];

			$meta_query['bouwjaar'] = [
				'key'	  => 'BouwJaar',
				'value'	  => - 1,
				'compare' => '>=',
				'type'	  => 'NUMERIC'
			];

			$query->set( 'meta_query', $meta_query );

			if ( isset( $_REQUEST['sortering'] ) ) {

				$sort = $_REQUEST['sortering'];

				if ( $sort == 'prijs-oplopend' ) {
					$orderby = [
						'prijs' => 'ASC',
					];
				} elseif ( $sort == 'prijs-aflopend' ) {
					$orderby = [
						'prijs' => 'DESC',
					];
				} elseif ( $sort == 'datum-aflopend' ) {
					$orderby = [
						'date' => 'DESC',
					];
				} elseif ( $sort == 'datum-oplopend' ) {
					$orderby = [
						'date' => 'ASC',
					];
				} else if ( $sort == 'bouwjaar-oplopend') {
					$orderby = [
						'BouwJaar' => 'ASC'
					];
				} else if ( $sort == 'bouwjaar-aflopend') {
					$orderby = [
						'BouwJaar' => 'DESC'
					];
				} else if ( $sort == 'merken-aflopend') {
					$orderby = [
						'merk' => 'DESC'
					];
				} else if ( $sort == 'merken-oplopend') {
					$orderby = [
						'merk' => 'ASC'
					];
				}
			} else {
				$orderby = [
					'merk' => 'ASC'
				];
			}

			$query->set( 'orderby', $orderby );

			return;
		}
	}
}

new HexonPostType();
