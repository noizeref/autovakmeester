<?php

namespace Gravity\Hexon\Routes;

use Gravity\Hexon;

class HexonRoutes extends Hexon {

	public function __construct() {
		// Rewrites
		add_action( 'init', [ $this, $this->prefix . '_rewrites' ] );

	}

	public function hexon_rewrites() {

		//Rewrite tags for search pages
		add_rewrite_tag( '%hexon_search%', '([^/]+)' );
		add_rewrite_tag( '%car_search%', '([^/]+)' );
		add_rewrite_tag( '%car_post_type%', '([^/]+)' );
		add_rewrite_tag( '%hexon_radius%', '([^/]+)' );
		add_rewrite_tag( '%hexon_radius_term%', '([^/]+)' );
		add_rewrite_tag( '%garage_slug%', '([^/]+)' );
		add_rewrite_tag( '%car_slug%', '([^/]+)' );
		add_rewrite_tag( '%car_post_request%', '([^/]+)' );

		//Rewrite tags for API
		add_rewrite_tag( '%hexon%', '([^/]+)' );
		add_rewrite_tag( '%car_id%', '([^/]+)' );
		add_rewrite_tag( '%car_type%', '([^/]+)' );

		if ( get_option( 'hexon_slug' ) != '' ) {
			$slug = get_option( 'hexon_slug' );
		} else {
			$slug = 'voorraad';
		}

		//Rewrite rule for single occasions page
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/auto/([^&]+)/?$', 'index.php?garage_slug=$matches[1]&car_slug=$matches[2]&hexon_search=false&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/([^&]+)/page/([0-9]{1,})/?$', 'index.php?garage_slug=$matches[1]&car_search=$matches[2]&paged=$matches[3]&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/([^&]+)/?$', 'index.php?garage_slug=$matches[1]&car_search=$matches[2]&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/zoeken/?$', 'index.php?garage_slug=$matches[1]&hexon_search=true&car_post_type=voorraad&car_post_request=true&hexon=true', 'top');
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/page/([0-9]{1,})/?$', 'index.php?garage_slug=$matches[1]&paged=$matches[2]&hexon_search=false&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^vestiging/([^&]+)/' . $slug . '/?$', 'index.php?garage_slug=$matches[1]&hexon_search=false&car_post_type=voorraad', 'top' );

		//Rewrite rule for global occasions page
		add_rewrite_rule( '^' . $slug . '/zoeken/([^&]+)/page/([0-9]{1,})/?$', 'index.php?car_search=$matches[1]&paged=$matches[2]&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^' . $slug . '/zoeken/([^&]+)/?$', 'index.php?car_search=$matches[1]&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^' . $slug . '/zoeken/?$', 'index.php?hexon_search=true&car_post_type=voorraad&car_post_request=true&hexon=true', 'top' );

		add_rewrite_rule( '^hexon/api/' . $slug . '/?$', 'index.php?hexon=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^hexon/api/' . $slug . '/zoeken/([^&]+)/page/([0-9]{1,})/?$', 'index.php?car_search=$matches[1]&paged=$matches[2]&hexon=true&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^hexon/api/' . $slug . '/zoeken/([^&]+)/?$', 'index.php?car_search=$matches[1]&hexon=true&hexon_search=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^hexon/api/' . $slug . '/autocomplete/([^&]+)/?', 'index.php?car_search=$matches[1]&hexon=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^hexon/api/' . $slug . '/car/([^&]+)/?$', 'index.php?car_id=$matches[1]&hexon=true&car_post_type=voorraad', 'top' );
		add_rewrite_rule( '^hexon/api/' . $slug . '/type/([^&]+)/?$', 'index.php?car_type=$matches[1]&hexon=true&car_post_type=voorraad', 'top' );

		//Rewrite tags for processing cars
		add_rewrite_tag( '%hexon_parser%', '([^/]+)' );

		//Rewrite rule for processing cars
		add_rewrite_rule( '^hexon/voorraad/endpoint/?$', 'index.php?hexon_parser=true', 'top' );
	}

}

new HexonRoutes();
