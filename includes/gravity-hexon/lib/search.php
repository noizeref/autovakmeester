<?php

namespace Gravity\Hexon\Search;

use Gravity\Hexon;

class HexonSearch extends Hexon {
	public function get_search_results( $post_type ) {
		global $wp_query;

		$post_in           = [];
		$tax_query         = [];
		$meta_query        = [];
		$search_query      = '';
		$hexon_customer_id = null;



		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        error_log("TEST SEARCH");

		if ( isset( $_POST['hexon_set_search'] ) && $_POST['hexon_set_search'] == 1 ) { // Fetch the search $_POST


			if ( isset( $_POST['hexon_search']['term'] ) && ! empty( $_POST['hexon_search']['term'] ) ) {
				$search_term  = $_POST['hexon_search']['term'];
				$search_term  = str_replace( ' ', '+', $search_term );
				$search_query .= str_replace( ',', '', $search_term ) . '/';

			} else {
				$search_query .= 'alles' . '/';
			}

			if ( isset( $_POST['hexon_radius']['term'] ) && ! empty( $_POST['hexon_radius']['term'] ) ) {
				$search_radius_term = $_POST['hexon_radius']['term'];
				$search_radius_term = str_replace( ' ', '+', $search_radius_term );
				$search_query       .= str_replace( ',', '', $search_radius_term ) . '/';
			} else {
//				$search_query .= 'heel-nederland' . '/';
			}

			if ( isset( $_POST['hexon_radius'] ) && ! empty( $_POST['hexon_radius'] ) ) {
				$hexon   = new Hexon\Admin\HexonAdmin();
				$filters = $hexon->get_all_filters();

				$combined_filters = [];
				foreach ( $filters as $index => $key ) {
					$combined_filters[ $key ] = isset( $_POST['hexon_radius'][ $key ] ) ? $_POST['hexon_radius'][ $key ] : null;
				}


				foreach ( $combined_filters as $combined_filter ) {
					if ( ! empty( $combined_filter ) ) {
						if ( $combined_filter[0] != '+0km-afstand' ) {
							$filter_terms = implode( ',', $combined_filter );
							$search_query .= $filter_terms . '/';
						}
					} else {
						$search_query .= '';
					}
				}

			}

			if ( isset( $_POST[ 'hexon_soort_' . $post_type ] ) ) {
				$soorten = $_POST[ 'hexon_soort_' . $post_type ];

				if ( ! empty( $soorten ) ) {
					if ( is_array( $soorten ) ) {
						foreach ( $soorten as $type_key => $type_filter ) {
							if ( is_array( $type_filter ) ) {
								$filter_terms = implode( ',', $type_filter );
								$search_query .= $type_key . ',' . $filter_terms . '/';
							} else {
								$search_query .= $type_key . '/';
							}
						}
					} else {
						$search_query .= $soorten . '/';
					}
				}
			}

			if ( isset( $_POST[ 'hexon_financiering_' . $post_type ] ) ) {
				$types = $_POST[ 'hexon_financiering_' . $post_type ];

				if ( ! empty( $types ) ) {
					if ( is_array( $types ) ) {
						foreach ( $types as $type_key => $type_filter ) {
							if ( is_array( $type_filter ) ) {
								$filter_terms = implode( ',', $type_filter );
								$search_query .= $type_key . ',' . $filter_terms . '/';
							} else {
								$search_query .= $type_key . '/';
							}
						}
					} else {
						$search_query .= $types . '/';
					}
				}
			}

			if ( isset( $_POST['hexon_price'] ) && ! empty( $_POST['hexon_price'] ) ) {
				if ( is_array( $_POST['hexon_price'] ) ) {
					if ( ! isset( $_POST['hexon_price']['prijs_van'] ) ) {
						$search_query .= 0 . '-';
					}
					foreach ( $_POST['hexon_price'] as $type_key => $type_filter ) {
						if ( $type_key == 'prijs_van' ) {
							$search_query .= $type_filter[0] . '-';
						} else if ( $type_key == 'prijs_tot' ) {
							$search_query .= $type_filter[0];
						}
					}
					if ( ! isset( $_POST['hexon_price']['prijs_tot'] ) ) {
						$search_query .= 99999999;
					}
					$search_query .= '/';
				} else {
					$search_query .= $_POST['hexon_price'] . '/';
				}
			}

			if ( isset( $_POST[ 'hexon_filters_' . $post_type ] ) && ! empty( $_POST[ 'hexon_filters_' . $post_type ] ) ) {

				$hexon   = new Hexon\Admin\HexonAdmin();
				$filters = $hexon->get_all_filters();

				$combined_filters = [];
				foreach ( $filters as $index => $key ) {
					$combined_filters[ $key ] = isset( $_POST[ 'hexon_filters_' . $post_type ][ $key ] ) ? $_POST[ 'hexon_filters_' . $post_type ][ $key ] : null;
				}

				foreach ( $combined_filters as $combined_filter ) {
					if ( ! empty( $combined_filter ) ) {
						if ( $combined_filter[0] != '+0 km' ) {
							$filter_terms = implode( ',', $combined_filter );
							$search_query .= $filter_terms . '/';
						}
					} else {
						$search_query .= '';
					}
				}

			}

			if ( isset( $_POST['hexon_set_sort'] ) ) {
				$sort = '?sortering=' . $_POST['hexon_set_sort'];
			} else {
				$sort = '';
			}



			if(!empty($search_query)){
                if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
                    $search_url = get_bloginfo( 'url' ) . '/vestiging/' . $garage_slug . '/' . get_option( 'hexon_slug' ) . '/zoeken/' . $search_query . $sort;
                } else {
                    $search_url = get_bloginfo( 'url' ) . '/' . get_option( 'hexon_slug' ) . '/zoeken/' . $search_query . $sort;
                }
            }else {
                if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
                    $search_url = get_bloginfo( 'url' ) . '/vestiging/' . $garage_slug . '/' . get_option( 'hexon_slug' ) . $sort;
                } else {
                    $search_url = get_bloginfo( 'url' ) . '/' . get_option( 'hexon_slug' ) . $sort;
                }
            }


			wp_redirect( $search_url );
			exit;

		} else if ( ! empty( $wp_query->get( 'car_search' ) ) ) { // Handle the search redirect / request

			$search_cars = explode( "/", $wp_query->get( 'car_search' ) );

			//Set empty tax and meta query car
			$tax_query  = [];
			$meta_query = [];

			//Set standard meta_query, override later
			$meta_query['prijs'] = [
				'key'     => '_prijs',
				'value'   => - 1,
				'compare' => '>=',
				'type'    => 'NUMERIC'
			];

			$meta_query['merk'] = [
				'key'     => 'CarSearch',
				'value'   => - 1,
				'compare' => '>=',
			];

			$meta_query['bouwjaar'] = [
				'key'	  => 'CarDate',
				'value'	  => - 1,
				'compare' => '>=',
				'type'	  => 'NUMERIC'
			];
			
			//Get search term
			$search_query         = '';
			$search_address_query = '';
			$search_term          = isset($search_cars[0]) ? $search_cars[0] : '';
			$search_address_term  = isset($search_cars[1]) ? $search_cars[1] : '';

			if ( $search_term != 'alles' ) {
				$search_query = str_replace( '+', ' ', $search_term );
			}

			if ( $search_address_term != 'heel-nederland' ) {
				$search_address_query = str_replace( '+', ' ', $search_address_term );
			}

			$search_soorten = [];
			$soorten_term   = 'soort_' . $post_type;
			$soorten        = get_terms( [
				'taxonomy' => $soorten_term
			] );

			if ( ! isset( $soorten->errors ) ) {
				foreach ( $soorten as $soort ) {
					$search_soorten[] = $soort->slug;
				}
			}

			$search_types = [];
			$types_term   = 'financiering_' . $post_type;
			$types        = get_terms( [
				'taxonomy' => $types_term
			] );
			if ( ! isset( $types->errors ) ) {
				foreach ( $types as $type ) {
					$search_types[] = $type->slug;
				}
			}

			if ( ! empty( $search_cars ) ) {

				//Combine the array
				$combined_array = $search_cars;

				foreach ( $combined_array as $search_value ) {

					$search_key = '';

					$price_range = false;
					$mileage_range = false;
					$year_range = false;

					if ( ! strpos( $search_value, 'voorraad' ) ) {
						$price_value = explode( '-', $search_value );
						if ( is_numeric( $price_value[0] ) && isset( $price_value[1] ) && is_numeric( $price_value[1] ) && $price_value[2] != 'deuren' ) {
							$price_range = true;
						} else if ($price_value[1] == 'km' && $price_value[3] == 'km') {
							$mileage_range = true;
						} else if ($price_value[1] == 'year' && $price_value[3] == 'year') {
							$year_range = true;
						}
					}

					if ( $price_range ) {
						$price_from = (int) $price_value[0];
						$price_to   = (int) $price_value[1];
						$search_key = 'prijs';
					} else if ( $mileage_range ) {
						$mileage_from = (int) $price_value[0];
						$mileage_to   = (int) $price_value[2];
						$search_key = 'kilometerstand';
					} else if ( $year_range ) {
						$year_from = (int) $price_value[0];
						$year_to   = (int) $price_value[2];
						$search_key = 'BouwJaar';
					} else if ( strpos( $search_value, 'km-afstand' ) ) {
						$search_key = 'straal';
					} else if ( in_array( $search_value, $search_soorten ) ) {
						$search_key = 'soort';
					} else if ( in_array( $search_value, $search_types ) ) {
						$search_key = 'financiering';
					} else if ( strpos( $search_value, 'deuren' ) ) {
						$search_key = 'deuren';
					}


					if ( $search_value ) {

						if ( $search_key == 'straal' ) {
							if ( $search_address_term != 'heel-nederland' ) {

								$ids          = $this->get_property_radius_ids( $post_type );
								$search_value = preg_replace( '/[^\d]+/i', '', $search_value );

								// Set query var
								set_query_var( 'hexon_radius', $search_value );
								$hexon_radius = $search_value;

								// Force Google to search within our beautiful country
								if ( ! stripos( $search_address_query, 'nederland' ) && ! stripos( $search_address_query, 'holland' ) && ! stripos( $search_address_query, 'netherlands' ) ) {
									$google_search_query = $search_address_query . ', Nederland';
								} else {
									$google_search_query = $search_address_query;
								}

								// get address field lat and long
								$jsonurl  = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode( $google_search_query ) . "&sensor=false&key=AIzaSyDQTKWoMScsLucFnTZzKE6-ersHE3a3TMw";
								$response = file_get_contents( $jsonurl );

								if ( ! empty( $response ) ) {
									$output = json_decode( $response );

									if ( ! empty( $output->results ) ) {

										$lat = $output->results[0]->geometry->location->lat;
										$lng = $output->results[0]->geometry->location->lng;

										$garage_ids = [];
										if ( ! empty( $ids ) && is_array( $ids ) ) {
											foreach ( $ids as $id ) {
												$dist = $this->check_distance( $lat, $lng, $id['lat'], $id['long'] );

												if ( ! empty( $dist ) ) {
													if ( $dist <= $search_value ) {
														$garage_ids[] = $id['id'];
													}
												}
											}
										}

										if ( ! empty( $garage_ids ) ) {
											$post_in = $garage_ids;
										} else {
											$post_in = [ 0 ];
										}

										//Set new search_query
										$hexon_radius_term = $search_address_query;

										//Clear the search term because we are searching on radius
										$search_address_query = '';

									}

								}

							}

						} else if ( $search_key == 'soort' || $search_key == 'financiering' ) {

							$search_key = $search_key . '_' . $post_type;

							$search_value = explode( ',', $search_value );
							if ( sizeof( $search_value ) > 1 ) {
								array_shift( $search_value );
							}
							$tax_query[] = [
								'taxonomy' => $search_key,
								'field'    => 'slug',
								'terms'    => $search_value
							];

						}  else if ( $search_key == 'prijs' ) {
							$meta_query[ $search_key ] = [
								'key'     => '_' . $search_key,
								'value'   => [ $price_from, $price_to ],
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							];
						} else if ( $search_key == 'BouwJaar') {
							$meta_query[ $search_key ] = [
								'key'     => 'BouwJaar',
								'value'   => [ $year_from, $year_to ],
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							];
						} else if ($search_key == 'kilometerstand') {
							$meta_query[ $search_key ] = [
								'key'     => '_' . $search_key,
								'value'   => [ $mileage_from, $mileage_to ],
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							];
						} else if ($search_key == 'deuren') {
							$search_value = explode( ',', $search_value );
							$merk_model   = get_term_by( 'slug', 'aantal-deuren', 'filters_voorraad' );
							$parents      = [];
	
							foreach ( $search_value as $value ) {
								$term = get_term_by( 'slug', $value, 'filters_voorraad' );
								if ( $term->parent == $merk_model->term_id ) {
									$parents[] = $term->term_id;
								}
							}
	
							foreach ( $search_value as $value ) {
								$term = get_term_by( 'slug', $value, 'filters_voorraad' );
								if ( in_array( $term->parent, $parents ) ) {
									$parent = get_term_by( 'id', $term->parent, 'filters_voorraad' );
									if ( ( $key = array_search( $parent->slug, $search_value ) ) !== false ) {
										unset( $search_value[ $key ] );
									}
								}
							}
	
							$tax         = 'filters_' . $post_type;
							$tax_query[] = [
								'taxonomy' => $tax,
								'field'    => 'slug',
								'terms'    => $search_value
							];
	
						} else {

							$search_value = explode( ',', $search_value );


							$merk_model   = get_term_by( 'slug', 'merk-model', 'filters_voorraad' );
							$parents      = [];

							foreach ( $search_value as $value ) {
								$term = get_term_by( 'slug', $value, 'filters_voorraad' );
								if ( $term->parent == $merk_model->term_id ) {
									$parents[] = $term->term_id;
								}
							}


							foreach ( $search_value as $value ) {
								$term = get_term_by( 'slug', $value, 'filters_voorraad' );

								if ( in_array( $term->parent, $parents ) ) {
									$parent = get_term_by( 'id', $term->parent, 'filters_voorraad' );
									if ( ( $key = array_search( $parent->slug, $search_value ) ) !== false ) {
										unset( $search_value[ $key ] );
									}
								}
							}
							$tax         = 'filters_' . $post_type;
							$tax_query[] = [
								'taxonomy' => $tax,
								'field'    => 'slug',
								'terms'    => $search_value,
                                'operator' => 'IN'
							];

						}

					}
				}

				if ( sizeof( $meta_query ) > 1 ) {
					$meta_query['relation'] = 'AND';
				}

				if ( sizeof( $tax_query ) > 1 ) {
					$tax_query['relation'] = 'OR ';
				}

			}

			if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
				$garage = $this->get_page_by_post_name( $garage_slug, OBJECT, 'garage' );
				if ( $garage ) {
					$hexon_customer_id = get_field( 'hexon_klantennummer', $garage->ID );
				}
			}

		} else if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
			$garage = $this->get_page_by_post_name( $garage_slug, OBJECT, 'garage' );

			if ( $garage ) {
				if ( $hexon_id = get_field( 'hexon_klantennummer', $garage->ID ) ) {
					$hexon_customer_id = $hexon_id;
				} else {
					$this->is_404();
				}
			} else { // Return 404 page, if garage not found
				$this->is_404();
			}
		} else { // Return 404 page, nothing to do
			if ( $wp_query->get( 'car_post_request' ) ) {
//				$this->is_404();
			} else {

			}
		}

		if ( $wp_query->get( 'car_search' ) ) {
			$car_search = $wp_query->get( 'car_search' );
		} else {
			$car_search = null;
		}

		if ( $wp_query->get( 'hexon_price_from' ) !== '' ) {
			$hexon_price_from = $wp_query->get( 'hexon_price_from' );
		}

		if ( $wp_query->get( 'hexon_price_to' ) !== '' ) {
			$hexon_price_to = $wp_query->get( 'hexon_price_to' );
		}

		if ( isset( $hexon_customer_id ) ) {
			$meta_query['hexon_klantennummer'] = [
				'key'     => 'Klantnummer',
				'value'   => $hexon_customer_id,
				'compare' => '=',
			];
		}
        $order_type = 'meta_value';
		if ( isset( $_REQUEST['sortering'] ) ) {
			$sort = $_REQUEST['sortering'];

			if ( $sort == 'prijs-oplopend' ) {
				$orderby = [
					'prijs' => 'ASC'
				];
                $order_type = 'meta_value_num';

            } else if ( $sort == 'prijs-aflopend' ) {
				$orderby = [
					'prijs' => 'DESC'
				];
                $order_type = 'meta_value_num';

			} else if ( $sort == 'datum-aflopend' ) {
				$orderby = [
					'CarUpdated' => 'DESC'
				];
                $order_type = 'meta_value_num';
            } else if ( $sort == 'datum-oplopend' ) {
				$orderby = [
					'CarUpdated' => 'ASC'
				];
                $order_type = 'meta_value_num';

            } else if ( $sort == 'oppervlakte-oplopend' ) {
				$orderby = [
					'oppervlakte' => 'ASC'
				];
			} else if ( $sort == 'oppervlakte-aflopend' ) {
				$orderby = [
					'oppervlakte' => 'DESC'
				];
			} else if ( $sort == 'bouwjaar-oplopend') {
				$orderby = [
					'BouwJaar' => 'ASC'
				];
			} else if ( $sort == 'bouwjaar-aflopend') {
				$orderby = [
					'BouwJaar' => 'DESC'
				];
			} else if ( $sort == 'merken-aflopend') {
				$orderby = [
					'merk' => 'DESC'
				];
                $order_type  = 'title';

            } else if ( $sort == 'merken-oplopend') {
				$orderby = [
					'merk' => 'ASC'
				];
				$order_type  = 'title';
			}
		} else {
			$orderby = [
				'merk' => 'ASC'
			];
            $order_type  = 'title';
        }






		$meta_key = array_keys($orderby)[0];
		$order = $orderby[array_keys($orderby)[0]];


		$args = [
            'post_type'  => $post_type,
			'tax_query'  => $tax_query,
			'meta_query' => $meta_query,
			'paged'      => $paged,
//			'orderby'    => $orderby,
            'orderby'    => $order_type,
            'order'      => $order,
			'post__in'   => $post_in
		];

        if($order_type != 'title'){
            $args['meta_key'] = $meta_key;
        }


		query_posts( $args );

		foreach ( $tax_query as $tax ) {
			if ( isset( $tax['taxonomy'] ) ) {
				$var   = $tax['taxonomy'];
				$value = reset( $tax['terms'] );
				set_query_var( $var, $value );
			}
		}

		if ( isset( $hexon_price_from ) ) {
			set_query_var( 'hexon_price_from', $hexon_price_from );
		}

		if ( isset( $hexon_price_to ) ) {
			set_query_var( 'hexon_price_to', $hexon_price_to );
		}

		if ( $car_search ) {
			set_query_var( 'car_search', $car_search );
		}

		if ( $post_type ) {
			set_query_var( 'car_post_type', $post_type );
		}

		if ( isset( $hexon_radius ) ) {
			set_query_var( 'hexon_radius', $hexon_radius );
		}

		if ( isset( $hexon_radius_term ) ) {
			set_query_var( 'hexon_radius_term', $hexon_radius_term );
		}

		if ( isset( $garage_slug ) ) {
			set_query_var( 'garage_slug', $garage_slug );
		}

		return $args;
	}

	public function get_property_radius_ids( $post_type ) {

		$args = [
			'posts_per_page' => - 1,
			'post_type'      => $post_type,
			'post_status'    => 'publish',
			'fields'         => 'ids'
		];

		$properties = new \WP_Query( $args );

		$ids = [];
		if ( $properties->have_posts() ) :
			while ( $properties->have_posts() ) : $properties->the_post();
				$latitude  = get_post_meta( get_the_ID(), 'Latitude', true );
				$longitude = get_post_meta( get_the_ID(), 'Longitude', true );
				if ( ! empty( $latitude ) && ! empty( $longitude ) ) {
					$ids[] = [
						'lat'  => $latitude,
						'long' => $longitude,
						'id'   => get_the_ID()
					];
				}
			endwhile;
		endif;

		return $ids;
	}

	public function check_distance( $lat1, $lng1, $lat2, $lng2, $miles = false ) {
		$pi80 = M_PI / 180;
		$lat1 *= $pi80;
		$lng1 *= $pi80;
		$lat2 *= $pi80;
		$lng2 *= $pi80;

		$r    = 6372.797; // mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlng = $lng2 - $lng1;
		$a    = sin( $dlat / 2 ) * sin( $dlat / 2 ) + cos( $lat1 ) * cos( $lat2 ) * sin( $dlng / 2 ) * sin( $dlng / 2 );
		$c    = 2 * atan2( sqrt( $a ), sqrt( 1 - $a ) );
		$km   = $r * $c;

		$distance = ( $miles ? ( $km * 0.621371192 ) : $km );

		return $distance;
	}

	public function get_page_by_post_name( $post_name, $output = OBJECT, $post_type = 'post' ) {
		$args = [
			'name'           => $post_name,
			'post_type'      => $post_type,
			'posts_per_page' => 1,
		];

		$query = new \WP_Query( $args );

		if ( $query->post_count > 0 ) {
			return reset( $query->posts );
		} else {
			return null;
		}

	}

	public function is_404() {
		global $wp_query;
		$wp_query->set_404();

		status_header( 404 );
		get_template_part( 404 );

		exit();
	}

}
