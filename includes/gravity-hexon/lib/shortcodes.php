<?php

namespace Gravity\Hexon\Shortcodes;

use Gravity\Hexon;
use Gravity\Hexon\Admin;

class HexonShortcodes extends Hexon {
	private $object_ids = [];

	public function __construct() {
		add_shortcode( 'hexon_voorraad_sort', [ $this, 'voorraad_sort' ] );
		add_shortcode( 'hexon_voorraad_filters', [ $this, 'voorraad_filters' ] );
		add_shortcode( 'hexon_aanvraag_form', [ $this, 'aanvraag_form' ] );
	}

	public function set_object_ids( $post_type, $hexon_id = null ) {
		if ( $hexon_id ) {
			$objects = new \WP_Query( [
				'post_type'      => $post_type,
				'posts_per_page' => - 1,
				'post_status'    => 'publish',
				'meta_query'     => [
					'hexon_klantennummer' => [
						'key'     => 'Klantnummer',
						'value'   => $hexon_id,
						'compare' => '=',
					]
				],
				'fields'         => 'ids'
			] );
		} else {
			$objects = new \WP_Query( [
				'post_type'      => $post_type,
				'posts_per_page' => - 1,
				'fields'         => 'ids'
			] );
		}

		$this->object_ids = $objects->posts;
	}

	public function voorraad_sort() {

		$output = '';

		if ( isset( $_REQUEST['sortering'] ) ) {
			$sort = $_REQUEST['sortering'];
		} else {
			$sort = 'merken-oplopend';
		}

		$types = [
			['value' => 'datum-aflopend', 'name'  => 'Datum ↓'],
			['value' => 'datum-oplopend', 'name'  => 'Datum ↑'],
			['value' => 'prijs-oplopend', 'name'  => 'Prijs ↑'],
			['value' => 'prijs-aflopend', 'name'  => 'Prijs ↓'],
			['value' => 'bouwjaar-aflopend', 'name'  => 'Bouwjaar ↓'],
			['value' => 'bouwjaar-oplopend', 'name'  => 'Bouwjaar ↑'],
			['value' => 'merken-oplopend', 'name'  => 'Merken ↑'],
			['value' => 'merken-aflopend', 'name'  => 'Merken ↓'],
		];

		$output .= '<form method="get">';
		$output .= '<select name="sortering" onchange="this.form.submit()" class="select__input">';
		foreach ( $types as $type ) {
			$output .= ($sort == $type['value'])
						? '<option value="' . $type['value'] . '" selected="selected">' . $type['name'] . '</option>'
						: '<option value="' . $type['value'] . '">' . $type['name'] . '</option>';
		}
		$output .= '</select>';
		$output .= '</form>';

		return $output;
	}

	public function sort_filters( &$array, $key ) {
		if ( ! empty( $array ) ) {
			$array = json_decode( json_encode( $array ), true );

			$sorter = [];
			$ret    = [];
			reset( $array );
			foreach ( $array as $ii => $va ) {
				if ( isset( $va[ $key ] ) ) {
					$sorter[ $ii ] = $va[ $key ];
				}
			}
			asort( $sorter );
			foreach ( $sorter as $ii => $va ) {
				if ( isset( $array[ $ii ] ) ) {
					$ret[ $ii ] = $array[ $ii ];
				}
			}
			$array = $ret;

			return json_decode( json_encode( $array ), false );
		}

		return null;
	}

	public function voorraad_filters() {
		global $wp_query;

		$output         = '';
		$filters_output = '';
		$post_type      = $wp_query->get( 'car_post_type' );

		if (!$post_type) { $post_type = $wp_query->get( 'post_type' ); }

		if (!$post_type) {
			$post_type = explode( '_', $wp_query->get( 'taxonomy' ) );
			$post_type = end( $post_type );
		}

		if (!$post_type) { $post_type = 'voorraad'; }

		if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
			$search_url = get_bloginfo( 'url' ) . '/vestiging/' . $garage_slug . '/' . get_option( 'hexon_slug' ) . '/zoeken/';
		} else {
			$search_url = get_bloginfo( 'url' ) . '/' . get_option( 'hexon_slug' ) . '/zoeken/';
		}

		if ( $garage_slug ) {
			$hexon_customer_id = null;
			$search            = new Hexon\Search\HexonSearch();
			$garage            = $search->get_page_by_post_name( $garage_slug, OBJECT, 'garage' );

			if ( $hexon_customer_id = get_field( 'hexon_klantennummer', $garage->ID ) ) {
				$this->set_object_ids( $post_type, $hexon_customer_id );
			}
		}

		if ( empty( $this->object_ids ) ) {
			// $this->set_object_ids( $post_type ); // This leads to performance issues on production
		}

		$hexon         = new Admin\HexonAdmin();
		$filters       = $hexon->get_filters();
		$current_terms = [];
		if ( isset( $wp_query->query_vars['tax_query'] ) ) {
			foreach ( $wp_query->query_vars['tax_query'] as $tax_query ) {
				if ( isset( $tax_query['terms'] ) ) {
					foreach ( $tax_query['terms'] as $term ) {
						$current_terms[] = $term;
					}
				}
			}
		}

		$current_meta = [];
		if ( isset( $wp_query->query_vars['meta_query'] ) ) {
			foreach ( $wp_query->query_vars['meta_query'] as $meta_query ) {
				if ( is_array( $meta_query ) ) {
					if ( count( $meta_query ) > 1 ) {
						$meta_array = $meta_query;
						if ( is_array( $meta_array ) ) {
							if ( isset( $meta_array['value'] ) && isset( $meta_array['key'] ) ) {

								if ( $meta_array['value'] == 1 ) {
									$meta_array['key'] = substr( $meta_array['key'], 0, - 1 );
								}
								$meta_array['key'] = substr( $meta_array['key'], 1 );
								if ( $meta_array['key'] != 'prijs' ) {
									$current_meta[] = $meta_array['value'] . '-' . $meta_array['key'];
								} else {
									$current_meta[] = $meta_array['value'];
								}
							}
						}
					} else {
						if ( $meta_query['value'] == 1 ) {
							$meta_query['key'] = substr( $meta_query['key'], 0, - 1 );
						}
						$meta_query['key'] = substr( $meta_query['key'], 1 );
						if ( $meta_query['key'] != 'prijs' ) {
							$current_meta[] = $meta_query['value'] . '-' . $meta_query['key'];
						} else {
							$current_meta[] = $meta_query['value'];
						}
					}
				}
			}
		}

		$submit = false;

		foreach ( $filters as $filter ) {
			if ( $filter == 'straal' ) {
				$filters_output .= $this->build_radius_output();
			} else if ( $filter == 'soort' ) {
				$filters_output .= $this->build_checkbox_type_output( 'soort_' . $post_type, $current_terms, $submit, 'sidebar' );
			} else if ( $filter == 'financiering' ) {
				$filters_output .= $this->build_checkbox_type_output( 'financiering_' . $post_type, $current_terms, $submit, 'sidebar' );
			} else if ( $filter == 'prijs' ) {
				$filters_output .= $this->build_price_range_output( $submit );
			} else if ( $filter == 'kilometerstand') {
				$filters_output .= $this->build_mileage_range_output( $submit );
			} else if ( $filter == 'vermogen') {
				$filters_output .= $this->build_power_range_output( $sumbit );
			} else if ( $filter == 'bouwjaar') {
				$filters_output .= $this->build_construction_year_range_output( $submit );
			} else if ( $filter != 'apk' ) {
				$filter = get_term_by( 'slug', $filter, 'filters_' . $post_type );
				if ( $filter ) {
					$filters_output .= $this->build_checkbox_output( $filter, 'filters_' . $post_type, $current_terms, $current_meta, $submit, 'sidebar' );
				}
			}
		}

		$hidden = '';
		$types = get_terms(['taxonomy' => 'financiering_' . $post_type, 'parent' => 0]);
		$soorten = get_terms(['taxonomy' => 'soort_' . $post_type, 'parent' => 0]);

		foreach ( $types as $type ) {
			$subtypes = get_terms(['taxonomy' => 'financiering_' . $post_type, 'parent' => (int) $type->term_id]);

			if ( in_array( $type->slug, $current_terms ) ) {
				$hidden .= '<input type="hidden" name="hexon_financiering" value="' . $type->slug . '" />';
			}
			foreach ( $subtypes as $subtype ) {
				if ( in_array( $subtype->slug, $current_terms ) ) {
					$hidden .= '<input type="hidden" name="hexon_financiering[' . $type->slug . '][]" value="' . $subtype->slug . '" />';
				}
			}
		}

		foreach ( $soorten as $soort ) {
			$subsoorten = get_terms(['taxonomy' => 'soort_' . $post_type, 'parent' => (int) $soort->term_id]);

			if ( in_array( $soort->slug, $current_terms ) ) {
				$hidden .= '<input type="hidden" name="hexon_soort" value="' . $soort->slug . '" />';
			}
			foreach ( $subsoorten as $subsoort ) {
				if ( in_array( $subsoort->slug, $current_terms ) ) {
					$hidden .= '<input type="hidden" name="hexon_soort[' . $soort->slug . '][]" value="' . $subsoort->slug . '" />';
				}
			}
		}

		$hidden .= get_query_var('s') ? '<input type="hidden" name="hexon_search[term]" value="' . get_query_var('s') . '" />' : '';

		if ( $wp_query->get( 'car_search' ) ) {
			$hidden .= '<input type="hidden" name="hexon_set_car_search" value="' . $wp_query->get( 'car_search' ) . '"/>';
		}

		if ( $garage_slug ) {
			$hidden .= '<input type="hidden" name="garage_slug" value="' . $garage_slug . '"/>';
		}

		if ( isset( $_REQUEST['sortering'] ) ) {
			$hidden .= '<input type="hidden" name="hexon_set_sort" value="' . $_REQUEST['sortering'] . '"/>';
		}

		$s = get_query_var('s') ? 'value="' . get_query_var('s') . '"' : '';

		$filters_output .= '<div class="filter filter-search">
			<header class="filter-trigger"><h5 class="filter__title">Zoeken</h5></header>
			<div class="search-holder">
				<input type="search" name="hexon_search[term]" id="hexon-auto-complete" class="hero__search-input" placeholder="Zoeken op kenmerken..." ' . $s . ' data-type="' . $post_type . '"/>
			</div>
		</div>';

		$filters_output .= '<button type="button" class="button button--red button--fullwidth">Zoeken<i class="icon-angle-right"></i></button>';

		$output .= '<form action="' . $search_url . '" method="post" class="hexon-filters">
			' . $hidden . '
			<input type="hidden" name="hexon_set_search" value="1" />
            <div class="row">
                <div class="col-sm-12">
                	' . $filters_output . '
                </div>
            </div>
        </form>';

		return $output;
	}

	public function build_checkbox_type_output( $term_slug = 'type', $current_terms = [], $submit = false, $prefix = '' ) {
		$filters_output = '';

		$term_query = new \WP_Term_Query( [
			'taxonomy'   => $term_slug,
			'object_ids' => $this->object_ids,
			'parent'     => 0,
			'order'      => 'ASC',
			'hide_empty' => true,
		] );

		$filter_children = $this->sort_filters( $term_query->terms, 'order' );

		if ( $prefix ) {
			$prefix = $prefix . '_';
		}

		if ( $filter_children ) {

			$filters_output .= '<div class="filter filter-group-' . $term_slug . '">';
			$filters_output .= '<header class="filter-trigger">';
			if ( $term_slug == 'soort_voorraad' ) {
				$filters_output .= '<h5 class="filter__title">Soort auto</h5>';
			} else {
				$filters_output .= '<h5>' . ucfirst( explode( '_', $term_slug )[0] ) . '</h5>';
			}
			$filters_output .= '</header>';
			$filters_output .= '<div class="filter-dropdown">';

			foreach ( $filter_children as $child ) {

				$parent_term = $child;

				$term_query = new \WP_Term_Query( [
					'child_of'   => (int) $parent_term->term_id,
					'taxonomy'   => $term_slug,
					'object_ids' => $this->object_ids,
					'order'      => 'ASC',
					'hide_empty' => true,
				] );

				$filter_children = $this->sort_filters( $term_query->terms, 'name' );
				$filters_output  .= '<div class="sub-filter sub-filter-group-' . $parent_term->slug . '">';
				$filters_output  .= '<header>';
				$filters_output  .= '<h5 class="filter__title">';

				$checked = '';

				if ( ! empty( $current_terms ) ) {
					if ( in_array( $parent_term->slug, $current_terms, true ) ) {
						$checked = 'checked="checked"';
					}
				}

				if ( $submit ) {
					$submit = 'onchange="this.form.submit()"';
				} else {
					$submit = '';
				}

				$filters_output .= '<label for="' . $prefix . 'hexon_' . $term_slug . '_' . $parent_term->slug . '">';
				$filters_output .= '<input type="radio" name="hexon_' . $term_slug . '" id="' . $prefix . 'hexon_' . $term_slug . '_' . $parent_term->slug . '" value="' . $parent_term->slug . '" ' . $checked . ' ' . $submit . ' />';
				$filters_output .= '<span class="checkbox__circle">
										<svg class="checkbox__icon"><use xlink:href="#icon-check" /></svg>
									</span>
									<span class="checkbox__text">' . $parent_term->name . '</span>';
				$filters_output .= '</label>';
				$filters_output .= '</h5>';
				$filters_output .= '</header>';
				if ( $filter_children ) {
					$filters_output .= '<ul>';
					$f              = 0;
					foreach ( $filter_children as $child ) {
						$term = get_term_by( 'id', (int) $child->term_id, $term_slug );
						$f ++;
						$class = $f > 5 ? 'collapsed' : '';

						$filters_output .= '<li class="filter-' . $term->slug . ' ' . $class . '">';
						$filters_output .= '<div class="checkbox">';

						$suffix  = '';
						$type    = 'checkbox';
						$checked = '';

						if ( ! empty( $current_terms ) ) {
							if ( in_array( $term->slug, $current_terms, true ) ) {
								$checked = 'checked="checked"';
							}
						}

						$filters_output .= '<input type="' . $type . '" name="hexon_' . $term_slug . '[' . $parent_term->slug . '][]" id="' . $prefix . 'hexon_' . $term_slug . '_' . $parent_term->slug . '_' . $term->slug . '" value="' . $term->slug . '" ' . $checked . ' ' . $submit . ' class="checkbox__checkbox" />';
						$filters_output .= '<label class="checkbox__label" for="' . $prefix . 'hexon_' . $term_slug . '_' . $parent_term->slug . '_' . $term->slug . '">';
						$filters_output .= '<span class="checkbox__circle">
										<svg class="checkbox__icon"><use xlink:href="#icon-check" /></svg>
									</span>
									<span class="checkbox__text">' . $term->name . $suffix . '</span> ';
						$filters_output .= '</label>';
						$filters_output .= '</div>';
						$filters_output .= '</li>';
					}
					$filters_output .= '</ul>';
					if ( $f > 5 ) {
						$filters_output .= '<a href="javascript:void(0)" class="show-more">+ Toon meer</a>';
					}
				}
				$filters_output .= '</div>';

			}
			$filters_output .= '</div>';
			$filters_output .= '</div>';
		}

		return $filters_output;
	}

	public function build_checkbox_output( $filter, $term_slug = 'filters', $current_terms = [], $current_meta = [], $submit = false, $prefix = '' ) {
		$filters_output = '';

		$term_query = new \WP_Term_Query( [
			'child_of'   => (int) $filter->term_id,
			'parent'     => (int) $filter->term_id,
			'taxonomy'   => $term_slug,
			'object_ids' => $this->object_ids,
			'order'      => 'ASC',
			'hide_empty' => true,
		] );

		$filter_children = $this->sort_filters( $term_query->terms, 'name' );

		if ( $prefix ) {
			$prefix = $prefix . '_';
		}

		if ( $filter_children ) {

			$filters_output .= '<div class="filter filter-group-' . $filter->slug . '">';
			$filters_output .= '<header class="filter-trigger">';
			$filters_output .= '<h5 class="filter__title">' . $filter->name . '</h5>';
			$filters_output .= '</header>';

			$filters_output .= '<ul class="filter-dropdown">';
			$f              = 0;
			foreach ( $filter_children as $child ) {
				$term = get_term_by( 'id', (int) $child->term_id, $term_slug );

				$f++;
				$class = $f > 5 ? 'collapsed' : '';

				$filters_output .= '<li class="filter-' . $term->slug . ' ' . $class . '">';
				$filters_output .= '<div class="checkbox">';

				$suffix         = '';
				$checked        = '';
				$parent_checked = '';

				$type = $filter->slug == 'nap' ? 'checkbox' : 'checkbox';

				if ( ! empty( $current_terms ) ) {
					if ( in_array( $term->slug, $current_terms, true ) ) {
						$checked = 'checked="checked"';
					}
				}

				$merk_model    = get_term_by( 'slug', 'merk-model', 'filters_voorraad' );
				$merk_children = new \WP_Term_Query( [
					'child_of'   => (int) $merk_model->term_id,
					'parent'     => (int) $merk_model->term_id,
					'taxonomy'   => $term_slug,
					'object_ids' => $this->object_ids,
					'order'      => 'ASC',
					'hide_empty' => true,
				] );

				$parents = [];
				foreach ( $merk_children->terms as $merk ) {
					$parents[] = $merk->term_id;
				}

				foreach ( $current_terms as $current_term ) {
					$child_term = get_term_by( 'slug', $current_term, 'filters_voorraad' );
					if ( ! empty( $child_term ) ) {
						if ( in_array( $child_term->parent, $parents ) ) {
							$parent = get_term_by( 'id', $child_term->parent, 'filters_voorraad' );
							if ( $parent->slug == $term->slug ) {
								$parent_checked = 'checked="checked"';
							}
						}
					}
				}

				if ( ! empty( $current_meta ) ) {
					if ( in_array( $term->slug, $current_meta, true ) ) {
						$checked = 'checked="checked"';
					} else if ( is_numeric( $term->slug ) ) {
						$term->slug = (int) $term->slug;
						if ( in_array( $term->slug, $current_meta, true ) ) {
							$checked = 'checked="checked"';
						}
					}
				}

				$submit = $submit ? 'onchange="this.form.submit()"' : '';
				$filters_output .= '<input type="' . $type . '"
										   name="hexon_' . $term_slug . '[' . $filter->slug . '][]"
										   id="' . $prefix . 'hexon_filters_' . $filter->slug . '_' . $term->slug . '"
										   value="' . $term->slug . '" ' . $checked . ' ' . $submit . '
										   data-radio-list="' . $term->slug . '"
										   class="checkbox__checkbox"/>';
				$filters_output .= '<label class="checkbox__label" for="' . $prefix . 'hexon_filters_' . $filter->slug . '_' . $term->slug . '">';
				$filters_output .= '<span class="checkbox__circle">
										<svg class="checkbox__icon"><use xlink:href="#icon-check" /></svg>
									</span>
									<span class="checkbox__text">' . $term->name . $suffix . '</span> ';
				$filters_output .= '</label>';
				$filters_output .= '</div>';

				$filter_children = new \WP_Term_Query( [
					'child_of'   => (int) $term->term_id,
					'parent'     => (int) $term->term_id,
					'taxonomy'   => $term_slug,
					'object_ids' => $this->object_ids,
					'order'      => 'ASC',
					'hide_empty' => true,
				] );

				$filter_children = $this->sort_filters( $filter_children->terms, 'name' );

				if ( $filter_children ) {
					$filters_output .= '<ul class="filter-dropdown-sub">';

					foreach ( $filter_children as $child ) {
						$term = get_term_by( 'id', (int) $child->term_id, $term_slug );

						$filters_output .= '<li class="filter-' . $term->slug . '">';
						$filters_output .= '<div class="checkbox">';

						$suffix  = '';
						$checked = '';

						$type = $filter->slug == 'nap' ? 'checkbox' : 'checkbox';

						if ( ! empty( $current_terms ) ) {
							if ( in_array( $term->slug, $current_terms, true ) ) {
								$checked = 'checked="checked"';
							}
						}

						if ( ! empty( $current_meta ) ) {
							if ( in_array( $term->slug, $current_meta, true ) ) {
								$checked = 'checked="checked"';
							} else if ( is_numeric( $term->slug ) ) {
								$term->slug = (int) $term->slug;
								if ( in_array( $term->slug, $current_meta, true ) ) {
									$checked = 'checked="checked"';
								}
							}
						}

						$submit = $submit ? 'onchange="this.form.submit()"' : '';
						$filters_output .= '<input type="' . $type . '"
												   name="hexon_' . $term_slug . '[' . $filter->slug . '][]"
												   id="' . $prefix . 'hexon_filters_' . $filter->slug . '_' . $term->slug . '"
												   value="' . $term->slug . '" ' . $checked . ' ' . $submit . '
												   class="checkbox__checkbox"/>';
						$filters_output .= '<label class="checkbox__label" for="' . $prefix . 'hexon_filters_' . $filter->slug . '_' . $term->slug . '">';
						$filters_output .= '<span class="checkbox__circle">
										<svg class="checkbox__icon"><use xlink:href="#icon-check" /></svg>
									</span>
									<span class="checkbox__text">' . $term->name . $suffix . '</span> ';
						$filters_output .= '</label>';
						$filters_output .= '</div>';
						$filters_output .= '</li>';
					}

					$filters_output .= '</li>';
					$filters_output .= '</ul>';
				}

			}
			$filters_output .= '</ul>';

			if ( $f > 5 ) { $filters_output .= '<a href="javascript:void(0)" class="show-more">+ Toon meer</a>'; }

			$filters_output .= '</div>';
		}

		if ($filter->slug == 'nap') {
			$script = 
			"<script>
				let checkbox = $(\"input[name='hexon_filters_voorraad[nap][]']\");
		
				checkbox.change(function() {
					if ($(this).prop('checked') == true) {
						let current = this;
						jQuery.each( checkbox, function( index, value ){
							if( $('#'+value.id).prop('checked') == true && value.id != current.id){
								jQuery('#'+value.id).trigger('click');
							}
						});
					}
				});
			</script>";

			$filters_output .= $script;
		}

		return $filters_output;
	}

	public function build_price_range_output( $submit = false ) {
		global $wp_query;

		$filters_output = '';
		$filter_children = [
			0,
			2500,
			5000,
			7500,
			10000,
			12500,
			15000,
			17500,
			20000,
			22500,
			25000,
			27500,
			30000,
			32500,
			35000,
			37500,
			40000,
			45000,
			50000,
			55000,
			60000,
			65000,
			70000,
			75000,
			80000,
			90000,
			100000,
			125000,
			150000,
			200000
		];

		if ( $filter_children ) {
			$filters_output .= '<div class="filter filter-group-prijs">';
			$filters_output .= '<header class="filter-trigger"><h5 class="filter__title">Prijs</h5></header>';
			$filters_output .= '<div class="filter-dropdown"><div class="row">';
			$submit = $submit ? 'onchange="this.form.submit()"' : '';

			//Van
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_price[prijs_van][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Van</option>';

			foreach ( $filter_children as $child ) {
				$checked = '';
				if ( $wp_query->get( 'hexon_price_from' ) !== '' ) {
					$hexon_price_from = (int) $wp_query->get( 'hexon_price_from' );
					if ( $hexon_price_from === $child ) {
						$checked = 'selected="selected"';
					}
				}

				$filters_output .= '<option value="' . $child . '" ' . $checked . '>€ ' . number_format_i18n( $child ) . '</option>';
			}
			$filters_output .= '</select></div></div>';

			//Tot
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_price[prijs_tot][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Tot</option>';

			foreach ( $filter_children as $child ) {

				$checked = '';

				if ( $wp_query->get( 'hexon_price_to' ) !== '' ) {

					$hexon_price_to = (int) $wp_query->get( 'hexon_price_to' );
					if ( $hexon_price_to === $child ) {
						$checked = 'selected="selected"';
					}
				}

				$filters_output .= '<option value="' . $child . '" ' . $checked . '>€ ' . number_format_i18n( $child ) . '</option>';
			}
			$filters_output .= '</select></div></div>';
			$filters_output .= '</div></div>';
			$filters_output .= '</div>';
		}

		return $filters_output;
	}

	public function build_mileage_range_output( $submit = false ) {
		$filters_output = '';
		$filter_children = [0, 200000];

		if ( $filter_children ) {
			$filters_output .= '<div class="filter filter-group-kilometerstand">';
			$filters_output .= '<header class="filter-trigger"><h5 class="filter__title">Kilometerstand</h5></header>';
			$filters_output .= '<div class="filter-dropdown"><div class="row">';
			$submit = $submit ? 'onchange="this.form.submit()"' : '';

			//Van
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_mileage[kilometerstand_van][]]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Van</option>';

			for ($i = $filter_children[0]; $i <= $filter_children[1]; $i = $i + 2500) {
				$filters_output .= '<option value="' . $i . '-km">' . $i . '</option>';
			}

			$filters_output .= '</select></div></div>';

			//Tot
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_mileage[kilometerstand_tot][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Tot</option>';

			for ($i = $filter_children[0]; $i <= $filter_children[1]; $i = $i + 2500) {
				$filters_output .= '<option value="' . $i . '-km">' . $i . '</option>';
			}

			$filters_output .= '</select></div></div>
								</div></div>
								</div>';
		}
		return $filters_output;
	}

	public function build_power_range_output( $submit = false) {
		$filters_output = '';
		$filter_children = [0, 500];
		
		$filter_type = 'vermogen';
		$filter_slug = 'filters_voorraad';

		if ( $filter_children ) {
			$filters_output .= "<div class='filter filter-group-$filter_type'>";
			$filters_output .= "<header class='filter-trigger'><h5 class='filter__title'>" . ucfirst($filter_type) . "</h5></header>";
			$filters_output .= '<div class="filter-dropdown"><div class="row">';
			$submit = $submit ? 'onchange="this.form.submit()"' : '';

			//Van
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_output[vermogen_van][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Van</option>';

			for ($i = $filter_children[0]; $i <= $filter_children[1]; $i = $i + 50) {
				$filters_output .= '<option value="' . $i . '">' . $i . '</option>';
			}

			$filters_output .= '</select></div></div>';

			//Tot
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_output[vermogen_tot][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Tot</option>';

			for ($i = $filter_children[0]; $i <= $filter_children[1]; $i = $i + 50) {
				$filters_output .= '<option value="' . $i . '">' . $i . '</option>';
			}

			$filters_output .= '</select></div></div>
								</div></div>
								</div>';
		}
		return $filters_output;
	}

	public function build_construction_year_range_output( $submit = false) {
		global $wpdb;

        //$results = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key='BouwJaar' ORDER BY meta_value ASC");

        $resultsMax = $wpdb->get_results("SELECT MAX(meta_value) as meta_value FROM wp_postmeta WHERE meta_key='BouwJaar' ORDER BY meta_value ASC");
		$resultsMin = $wpdb->get_results("SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key='BouwJaar' AND meta_value != '' ORDER BY meta_value ASC LIMIT 1");

		$min = $resultsMin[array_keys($resultsMin)[0]];
		$max = $resultsMax[array_keys($resultsMax)[0]];

		$filters_output = '';
		$filter_children = [];




		for($i = intval($min->meta_value); $i <= intval($max->meta_value) + 1; $i += 1){
		    if($i > date('Y')){
                $filter_children[] = date('Y');
                break;
            }
		    else if($i === date('Y')){
                $filter_children[] = $i;
                break;
            }
            $filter_children[] = $i;
        }

//		foreach($results as $result) {
//			$filter_children[] = $result->meta_value;
//		}
		
		$filter_type = 'bouwjaar';
		$filter_slug = 'filters_voorraad';

		if ( $filter_children ) {
			$filters_output .= "<div class='filter filter-group-$filter_type'>";
			$filters_output .= "<header class='filter-trigger'><h5 class='filter__title'>" . ucfirst($filter_type) . "</h5></header>";
			$filters_output .= '<div class="filter-dropdown"><div class="row">';
			$submit = $submit ? 'onchange="this.form.submit()"' : '';

			//Van
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_construction_year[bouwjaar_van][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Van</option>';

			for($i = intval($min->meta_value); $i <= intval($max->meta_value) + 1; $i += 1) {
                if($i > intval(date('Y'))){
                    $i = date('Y');
                    $filters_output .= '<option value="' . $i . '">' . $i . '</option>';
                    break;
                }
                $filters_output .= '<option value="' . $i . '">' . $i . '</option>';
            }

			$filters_output .= '</select></div></div>';

			//Tot
			$filters_output .= '<div class="col-md-6"><div class="select car-overview__select"><select class="select__input" name="hexon_construction_year[bouwjaar_tot][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">Tot</option>';

            for($i = intval($min->meta_value); $i <= intval($max->meta_value) + 1; $i += 1) {
                if($i > intval(date('Y'))){
                    $i = date('Y');
                    $filters_output .= '<option value="' . $i . '">' . $i . '</option>';
                    break;
                }
				$filters_output .= '<option value="' . $i . '">' . $i . '</option>';
			}

			$filters_output .= '</select></div></div>
								</div></div>
								</div>';
		}
		return $filters_output;
	}

	public function build_filter_output( $filter, $term_slug = 'filters', $current_terms = [], $current_meta = [], $submit = false, $prefix = '' ) {
		$filters_output  = '';
		$filter_children = new \WP_Term_Query( [
			'child_of'   => (int) $filter->term_id,
			'taxonomy'   => $term_slug,
			'object_ids' => $this->object_ids,
			'order'      => 'ASC',
			'hide_empty' => true,
		] );

		$filter_children = $this->sort_filters( $filter_children->terms, 'name' );

		if ( $filter_children ) {

			$filters_output .= '<div class="filter filter-group-' . $filter->slug . '">';

			if ( $submit ) {
				$submit = 'onchange="this.form.submit()"';
			} else {
				$submit = '';
			}

			$filters_output .= '<div class="select car-overview__select"><select class="select__input" name="hexon_' . $term_slug . '[' . $filter->slug . '][]" ' . $submit . '>';
			$filters_output .= '<option selected="selected">' . str_replace( '_', ' ', $filter->name ) . '</option>';
			$filters_output .= '<option value="">Alles</option>';

			foreach ( $filter_children as $child ) {

				$term = get_term_by( 'id', (int) $child->term_id, $term_slug );

				$suffix  = '';
				$checked = '';

				if ( $filter->slug == 'prijs' ) {
					$term->name = '€ ' . number_format_i18n( $term->name );
				}

				if ( $filter->slug == 'prijs' || $filter->slug == 'kilometerstand' ) {
					if ( $filter->slug == 'kilometerstand' ) {
						$suffix = '+ km';
					} else {
						$suffix = '+';
					}
				}

				if ( ! empty( $current_terms ) ) {
					if ( in_array( $term->slug, $current_terms, true ) ) {
						$checked = 'selected="selected"';
					}
				}

				if ( ! empty( $current_meta ) ) {
					if ( in_array( $term->slug, $current_meta, true ) ) {
						$checked = 'selected="selected"';
					} else if ( is_numeric( $term->slug ) ) {
						$term->slug = (int) $term->slug;
						if ( in_array( $term->slug, $current_meta, true ) ) {
							$checked = 'selected="selected"';
						}
					}
				}

				$filters_output .= '<option value="' . $term->slug . '" ' . $checked . '>' . $term->name . $suffix . '</option>';
			}
			$filters_output .= '</select></div>';
			$filters_output .= '</div>';
		}

		return $filters_output;
	}

	public function build_type_output( $term_slug = 'type', $current_terms = [], $submit = false, $prefix = '' ) {

		$filters_output  = '';
		$filter_children = new \WP_Term_Query( [
			'parent'     => 0,
			'taxonomy'   => $term_slug,
			'object_ids' => $this->object_ids,
			'order'      => 'ASC',
			'hide_empty' => true,
		] );

		$filter_children = $this->sort_filters( $filter_children->terms, 'name' );

		if ( $filter_children ) {

			$filters_output .= '<div class="filter filter-group-' . $term_slug . '">';

			if ( $submit ) {
				$submit = 'onchange="this.form.submit()"';
			} else {
				$submit = '';
			}

			$filters_output .= '<div class="select car-overview__select"><select class="select__input" name="hexon_' . $term_slug . '" ' . $submit . '>';
			$filters_output .= '<option selected="selected">' . ucfirst( explode( '_', $term_slug )[0] ) . '</option>';
			$filters_output .= '<option value="">Alles</option>';

			foreach ( $filter_children as $child ) {

				$parent_term     = $child;
				$filter_children = new \WP_Term_Query( [
					'child_of'   => (int) $parent_term->term_id,
					'taxonomy'   => $term_slug,
					'object_ids' => $this->object_ids,
					'order'      => 'ASC',
					'hide_empty' => true,
				] );

				$checked = '';

				if ( ! empty( $current_terms ) ) {
					if ( in_array( $parent_term->slug, $current_terms, true ) ) {
						$checked = 'selected="selected"';
					}
				}

				$filter_children = $this->sort_filters( $filter_children->terms, 'name' );
				if ( $filter_children ) {
					$status         = 'disabled="disabled"';
					$filters_output .= '<option ' . $status . ' value="' . $parent_term->slug . '">--- ' . $parent_term->name . ' ---</option>';
				} else {
					$status         = '';
					$filters_output .= '<option ' . $status . ' value="' . $parent_term->slug . '" ' . $checked . '>' . $parent_term->name . '</option>';
				}

				if ( $filter_children ) {
					foreach ( $filter_children as $child ) {
						$term    = get_term_by( 'id', (int) $child->term_id, $term_slug );
						$suffix  = '';
						$checked = '';

						if ( ! empty( $current_terms ) ) {
							if ( in_array( $term->slug, $current_terms, true ) ) {
								$checked = 'selected="selected"';
							}
						}

						$filters_output .= '<option value="' . $term->slug . '" ' . $checked . '>' . $term->name . $suffix . '</option>';
					}
				}
			}
			$filters_output .= '</select></div>';
			$filters_output .= '</div>';
		}

		return $filters_output;
	}

	public function build_link_output( $term_slug = 'type', $current_terms = [], $submit = false, $prefix = '' ) {

		$filters_output  = '';
		$filter_children = new \WP_Term_Query( [
			'parent'     => 0,
			'taxonomy'   => $term_slug,
			'object_ids' => $this->object_ids,
			'order'      => 'ASC',
			'hide_empty' => true,
		] );

		$filter_children = $this->sort_filters( $filter_children->terms, 'name' );

		if ( $filter_children ) {

			$filters_output .= '<div class="filter filter-group-' . $term_slug . '">';
			$filters_output .= '<div class="filter-dropdown">';

			foreach ( $filter_children as $child ) {

				$parent_term = $child;

				$filters_output .= '<div class="sub-filter sub-filter-group-' . $parent_term->slug . '">';
				$filters_output .= '<header>';
				$filters_output .= '<h5 class="filter__title">';

				$checked = '';

				$queried_car = get_queried_object();

				if ( ! empty( $queried_car ) && isset( $queried_car->slug ) ) {
					if ( $parent_term->slug == $queried_car->slug ) {
						$checked = 'active';
					}
				}

				$link = get_term_link( $parent_term->slug, $term_slug );

				$filters_output .= '<span class="checkbox__text"><a href="' . $link . '" class="' . $checked . '">' . $parent_term->name . '</a></span>';
				$filters_output .= '</h5>';
				$filters_output .= '</header>';
				$filters_output .= '</div>';

			}
			$filters_output .= '</div>';
			$filters_output .= '</div>';
		}

		return $filters_output;
	}

	public function aanvraag_form() {

		$output = '';

		$output .= '<form name="car" method="post" action="' . get_the_permalink() . '" enctype="multipart/form-data" class="modal-content">
        <input type="hidden" name="car" value="1" />
        <input type="hidden" name="car_id" value="' . get_the_ID() . '" />
        <div class="modal-body">
            <div class="form-group firstname">
                <input type="text" name="firstname" class="form-control" placeholder="Voornaam" required>
            </div>
            <div class="form-group lastname">
                <input type="text" name="lastname" class="form-control" placeholder="Achternaam" required>
            </div>

            <div class="form-group email">
                <input type="email" name="email" class="form-control" placeholder="Email" required>
            </div>

            <div class="form-group motivation">
                <textarea name="motivation" class="form-control" rows="4" cols="50" placeholder="Opmerkingen"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary info__button">Versturen</button>
        </div>
    </form>';

		return $output;
	}

	public function build_radius_output() {

		global $wp_query;

		$filters_output = '';

		$filter_children = [
			1,
			2,
			5,
			10,
			15,
			25,
			50,
			100,
			150,
			200
		];

		if ( $filter_children ) {

			if ( get_query_var( 'hexon_radius_term' ) ) {
				$hexon_radius = 'value="' . get_query_var( 'hexon_radius_term' ) . '"';
			} else {
				$hexon_radius = '';
			}

			$filters_output .= '<div class="filter filter-group-straal">';
			$filters_output .= '<header class="filter-trigger"><h5 class="filter__title">Afstand</h5></header>';
			$filters_output .= '<input type="text" name="hexon_radius[term]" ' . $hexon_radius . ' placeholder="Adres en/of postcode" class="hero__search-input" />';
			$filters_output .= '<div class="select car-overview__select"><select class="select__input" name="hexon_radius[straal][]">';
			$filters_output .= '<option selected="selected">Afstand</option>';

			foreach ( $filter_children as $child ) {

				$checked = '';
				$suffix  = ' km';

				if ( get_query_var( 'hexon_radius' ) ) {
					$hexon_radius = (int) preg_replace( '/[^\d]+/i', '', get_query_var( 'hexon_radius' ) );
					if ( $hexon_radius === $child ) {
						$checked = 'selected="selected"';
					}
				}

				$filters_output .= '<option value="+' . $child . 'km-afstand" ' . $checked . '>+' . $child . $suffix . '</option>';
			}
			$filters_output .= '</select></div>';
			$filters_output .= '</div>';
		}

		return $filters_output;

	}
}

new HexonShortcodes();
