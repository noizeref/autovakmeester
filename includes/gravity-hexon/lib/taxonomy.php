<?php

namespace Gravity\Hexon\Taxonomy;

use Gravity\Hexon;

class HexonTaxonomy extends Hexon {

	public function __construct() {
		add_action( 'init', [ $this, $this->prefix . '_custom_taxonomy' ] );

	}

	// Register Custom Taxonomies
	public function hexon_custom_taxonomy() {

		if ( get_option( 'hexon_slug' ) != '' ) {
			$slug = get_option( 'hexon_slug' );
		} else {
			$slug = 'voorraad';
		}

		$slug_soort = 'soort';

		// Set Soort
		$labels_soort = [
			'name'          => _x( 'Soorten', 'Taxonomy General Name', 'hexon' ),
			'singular_name' => _x( 'Soort', 'Taxonomy Singular Name', 'hexon' ),
			'menu_name'     => __( 'Soorten', 'hexon' )
		];

		$args_soort = [
			'labels'            => $labels_soort,
			'hierarchical'      => true,
			'rewrite'           => [ 'slug' => $slug . '/' . $slug_soort, 'with_front' => true ],
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true
		];
		register_taxonomy( 'soort_voorraad', [ 'voorraad' ], $args_soort );

		wp_insert_term( 'Nieuw', 'soort_voorraad', [ 'slug' => 'nieuw' ] );
		wp_insert_term( 'Occasion', 'soort_voorraad', [ 'slug' => 'occasion' ] );
		wp_insert_term( 'Bedrijfsauto', 'soort_voorraad', [ 'slug' => 'bedrijfsauto' ] );
		wp_insert_term( 'Demo', 'soort_voorraad', [ 'slug' => 'demo' ] );

		// Financiering type
		$slug_type = 'financiering';

		// Set Type
		$labels_type = [
			'name'          => _x( 'Financieringen', 'Taxonomy General Name', 'hexon' ),
			'singular_name' => _x( 'Financiering', 'Taxonomy Singular Name', 'hexon' ),
			'menu_name'     => __( 'Financieringen', 'hexon' ),
		];

		$args_type = [
			'labels'            => $labels_type,
			'hierarchical'      => true,
			'rewrite'           => [ 'slug' => $slug . '/' . $slug_type, 'with_front' => true ],
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true
		];
		register_taxonomy( 'financiering_voorraad', [ 'voorraad' ], $args_type );

		wp_insert_term( 'Koop', 'financiering_voorraad', [ 'slug' => 'koop' ] );
		wp_insert_term( 'Private lease', 'financiering_voorraad', [ 'slug' => 'private-lease' ] );
		wp_insert_term( 'Short lease', 'financiering_voorraad', [ 'slug' => 'short-lease' ] );

		// Set Filters
		$labels_filters = [
			'name'          => _x( 'Filters', 'Taxonomy General Name', 'hexon' ),
			'singular_name' => _x( 'Filters', 'Taxonomy Singular Name', 'hexon' ),
			'menu_name'     => __( 'Filters', 'hexon' ),
		];

		$args_filters = [
			'labels'            => $labels_filters,
			'hierarchical'      => true,
			'rewrite'           => [ 'slug' => $slug . '/filters', 'with_front' => true ],
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true
		];
		register_taxonomy( 'filters_voorraad', [ 'voorraad' ], $args_filters );

		wp_insert_term( 'Merk en model', 'filters_voorraad', [ 'slug' => 'merk-model' ] );
		wp_insert_term( 'Prijs', 'filters_voorraad', [ 'slug' => 'prijs' ] );
		wp_insert_term( 'Bouwjaar', 'filters_voorraad', [ 'slug' => 'bouwjaar' ] );
		wp_insert_term( 'Kilometerstand', 'filters_voorraad', [ 'slug' => 'kilometerstand' ] );
		wp_insert_term( 'Brandstof', 'filters_voorraad', [ 'slug' => 'brandstof' ] );
		wp_insert_term( 'Transmissie', 'filters_voorraad', [ 'slug' => 'transmissie' ] );
		wp_insert_term( 'Carrosserie', 'filters_voorraad', [ 'slug' => 'carrosserie' ] );
		wp_insert_term( 'Kleur', 'filters_voorraad', [ 'slug' => 'kleur' ] );
		wp_insert_term( 'Vermogen', 'filters_voorraad', [ 'slug' => 'vermogen' ] );
		wp_insert_term( 'Motorinhoud', 'filters_voorraad', [ 'slug' => 'motor-inhoud' ] );
		wp_insert_term( 'Aantal deuren', 'filters_voorraad', [ 'slug' => 'aantal-deuren' ] );
		wp_insert_term( 'NAP', 'filters_voorraad', [ 'slug' => 'nap' ] );
		wp_insert_term( 'BTW verrekenbaar', 'filters_voorraad', [ 'slug' => 'btw-verrekenbaar' ] );

		// Set Opties
		$labels_opties = [
			'name'          => _x( 'Opties', 'Taxonomy General Name', 'hexon' ),
			'singular_name' => _x( 'Optie', 'Taxonomy Singular Name', 'hexon' ),
			'menu_name'     => __( 'Opties', 'hexon' )
		];

		$args_opties = [
			'labels'            => $labels_opties,
			'hierarchical'      => true,
			'rewrite'           => [ 'slug' => $slug . '/' . $slug_soort, 'with_front' => true ],
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true
		];
		register_taxonomy( 'opties_voorraad', [ 'voorraad' ], $args_opties );

	}

}

new HexonTaxonomy();
