<?php

namespace Gravity\Hexon\Templates;

use Gravity\Hexon;

class HexonTemplates extends Hexon {

	public function __construct() {
		add_filter( 'single_template', [ $this, $this->prefix . '_single' ] );
		add_filter( 'archive_template', [ $this, $this->prefix . '_archive_template' ] );
		add_action( 'template_redirect', [ $this, $this->prefix . '_search_template' ] );
	}

	public function hexon_single_template( $template, $init_post_type ) {

		static $using_null = [];

		// Adjust with your custom post types.
		$post_types = [ 'voorraad' ];

		if ( is_single() || is_archive() || $template == 'search' ) {
			$template_basename = basename( $template );

			if ( $template == '' || substr( $template_basename, 0, 4 ) == 'sing' || substr( $template_basename, 0, 4 ) == 'arch' || $template == 'search' ) {
				$post_type = get_post_type();
				$slug      = is_archive() ? 'archive' : 'single';
				if ( in_array( $post_type, $post_types ) ) {
					// Allow user to override.
					if ( $single_template = $this->hexon_get_template( $slug, $post_type ) ) {
						$template = $single_template;
					} else {

						if ( empty( $using_null[ $slug ][ $post_type ] ) ) {

							if ( $template && ( $content_template = $this->hexon_get_template( 'content-' . $slug, $post_type ) ) ) {
								$tpl_str = file_get_contents( $template );

								if ( preg_match( '/get_template_part\s*\(\s*\'content\'\s*,\s*\'' . $slug . '\'\s*\)/', $tpl_str, $matches, PREG_OFFSET_CAPTURE )
								     || preg_match( '/get_template_part\s*\(\s*\'content\'\s*,\s*get_post_format\s*\(\s*\)\s*\)/', $tpl_str, $matches, PREG_OFFSET_CAPTURE )
								     || preg_match( '/get_template_part\s*\(\s*\'content\'\s*\)/', $tpl_str, $matches, PREG_OFFSET_CAPTURE )
								     || preg_match( '/get_template_part\s*\(\s*\'[^\']+\'\s*,\s*\'' . $slug . '\'\s*\)/', $tpl_str, $matches, PREG_OFFSET_CAPTURE )
								) {
									$using_null[ $slug ][ $post_type ] = true;
									$tpl_str                           = substr( $tpl_str, 0, $matches[0][1] ) . 'include \'' . $content_template . '\'' . substr( $tpl_str, $matches[0][1] + strlen( $matches[0][0] ) );

									eval( '?>' . $tpl_str );
								}
							}
						}
						if ( empty( $using_null[ $slug ][ $post_type ] ) ) {
							// Failed to parse - look for fall back template.
							if ( file_exists( HEXON_FOLDER . 'templates/' . $slug . '.php' ) ) {
								$template = HEXON_FOLDER . 'templates/' . $slug . '.php';
							}
						} else {
							// Success! "null.php" is just a blank zero-byte file.
							$template = HEXON_FOLDER . 'templates/null.php';
						}
					}
				} else if ( $hexon_template = $this->hexon_get_template( $template, $init_post_type ) ) {
					$template = $hexon_template;
				}
			}
		}

		return $template;
	}

	public function hexon_search_template() {

		global $wp_query;

		$search    = new Hexon\Search\HexonSearch();
		$car       = $wp_query->get( 'car_slug' );
		$garage    = $wp_query->get( 'garage_slug' );
		$hexon     = filter_var( $wp_query->get( 'hexon_search' ), FILTER_VALIDATE_BOOLEAN );
		$post_type = $wp_query->get( 'car_post_type' );

		if ( $hexon ) {
			$template = 'search';

			add_filter( 'wpseo_title', function () {
				$wpseo_separator = trim( wpseo_replace_vars( '%%sep%%', [] ) );

				return 'Zoeken ' . $wpseo_separator . ' ' . get_bloginfo( 'name' );
			} );

		} else if ( isset( $car ) && ! empty( $car ) ) {
			if ( $car = $search->get_page_by_post_name( $car, OBJECT, 'voorraad' ) ) {
				$post = $search->get_page_by_post_name( $garage, OBJECT, 'garage' );
				if ( get_field( 'has_occasions', $post->ID ) ) {
					add_filter( 'wpseo_title', function () use ( $car, $search ) {
						$wpseo_separator = trim( wpseo_replace_vars( '%%sep%%', [] ) );

						return $car->post_title . ' ' . $wpseo_separator . ' ' . get_bloginfo( 'name' );
					} );

					$template = $this->hexon_get_template( 'single', 'voorraad' );
				} else {
					$search->is_404(); //404 if occasions not enabled
				}
			} else {
				$search->is_404(); //404 if occasions not enabled
			}
		} else if ( isset( $garage ) && ! empty( $garage ) ) {
			$post = $search->get_page_by_post_name( $garage, OBJECT, 'garage' );

			if ( get_field( 'has_occasions', $post->ID ) ) {
				add_filter( 'wpseo_title', function () use ( $garage, $search, $post ) {
					$wpseo_separator = trim( wpseo_replace_vars( '%%sep%%', [] ) );

					return 'Occasions ' . $post->post_title . ' ' . $wpseo_separator . ' ' . get_bloginfo( 'name' );
				} );

				$template = $this->hexon_get_template( 'archive', 'voorraad' );
			} else {
				$search->is_404(); //404 if occasions not enabled
			}
		} else {
			return;
		}

		include $this->hexon_single_template( $template, $post_type );

		exit;
	}

	public function hexon_single( $template ) {
		global $wp_query;

		if ( ! $wp_query->query_vars['post_type'] ) {
			return;
		}

		$post_type = $wp_query->get( 'car_post_type' );

		include $this->hexon_single_template( $template, $post_type );

		exit;
	}

	public function hexon_archive_template( $template ) {
		global $wp_query;

		if ( ! $wp_query->query_vars['post_type'] ) {
			return;
		}

		$post_type = $wp_query->get( 'car_post_type' );

		include $this->hexon_single_template( $template, $post_type );

		exit;
	}

	public function hexon_get_template( $slug, $part = '' ) {
		$template = $slug . ( $part ? '-' . $part : '' ) . '.php';

		$dirs = [];

		if ( is_child_theme() ) {
			$child_dir = get_stylesheet_directory() . '/';
			$dirs[]    = $child_dir . HEXON_BASENAME . '/';
			$dirs[]    = $child_dir;
		}

		$template_dir = get_template_directory() . '/';
		$dirs[]       = $template_dir . HEXON_BASENAME . '/';
		$dirs[]       = $template_dir;
		$dirs[]       = HEXON_FOLDER . 'templates/';

		foreach ( $dirs as $dir ) {
			if ( file_exists( $dir . $template ) ) {
				return $dir . $template;
			}
		}

		return false;
	}
}

new HexonTemplates();
