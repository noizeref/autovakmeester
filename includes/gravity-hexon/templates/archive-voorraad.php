<?php
/**
 * The template for displaying aanbod archive
 */

use Gravity\Hexon\HexonObject;

$object = new HexonObject\HexonObject();

get_header();

if ( have_posts() ) : ?>

    <header class="page-header">
		<?php
		the_archive_title( '<h1 class="page-title">', '</h1>' );
		the_archive_description( '<div class="taxonomy-description">', '</div>' );
		?>
    </header>

    <div class="row">
        <div class="col-sm-3">
			<?php echo do_shortcode( '[hexon_voorraad_filters]' ); ?>
        </div>
        <div class="col-sm-9">
			<?php echo do_shortcode( '[hexon_voorraad_sort]' ); ?>
            <div class="hexon-results">
				<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Get the object fields
					$car = $object->parse_car( get_the_ID() );
					?>
                    <div>
						<?php the_title(); ?>
                    </div>
					<?php
					// End the loop.
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination( array(
					'prev_text'          => __( 'Vorige', 'twentyfifteen' ),
					'next_text'          => __( 'Volgende', 'twentyfifteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
				) );

				?>
            </div>
        </div>
    </div>
	<?php
endif;

get_footer();
