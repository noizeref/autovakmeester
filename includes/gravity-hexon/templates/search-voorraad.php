<?php

use Gravity\Hexon\Search;
use Gravity\Hexon\HexonObject;

/**
 * The template for displaying aanbod zoeken
 */

global $wp_query;

$search       = new Search\HexonSearch();
$args         = $search->get_search_results( $wp_query->get( 'car_post_type' ) );
$object_query = new WP_Query( $args );
$object       = new HexonObject\HexonObject();
get_header();

?>

    <header class="page-header">
        <h1 class="page-title"><?php _e( 'Zoeken' ); ?></h1>
    </header>

    <div class="row">
        <div class="col-sm-3">
			<?php echo do_shortcode( '[hexon_voorraad_filters]' ); ?>
        </div>
        <div class="col-sm-9">
			<?php echo do_shortcode( '[hexon_voorraad_sort]' ); ?>
            <div class="hexon-results">
				<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Get the object fields
					$car = $object->parse_car( get_the_ID() );
					?>
                    <div>
						<?php the_title(); ?>
                    </div>
				<?php
					// End the loop.
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination( [
					'prev_text'          => __( 'Vorige', 'twentyfifteen' ),
					'next_text'          => __( 'Volgende', 'twentyfifteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
				] );

				?>
            </div>
        </div>
    </div>
<?php

get_footer();
