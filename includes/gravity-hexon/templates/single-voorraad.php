<?php
/**
 * The template for displaying the single for aanbod
 */

use Gravity\Hexon\HexonObject;

$object = new HexonObject\HexonObject();

get_header();

// Start the loop.
while ( have_posts() ) : the_post();

	// Get the object fields
	$car = $object->parse_car( get_the_ID() );
	print_r( $car );

	$gallery = $object->parse_gallery( get_the_ID() );
	print_r( $gallery );

	the_title();
	the_content();

	// End the loop.
endwhile;

get_footer();