<?php
$link  = get_field( 'faq_link', 'option' );
$title = get_field( 'faq_title', 'option' );
if ( have_rows( 'faq', 'option' ) ):
	?>
    <section class="faq section">
        <div class="triangle">
            <div class="triangle__element"></div>
            <div class="triangle__element"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5">
                    <h2 class="side-title" data-animate-scroll><?php echo $title; ?></h2>
                </div>
                <div class="col-12 col-md-7">
                    <ul id="faq" class="faq__list">
						<?php $faq_counter = 0;
						while ( have_rows( 'faq', 'option' ) ): the_row();
							$faq_counter ++; ?>
							<?php if ( $faq_counter > 5 && is_front_page() ): ?>
								<?php break; ?>
							<?php endif; ?>

                            <li class="faq__item">
                                <div class="faq__question js-question">
                                    <span><?php the_sub_field( 'question' ); ?></span>
                                    <svg class="faq__icon">
                                        <use xlink:href="#icon-faq"/>
                                    </svg>
                                </div>
                                <div class="faq__answer js-answer">
                                    <div class="wysiwyg">
                                        <p><?php the_sub_field( 'answer' ); ?></p>
                                    </div>
                                </div>
                            </li>

						<?php endwhile; ?>
                    </ul>
					<?php if ( is_front_page() && isset( $link['url'] ) && isset( $link['target'] ) && isset( $link['title'] ) ): ?>
                        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"
                           class="link faq__link"><?php echo $link['title']; ?></a>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
