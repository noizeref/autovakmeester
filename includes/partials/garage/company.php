<section class="company section">
  <div class="triangle">
      <div class="triangle__element"></div>
      <div class="triangle__element"></div>
  </div>
  <div class="container company__container">
      <div class="company__row row">
          <div class="col-12 col-md-5 company__col">
              <h1 class="company__title">Autovakmeester <span><?php the_title(); ?></span></h1>
              <div class="company_description"><?php the_content(); ?></div>
              <address class="company__address">
                  <ul>
                  <?php
                    $garage_location  = get_field('business_location');
                    $location_fields  = explode(',', $garage_location['address']);
                    $garage_mail      = get_field('garage_mail');
                    $garage_phone     = get_field('garage_phone');

/*
                    foreach($location_fields as &$location){
                      echo '<li>'.$location.'</li>';
                    }
*/
                  ?>
                  <li style="margin-top: 20px">Tel: <a href="tel:<?php echo antispambot($garage_phone); ?>"><?php echo antispambot($garage_phone); ?></a></li>
                  <li>E-mail: <a href="mailto:<?php echo antispambot($garage_mail); ?>"><?php echo antispambot($garage_mail); ?></a></li>
                </ul>
              </address>
              <?php if(get_field('garage_facebook') || get_field('garage_twitter') || get_field('garage_youtube') || get_field('garage_linkedin')): ?>
                  <div class="company__share">
                      <div class="share__flex  share__flex--clear">
                          <?php if(get_field('garage_facebook')): ?>
                              <a href="<?php the_field('garage_facebook'); ?>" class="share__link share__link--facebook" target="_blank">
                                  <svg>
                                      <use xlink:href="#icon-facebook"/>
                                  </svg>
                              </a>
                          <?php endif; if(get_field('garage_twitter')): ?>
                              <a href="<?php the_field('garage_twitter'); ?>" class="share__link share__link--twitter" target="_blank">
                                  <svg>
                                      <use xlink:href="#icon-twitter"></use>
                                  </svg>
                              </a>
                          <?php endif; if(get_field('garage_youtube')): ?>
                              <a href="<?php the_field('garage_youtube'); ?>" class="share__link share__link--youtube" target="_blank">
                                  <svg>
                                      <use xlink:href="#icon-youtube"></use>
                                  </svg>
                              </a>
                          <?php endif; if(get_field('garage_linkedin')): ?>
                              <a href="<?php the_field('garage_linkedin'); ?>" class="share__link share__link--linkedin" target="_blank">
                                  <svg>
                                      <use xlink:href="#icon-linkedin"></use>
                                  </svg>
                              </a>
                          <?php endif; ?>
                      </div>
                  </div>
              <?php endif; ?>
              <?php if(get_field('garage_link') || get_field('garage_hours_workshop') || get_field('garage_hours_showroom')): ?>
                  <ul class="company__actions">
                      <?php if( $link = get_field('garage_link')): ?>
                      <li>
                          <a href="<?php echo $link['url']; ?>" class="button" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                      </li>
                      <?php endif; ?>
                      <?php if(get_field('garage_hours_workshop') || get_field('garage_hours_showroom')): ?>
                          <li>
                              <div class="link link--red js-modal-toggle">Openingstijden</div>
                          </li>
                      <?php endif; ?>
                  </ul>
              <?php endif; ?>
          </div>
          <?php if(has_post_thumbnail()): ?>
              <style>
                .company_description{
                  margin-right: 10%;
                }
                address.location {
                  text-align: right;
                  font-size: 0.8em;
                  margin: 10px 60px 10px 200px;
                  position: absolute;
                  width: calc(100% - 32px - 260px );
                  color: #0167B1;
                }
                @media only screen and (max-width: 990px) {
                  address.location {
                    margin-top: -3rem;
                    margin-right: 100px;
                    margin-left: 0;
                    width: calc(100% - 32px - 100px );
                    z-index: 10;
                    background-color: #fff;
                  }
                }
                @media only screen and (max-width: 767px) {
                  address.location {
                    position: relative;
                  }
                  .company_description{
                    margin-right: 0;
                  }

                }
              </style>
              <div class="col-12 col-md-7 company__col">
                  <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'cropped-12-12' ); ?>
                      <div class="company__aspect">
                          <div class="company__image" style="background-image:url(<?php echo $featured_image[0]; ?>)"></div>
                      </div>
                  <div class="company__rating">
                      <div class="company__rating-logo"></div>
                      <div class="company__rating-number">
                        <?php


                            if($clientid = get_field('klantenvertellen_clientid')){
                                the_klantenvertellen_total_score($clientid);
                            }
                        ?>
                      </div>
                      <svg class="company__rating-icon">
                          <use xlink:href="#icon-pointer"></use>
                      </svg>
                  </div>
                  <address class="location">
                    <?php echo $garage_location['address']; ?>
                  </address>
                  <div class="company__circle"></div>
                  <div class="company__triangle"></div>
              </div>
          <?php endif; ?>
      </div>
  </div>
</section>
