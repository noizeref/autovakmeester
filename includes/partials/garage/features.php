<?php if ( have_rows( 'garage_features' ) ): ?>
    <section class="features section">
        <div class="container">
            <div class="row features__row text-center">
                <div class="col-12">
                    <h2 class="features__title">Waarmee kunnen wij je helpen?</h2>
                </div>
            </div>
            <div class="features__flex">
				<?php while ( have_rows( 'garage_features' ) ): the_row(); ?>

                    <div data-animate-scroll class="features__col">
                        <a href="<?php the_sub_field( 'link' ); ?>" class="features__item">
                            <div class="features__circle">
                                <svg class="features__icon">
                                    <use xlink:href="#icon-<?php the_sub_field( 'icon' ); ?>"/>
                                </svg>
                            </div>
                            <h4 class="features__subtitle"><?php the_sub_field( 'title' ); ?></h4>
                            <p class="features__paragraph"><?php the_sub_field( 'description' ); ?></p>
                        </a>
                    </div>

				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
