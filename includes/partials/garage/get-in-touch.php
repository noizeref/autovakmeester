<section class="section">
   <div class="container">
      <div class="row">
         <div class="col-12 col-lg-5">
            <?php $grage_flow_url = add_query_arg( array(
               'location'  => get_garage_home_id(),
               'intention' => 'appointment',
               ), get_flow_urls(0) );
               ?>
            <a href="<?php echo esc_url($grage_flow_url); ?>" class="service__link">
               <div class="service__left">
                  <div class="service__circle service__circle--yellow">
                     <svg class="service__icon">
                        <use xlink:href="#icon-calendar"/>
                     </svg>
                  </div>
               </div>
               <div class="service__right">
                  <div class="service__caption">
                     <h4 class="service__title">Afspraak maken</h4>
                     <div class="service__description">Maak een afspraak bij onze vestiging.</div>
                  </div>
                  <svg class="service__arrow">
                     <use xlink:href="#icon-arrow-right"/>
                  </svg>
               </div>
            </a>
         </div>
         <div class="col-12 col-lg-5 offset-lg-2">
          <?php $grage_flow_url = add_query_arg( array('location'  => get_garage_home_id(), 'intention' => 'estimate',), get_flow_urls(0) );?>
          <a href="<?php echo esc_url($grage_flow_url); ?>" class="service__link">
             <div class="service__left">
                <div class="service__circle service__circle--blue">
                   <svg class="service__icon">
                      <use xlink:href="#icon-calculator"/>
                   </svg>
                </div>
             </div>
             <div class="service__right">
                <div class="service__caption">
                   <h4 class="service__title">Offerte aanvragen</h4>
                   <div class="service__description">Vraag hier een vrijblijvende offerte aan.</div>
                </div>
                <svg class="service__arrow">
                   <use xlink:href="#icon-arrow-right"/>
                </svg>
             </div>
          </a>
         </div>
      </div>
   </div>
</section>