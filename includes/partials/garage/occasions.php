<?php global $post;
if ( get_field( 'has_occasions' ) ) {
	?>
    <section class="occasions">
        <div class="container">
            <div class="row occasions__row">
				<?php if ( $occasions_link = get_field( 'occasions_link' ) ) {
					$search_url = $occasions_link . 'zoeken/';
				} else {
					$occasions_link = get_post_type_archive_link( 'voorraad' );
					$search_url     = get_post_type_archive_link( 'voorraad' ) . 'zoeken/';
				} ?>
                <div class="col-12 col-md-6 occasions__col">
                    <form action="<?php echo $search_url; ?>" class="occasions__form" method="post">
                        <h2 class="occasions__title side-title" data-animate-scroll>Zoek hier de juiste occasion!</h2>
                        <div class="occasions__input-wrapper">
                            <input class="occasions__input" type="search" name="hexon_search[term]"
                                   placeholder="Merk of model" required>
                            <button class="occasions__search-button">
                                <svg class="occasions__search-icon">
                                    <use xlink:href="#icon-search"/>
                                </svg>
                            </button>
                        </div>
                        <input type="hidden" name="garage_slug" value="<?php echo $post->post_name; ?>">
                        <input type="hidden" name="hexon_set_search" value="1">
                    </form>
                    <div>
                        <a href="<?php echo $occasions_link; ?>" class="link occasions__link">Bekijk alle
                            occasions</a>
                    </div>
                </div>
                <div class="col-7 offset-5 offset-md-0 col-md-6 occasions__col" data-animate-scroll>
                    <div class="occasions__cloud">
						<?php
						$customer_id = get_field( 'hexon_klantennummer' );
						$functions   = new \Gravity\Hexon\Functions\Functions();
						$occasions   = $functions->getOccasionsByCustomerId( $customer_id );
						foreach ( $occasions->posts as $occasion ) {
							$gallery_type   = 'local';
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $occasion->ID ), 'responsive-3-12' );

							if ( empty( $featured_image ) ) {
								$gallery = json_decode( get_post_meta( $occasion->ID, 'car_media', true ) );
								if ( ! empty( $gallery ) ) {
									$gallery        = reset( $gallery );
									$featured_image = reset( $gallery );
									$gallery_type   = 'hexon';
								} else {
									$gallery_type = 'empty';
								}
							}

							?>
                            <div class="occasions__cloud-item">
                                <a href="<?php echo get_the_permalink( $occasion->ID ); ?>">
									<?php if ( $gallery_type == 'local' ) { ?>
                                        <div class="occasions__cloud-image"
                                             style="background-image: url(<?php echo $featured_image[0]; ?>); background-size: contain; background-repeat: no-repeat;"></div>
									<?php } else if ( $gallery_type == 'hexon' ) { ?>
                                        <div class="occasions__cloud-image"
                                             style="background-image: url(<?php echo $featured_image->url; ?>); background-size: contain; background-repeat: no-repeat;"></div>
									<?php } ?>
                                </a>
                            </div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
