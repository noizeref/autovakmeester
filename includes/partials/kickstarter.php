<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="kickstarter">
          <div class="kickstarter__image"></div>
          <div class="kickstarter__caption">
            <h2 class="kickstarter__title"><?php the_field( 'kickstarter_title', 'option' ) ?></h2>
            <?php $link = get_field( 'kickstarter_link', 'option' ); if($link){ ?>
              <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="button"><?php echo $link['title']; ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>