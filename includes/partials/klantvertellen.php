<?php
if ( ( is_single() && get_post_type() === 'garage' ) || $wp_query->get( 'garage_slug' ) ) {
	$clientid = get_field( 'klantenvertellen_clientid' ) ?? get_field( 'global_klantenvertellen_clientid', 'option' );
	do_shortcode( '[klantenvertellen clientid="' . $clientid . '"]' );
} else {
	do_shortcode( '[klantenvertellen]' );
}
