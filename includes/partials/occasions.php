<section class="occasions">
    <div class="container">
        <div class="row occasions__row">
            <div class="col-12 col-md-6 occasions__col">
                <form action="<?php echo esc_url( home_url( '/occasions/zoeken/' ) ); ?>" method="post"
                      class="occasions__form">
                    <h2 class="occasions__title side-title"
                        data-animate-scroll><?php the_sub_field( 'occasions_title' ); ?></h2>
                    <div class="occasions__input-wrapper">
                        <input class="occasions__input" type="search" name="hexon_search[term]"
                               placeholder="Merk of model" data-type="voorraad" required>
                        <input type="hidden" name="hexon_set_search" value="1">
                        <button class="occasions__search-button">
                            <svg class="occasions__search-icon">
                                <use xlink:href="#icon-search"/>
                            </svg>
                        </button>
                    </div>
                </form>
				<?php $occasions_link = get_sub_field( 'occasions_link' );
				if ( $occasions_link ): ?>
                    <div>
                        <a href="<?php echo $occasions_link['url']; ?>" class="link occasions__link"
                           target="<?php echo $occasions_link['target']; ?>"><?php echo $occasions_link['title']; ?></a>
                    </div>
				<?php endif; ?>
            </div>
            <div class="col-7 offset-5 offset-md-0 col-md-6 occasions__col" data-animate-scroll>
                <div class="occasions__art-wrapper">
                    <div class="occasions__art occasions__art--1"></div>
                    <div class="occasions__art occasions__art--2"></div>
                    <div class="occasions__art occasions__art--3"></div>
                </div>
				<?php
				$occasions_image = get_sub_field( 'occasions_image' );
				if ( ! empty( $occasions_image ) ):
					$occasions_image_url = $occasions_image['sizes']['large'];
					?>
                    <div class="occasions__image"
                         style="background-image:url(<?php echo $occasions_image_url; ?>)"></div>

				<?php endif; ?>
            </div>
        </div>
    </div>
</section>
