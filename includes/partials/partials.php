<?php
	if( have_rows( 'partials', get_queried_object_id()) ):
		while( have_rows( 'partials', get_queried_object_id()) ): the_row();

			// Services
			if( get_row_layout() == 'services' ):
				include( get_stylesheet_directory() . '/includes/partials/services.php' );

			// Occasions
			elseif( get_row_layout() == 'occasions' ):
				include( get_stylesheet_directory() . '/includes/partials/occasions.php' );

			// FAQ
			elseif( get_row_layout() == 'faq' ):
				include( get_stylesheet_directory() . '/includes/partials/faq.php' );

			// Kickstarter
			elseif( get_row_layout() == 'kickstarter' ):
				include( get_stylesheet_directory() . '/includes/partials/kickstarter.php' );

			// Klantvertellen
			elseif( get_row_layout() == 'klantvertellen' ):
				include( get_stylesheet_directory() . '/includes/partials/klantvertellen.php' );

			endif;

		endwhile;
	endif;
?>