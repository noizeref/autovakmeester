<?php if( have_rows('cta') ): ?>
  <section class="service section">
  <div class="triangle">
    <div class="triangle__element"></div>
    <div class="triangle__element"></div>
  </div>
  <div class="container">
    <div class="row">
 			<div class="col-12 col-md-4">
 				<?php the_content(); ?>
 			</div>
 			<div class="col-12 col-md-7 offset-md-1">
        <div class="service__wrapper">

          <?php while( have_rows('cta') ): the_row(); ?>

            <?php
              if(get_sub_field('link_to_flow')){
                $cta_intention = get_sub_field('flow_intention');
                $cta_url = add_query_arg( array(
                  'intention' => $cta_intention,
                ), get_flow_urls(0) );
              } else{
                $cta_url = get_sub_field('link');
              }
            ?>

            <a href="<?php echo esc_url($cta_url); ?>" class="service__link">
              <div class="service__left">

                <?php
                  if(get_sub_field('icon') == 'calendar'):
                    $circle_class = 'service__circle--yellow';
                  elseif(get_sub_field('icon') == 'calculator'):
                    $circle_class = 'service__circle--blue';
                  else:
                    $circle_class = 'service__circle--green';
                  endif;
                ?>

                <div class="service__circle <?php echo $circle_class; ?>">
                  <svg class="service__icon"><use xlink:href="#icon-<?php the_sub_field('icon'); ?>" /></svg>
                </div>
              </div>
              <div class="service__right">
                <div class="service__caption">
                  <h4 class="service__title"><?php the_sub_field('title'); ?></h4>
                  <div class="service__description"><?php the_sub_field('description'); ?></div>
                </div>
                <svg class="service__arrow"><use xlink:href="#icon-arrow-right" /></svg>
              </div>
            </a>

          <?php endwhile; ?>

        </div>
      </div>
    </div>
  </section>
<?php endif; ?>