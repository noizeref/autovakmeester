<?php
	// Template Name: Login
	get_header();
?>
<section class="login">
	<div class="login__container container">
		<a href="<?php echo get_site_url(); ?>" class="login__back">Ga terug</a>
		<div class="login__center">
			<div class="login__form">
				<div class="login__logo"></div>
				<h1 class="login__title">Inloggen</h1>
        <?php
            global $user_login;
            // In case of a login error.
            if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
    	            <div class="login_error">
    		            <p><?php _e( 'Inloggegevens zijn onjuist.', 'gravity_theme' ); ?></p>
    	            </div>
            <?php
                endif;
            // If user is already logged in.
            if ( is_user_logged_in() ) : ?>

                <div class="gravity_theme_logout">
                    </br>
                    <p>Je bent al ingelogd in het CMS van de website! Ben je misschien op zoek naar de <a href="<?php echo(admin_url('edit.php?post_type=garage')); ?>" class="footer__link">admin pagina?</a></p>
                    <p>
                        of
                        </br>
                        <a id="wp-submit" class="footer__link" href="<?php echo wp_logout_url(); ?>" title="Logout">
                            <?php _e( 'Uitloggen', 'gravity_theme' ); ?>
                        </a>
                    </p>
                </div>

            <?php
                // If user is not logged in.
                else:

                    // Login form arguments.
                    $args = array(
                        'echo'           => false,
                        'redirect'       => admin_url('edit.php?post_type=garage'),
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => false,
                        'value_username' => NULL,
                        'value_remember' => true
                    );

                    // Calling the login form.
                    $login_form = wp_login_form( $args );

                    //add the placeholders
                    $login_form = str_replace('name="log"', 'name="log" placeholder="Gebruikersnaam" required="required"', $login_form);
                    $login_form = str_replace('name="pwd"', 'name="pwd" placeholder="Wachtwoord" required="required"', $login_form);

                    echo $login_form;

                ?>
                    <a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>" class="form__link">Wachtwoord vergeten?</a>
                <?php
                endif;
            ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>