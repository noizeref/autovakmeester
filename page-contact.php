<?php
/**
 * Template Name: Contact
 * Contact template file
 *
 */

get_header(); ?>

  <header class="contact-hero">
    <div class="contact-hero__top">
      <?php include (STYLESHEETPATH. '/parts/shape-animation.php'); ?>
      <div class="container container--hero">
        <div class="row">
          <div class="col-12">
            <h1 class="contact-hero__title">Hier kun je <br> ons bereiken</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="contact-hero__bottom">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-5">
            <?php the_content(); ?>
            <br>
            <ul class="contact__list">
              <li class="contact__item"><?php the_field('global_street', 'option'); ?>,</li>
              <li class="contact__item"><?php the_field('global_zipcode', 'option'); ?></li>
            </ul>
            <ul class="contact__list">
              <li class="contact__item">Tel: <a class="contact__link" href="tel:<?php the_field('global_phone', 'option'); ?>"><?php the_field('global_phone', 'option'); ?></a></li>
              <li class="contact__item">E-mail: <a class="contact__link" href="mailto:<?php echo antispambot(the_field('global_mail', 'option')); ?>"><?php echo antispambot(the_field('global_mail', 'option')); ?></a></li>
            </ul>
            <?php if( have_rows('global_hours', 'option') ): ?>
              <table class="company__table">
                <tbody>
                    <?php while( have_rows('global_hours', 'option') ): the_row(); ?>
                    <tr>
                      <?php
                        $day_field = get_sub_field_object('days', 'option');
                        $day_value = $day_field['value']['value'];
                        $day_label = $day_field['value']['label'];
                      ?>
                      <td><?php echo $day_label; ?></td>
                      <?php if(get_sub_field('closed', 'option')): ?>
                        <td>Gesloten</td>
                      <?php else: ?>
                        <td><?php the_sub_field('start', 'option'); ?> - <?php the_sub_field('end', 'option'); ?></td>
                      <?php endif; ?>
                    </tr>
                    <?php endwhile; ?>
                </tbody>
              </table>
            <?php endif; ?>
            <?php if(get_field('global_facebook', 'option') || get_field('global_twitter', 'option') || get_field('global_youtube', 'option') || get_field('global_linkedin', 'option')): ?>
              <div class="share__flex  share__flex--clear">
                  <?php if(get_field('global_facebook', 'option')): ?>
                      <a href="<?php the_field('global_facebook', 'option'); ?>" class="share__link share__link--facebook" target="_blank">
                          <svg>
                              <use xlink:href="#icon-facebook"/>
                          </svg>
                      </a>
                  <?php endif; if(get_field('global_twitter', 'option')): ?>
                      <a href="<?php the_field('global_twitter', 'option'); ?>" class="share__link share__link--twitter" target="_blank">
                          <svg>
                              <use xlink:href="#icon-twitter"></use>
                          </svg>
                      </a>
                  <?php endif; if(get_field('global_youtube', 'option')): ?>
                      <a href="<?php the_field('global_youtube', 'option'); ?>" class="share__link share__link--youtube" target="_blank">
                          <svg>
                              <use xlink:href="#icon-youtube"></use>
                          </svg>
                      </a>
                  <?php endif; if(get_field('global_linkedin', 'option')): ?>
                      <a href="<?php the_field('global_linkedin', 'option'); ?>" class="share__link share__link--linkedin" target="_blank">
                          <svg>
                              <use xlink:href="#icon-linkedin"></use>
                          </svg>
                      </a>
                  <?php endif; ?>
              </div>
          <?php endif; ?>
          </div>
          <div class="col-12 col-md-7">
            <div class="contact__card">
              <h2 class="contact__title">Heb jij een vraag voor ons?</h2>
              <?php
                $form_id  = '3'; // get_sub_field( 'form' );
              ?>
              <?php gravity_form( $form_id, false, false, false, '', false, 90, true ); ?>

            </div>
          </div>
        </div>
        <hr class="contact__border">
      </div>
    </div>
  </header>

  <?php
    // Partials
    include( 'includes/partials/partials.php');
  ?>

<?php get_footer();