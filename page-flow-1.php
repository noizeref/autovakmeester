<?php
/*
Template Name: flow 1
Template Post Type: page
*/
get_header(); ?>


<div class="flow">
<?php
  $vehicle = get_flow_info('vehicle');
?>
<?php include (STYLESHEETPATH. '/parts/flow-header.php'); ?>
  <section class="flow__section">
    <div class="container">
      <?php include (STYLESHEETPATH. '/parts/flow-breadcrumbs.php'); ?>

      <form action="<?php echo  (isset( get_flow_info( 'location' )['page-id'] ) ? get_flow_urls(2)  : get_flow_urls(1)); ?>" class="license-plate__form">
        <div class="license-plate">
          <div class="license-plate__left">
            <svg class="license-plate__icon"><use xlink:href="#icon-eu" /></svg>
            <div class="license-plate__country">NL</div>
          </div>
          <input autofocus type="text" class="license-plate__input" placeholder="08-XX-TT" value="<?php if(isset($vehicle['licenseplate'])) echo $vehicle['licenseplate'] ?>">
          <div class="license-plate__bottom">
            <a href="<?php echo get_flow_urls(1); ?>" class="link license-plate__skip">Ik weet mijn kenteken niet</a>
            <a href="#" class="link license-plate__reset" style="display: none;">Ander kenteken checken</a>
          </div>
        </div>
        <div class="license-plate__buttons">
          <button class="license-plate__button license-plate__check">Kentekencheck</button>
          <button class="license-plate__button license-plate__submit" type="submit">Ga verder</button>
        </div>
      </form>

      <div class="license-plate__card" style="display: none;">
      </div>

    </div>
  </section>
</div>


<?php
get_footer();
