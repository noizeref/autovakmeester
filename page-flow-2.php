<?php
/*
Template Name: flow 2
Template Post Type: page
*/
get_header(); ?>


<div class="flow">
<?php include (STYLESHEETPATH. '/parts/flow-header.php'); ?>
  <section class="flow__section">
    <div class="container">
      <?php include (STYLESHEETPATH. '/parts/flow-breadcrumbs.php'); ?>

        <div class="row">
          <div class="col-12 col-md-8">
            <?php include (STYLESHEETPATH. '/parts/flow-location-search.php'); ?>
          </div>
          <?php include (STYLESHEETPATH. '/parts/flow-navigator.php'); ?>

        </div>

    </div>
  </section>
</div>


<?php
get_footer();
