<?php
/*
Template Name: flow 3
Template Post Type: page
*/
get_header(); ?>


<div class="flow">
<?php include (STYLESHEETPATH. '/parts/flow-header.php'); ?>
  <section class="flow__section">
    <div class="container">
      <?php include (STYLESHEETPATH. '/parts/flow-breadcrumbs.php'); ?>
      <?php
      while ( have_posts() ) :
        the_post();

          the_content( );

      endwhile; // End of the loop.
      ?>


        <div class="row">
          <div class="col-12 col-md-8">
            <form action="<?php echo get_flow_urls(3); ?>" class="checklist__form">
              <ul class="checklist">
              <?php $tasks = get_flow_info('tasks'); ?>
              <?php if( have_rows('tasks') ): $i = 0;?>
                <?php while( have_rows('tasks') ): the_row(); $i++?>
                <?php $checked = (is_array($tasks) && array_key_exists('checklist-'.$i, $tasks)) ? 'checked' : ''; ?>
                  <li class="checklist__item">
                    <input type="checkbox" <?php echo($checked); ?> id="checklist-<?php echo $i ?>" class="checklist__checkbox" value="<?php the_sub_field('task') ?>">
                    <label for="checklist-<?php echo $i ?>" class="checklist__label" >
                      <span class="checklist__circle">
                        <svg class="checklist__icon"><use xlink:href="#icon-check" /></svg>
                      </span>
                      <div class="checklist__text"><?php the_sub_field('task') ?></div>
                    </label>
                  <?php if( get_sub_field('more_info') ): ?>
                    <div class="checklist__info js-modal-toggle">
                      <svg class="checklist__info-icon"><use xlink:href="#icon-info" /></svg>
                      <div class="modal-content">
                        <h3><?php the_sub_field('task') ?></h3>
                        <?php the_sub_field('more_info') ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  </li>

                <?php endwhile; ?>
              <?php endif; ?>
                <?php $checked = (is_array($tasks) && array_key_exists('checklist-text', $tasks)) ? 'checked' : ''; ?>
                <li class="checklist__item">
                  <input type="checkbox" id="checklist-text" <?php echo($checked); ?> class="checklist__checkbox js-textarea-checkbox">
                  <label for="checklist-text" class="checklist__label js-textarea-label">
                    <span class="checklist__circle">
                      <svg class="checklist__icon"><use xlink:href="#icon-check" /></svg>
                    </span>
                    <div class="checklist__text">Anders, </div>
                    <textarea class="checklist__textarea js-textarea" rows="1" placeholder="namelijk.."><?php if(isset($tasks['checklist-text'])) echo esc_textarea( $tasks['checklist-text'] ); ?></textarea>
                  </label>
                </li>

              </ul>
              <div class="text-right">
                <button type="submit" class="button">Ga verder</button>
              </div>
            </form>
          </div>
          <?php include (STYLESHEETPATH. '/parts/flow-navigator.php'); ?>
        </div>
    </div>
  </section>

  <div class="modal js-modal">
  <div class="modal__backdrop js-modal-toggle"></div>
  <div class="modal__content">
    <div class="modal__close js-modal-toggle">
      <svg class="modal__close-icon"><use xlink:href="#icon-close" /></svg>
    </div>
    <div class="modal__scroll">
      <div class="wysiwyg">
        <h3>Modal title</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt beatae molestias porro pariatur dolore delectus quae, ipsam itaque tenetur accusamus reiciendis neque a nam doloribus, nostrum hic totam! Sapiente, aspernatur.</p>
      </div>
    </div>
  </div>
</div>


</div>


<?php
get_footer();
