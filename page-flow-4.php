<?php
/*
Template Name: flow 4
Template Post Type: page
*/
get_header(); ?>


    <div class="flow">
		<?php include( STYLESHEETPATH . '/parts/flow-header.php' ); ?>
        <section class="flow__section">
            <div class="container">
				<?php include( STYLESHEETPATH . '/parts/flow-breadcrumbs.php' ); ?>

                <div class="row">
                    <div class="col-12 col-md-8 flex-column">

						<?php
						$flow_progress = get_flow_info( 'progress' );
						if ( isset( $flow_progress['intention'] ) && $flow_progress['intention'] == 'appointment' ) {
							$flow__form_class = 'order-2';
						} else {
							$flow__form_class = '';
						}
						?>

                        <div class="flow__form <?php echo( $flow__form_class ); ?>">
                            <div class="text-center">
                                <h4>Naar wie mogen wij ons voorstel toesturen?</h4>
                            </div>
							<?php
							$form_id = '1'; // get_sub_field( 'form' );
							?>
							<?php gravity_form( $form_id, false, false, false, '', true, 1000, true ); ?>
                        </div>

                        <div class="flow__schedule">
                            <div class="text-center">
                                <h4> Plan hieronder uw afspraak</h4>
                                <p>Selecteer hier uw voorkeursmomenten (maximaal 4). <br/><div style="color:#e6412c;">Staat uw voorkeursdatum er niet tussen of heeft u een vraag? Neem dan contact met ons op.</div></p>
                                <div class="schedule-notice" style="display: none;">Je mag maximaal 4 voorkeursmomenten
                                    opgeven
                                </div>
                            </div>
							<?php include( STYLESHEETPATH . '/parts/flow-date.php' ); ?>
                        </div>

                        <div class="flow__end">
                            <div class="checkbox">
                                <input type="checkbox" id="privacyCheckbox" class="checkbox__checkbox">
                                <label for="privacyCheckbox" class="checkbox__label">
                                    <span class="checkbox__circle"><svg class="checkbox__icon"><use xlink:href="#icon-check"/></svg></span>
                                    <span class="checkbox__text">
                                        Ik ga akkoord met de&nbsp;<a href="<?php echo get_permalink( 204 ); ?>" target="_blank">privacy voorwaarden</a>
                                      </span>
                                </label>
                            </div>
                            <button type="submit" id="flow-submit" data-formid="<?php echo $form_id; ?>" class="button" disabled="disabled">Versturen</button>
                        </div>
                    </div>

					<?php include( STYLESHEETPATH . '/parts/flow-navigator.php' ); ?>

                </div>

            </div>
        </section>
    </div>

<?php
get_footer();
