<?php
/*
Template Name: flow success
Template Post Type: page
*/
get_header(); ?>
<section class="landing">
	<div class="container landing__container">
		<div class="row">
			<div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
				<div class="send-check">
					<div class="send-check__left"></div>
					<div class="send-check__right"></div>
				</div>
				<h3>Succesvol verstuurd</h3>
				<p>De garage neemt verder contact met u op om de afspraak te bevestigen.</p>
				<a href="<?php echo get_site_url(); ?>" class="button">Naar home</a>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
