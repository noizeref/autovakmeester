<?php
/**
 * Default content page template file
 *
 */

get_header(); ?>
<?php if ( have_posts() ) :

  // Start the loop.
  while ( have_posts() ) : the_post();
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('content'); ?>>
      <header class="content-hero">
        <?php if(has_post_thumbnail()): ?>
          <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' ); ?>
          <figure class="post-image featured-image background-image news-hero__image" style="background-image: url(<?php echo $featured_image[0]; ?>);"></figure>
          <div class="news-hero__gradient"></div>
        <?php endif; ?>

        <?php include (STYLESHEETPATH. '/parts/shape-animation.php'); ?>

        <div class="container content-hero__container">
          <div class="row content-hero__row ">
          <?php $extra_offset = ( get_post_type() === 'garage') ? 1 : 0; ?>
            <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-<?php echo(3 + $extra_offset); ?>">
              <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-12 col-md-2 col-xl-<?php echo(3 - $extra_offset); ?> text-right">
              <h6>Pagina delen</h6>
              <div class="share__flex">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="share__link share__link--facebook" target="_blank"><svg><use xlink:href="#icon-facebook" /></svg></a>
                <a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" class="share__link share__link--twitter" target="_blank"><svg><use xlink:href="#icon-twitter"></use></svg></a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=" class="share__link share__link--linkedin" target="_blank"><svg><use xlink:href="#icon-linkedin"></use></svg></a>
              </div>
            </div>
          </div>
        </div>
      </header>

      <section class="content__section">
        <div class="container">
          <div class="row">

          <?php if( get_post_type() === 'garage'): ?>
            <div class="col-12 col-md-4 col-xl-3">
              <?php include (STYLESHEETPATH. '/parts/garage-sidenav.php'); ?>
            </div>
            <div class="col-12 col-md-8 col-xl-6 offset-xl-1">
          <?php else: ?>
            <div class="col-12 col-md-8 col-xl-6 offset-xl-3 offset-md-2">
          <?php endif; ?>
              <?php include (STYLESHEETPATH. '/parts/breadcrumbs.php'); ?>
              <div class="wysiwyg">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
        </div>
      </section>
    </article>

<?php
  endwhile;

// If no content, include the "No posts found" template.
else :
  get_template_part( 'template-parts/content', 'none' );

endif;
?>

<?php get_footer();
