<?php
global $wp_query;
$is_garage = false;
if ( ( is_single() && get_post_type() === 'garage' ) || $wp_query->get( 'garage_slug' ) ) {
	$is_garage = true;
}
?>
<navigation class="breadcrumbs">
    <ul class="breadcrumbs__list <?php echo $is_garage ? 'is-garage' : ''; ?>">
        <li class="breadcrumbs__item breadcrumbs__item--home">
            <a href="<?php echo get_site_url(); ?>" class="breadcrumbs__link">
                <svg class="breadcrumbs__icon">
                    <use xlink:href="#icon-home"/>
                </svg>
            </a>
        </li>
		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb();
		}
		?>
    </ul>
</navigation>
