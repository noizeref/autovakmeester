<?php
  $flow_progress = get_flow_info('progress');
  if( isset($flow_progress['intention']) ){
    if($flow_progress['intention'] == 'appointment'){
      $flow_intention_breadcrumb_text = 'Afspraak maken';
    } elseif($flow_progress['intention'] == 'estimate'){
      $flow_intention_breadcrumb_text = 'Offerte opvragen';
    } elseif($flow_progress['intention'] == 'apkcheck') {
      $flow_intention_breadcrumb_text = 'APK vervaldatum check';
    }
  } else{
      $flow_intention_breadcrumb_text = 'Offerte aanvragen / Afspraak maken';
  }
?>


<navigation class="breadcrumbs flow-breadcrumbs">
  <ul class="breadcrumbs__list">
    <li class="breadcrumbs__item">
      <a href="<?php echo get_site_url(); ?>" class="breadcrumbs__link">
        <svg class="breadcrumbs__icon"><use xlink:href="#icon-home" /></svg>
      </a>
    </li>
    <li class="breadcrumbs__item">
      <a href="#" class="breadcrumbs__link"><?php echo($flow_intention_breadcrumb_text); ?></a>
    </li>
  </ul>
</navigation>