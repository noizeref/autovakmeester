<?php
/*
$startdate=strtotime("+1 week");
$enddate=strtotime("+2 weeks", $startdate);

while ($startdate < $enddate) {
?>
  echo date("M d", $startdate) . "<br>";
<?php
  $startdate = strtotime("+1 day", $startdate);
  }
*/
?>

<div class="schedule">
    <div class="schedule__col">
        <div class="schedule__legend">Middag</div>
        <div class="schedule__legend">Ochtend</div>
        <div class="schedule__legend">Dag</div>
    </div>

	<?php
	$today     = strtotime( "+1 week" );
	$dates     = array();
	$timeslots = get_flow_info( 'timeslots' );
	for ( $i = 0; $i <= 14; $i ++ ) {
		$option_date = strtotime( "+$i day", $today );
		if ( date( 'N', $option_date ) != 7 ) {
			?>
            <div class="schedule__col">

                <!-- middag row -->
				<?php $checked = ( is_array( $timeslots ) && array_key_exists( 'checkbox-2-' . $i, $timeslots ) ) ? 'checked' : ''; ?>
                <input class="schedule__checkbox" <?php echo( $checked ); ?> type="checkbox"
                       id="checkbox-2-<?php echo $i ?>"
                       value="<?php echo date_i18n( 'D j M', $option_date ); ?> - Middag">
                <label class="schedule__label" for="checkbox-2-<?php echo $i ?>">
                  <span class="schedule__circle">
                    <svg class="schedule__icon"><use xlink:href="#icon-check"/></svg>
                  </span>
                </label>

                <!-- ochtend row -->
				<?php $checked = ( is_array( $timeslots ) && array_key_exists( 'checkbox-1-' . $i, $timeslots ) ) ? 'checked' : ''; ?>
                <input class="schedule__checkbox" <?php echo( $checked ); ?> type="checkbox"
                       id="checkbox-1-<?php echo $i ?>"
                       value="<?php echo date_i18n( 'D j M', $option_date ); ?> - Ochtend">
                <label class="schedule__label" for="checkbox-1-<?php echo $i ?>">
                  <span class="schedule__circle">
                    <svg class="schedule__icon"><use xlink:href="#icon-check"/></svg>
                  </span>
                </label>
                <div class="schedule__date">
                    <div><?php echo date_i18n( 'D j', $option_date ); ?></div>
                    <div><?php echo date_i18n( 'M', $option_date ); ?></div>
                </div>
            </div>

			<?php
		}
	} ?>

</div>
