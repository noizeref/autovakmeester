<?php
  $flow_array = $_SESSION['flow'];
  //pre_print_r($flow_array);
?>


<header class="flow__hero">
  <?php include( STYLESHEETPATH . '/parts/shape-animation.php' ); ?>
    <div class="container flow__container">
        <div class="flow__top">
  				<div class="col-md-6 md-offset-3 text-center">
            <h1 class="flow__title"><?php the_title(); ?></h1>
  					<?php the_content(); ?>
  				</div>

        </div>
    <?php wp_nav_menu( array(
      'theme_location' => 'flow-menu',
      'container'      => '',
      'menu_class'     => 'flow__tabs'
    ) ); ?>
    </div>
</header>
