<?php

$current_location = get_flow_info( 'current-location' ); // lat lng
if ( isset( $current_location['lat'] ) && isset( $current_location['lng'] ) ) {
	$current_latlng = $current_location['lat'] . ', ' . $current_location['lng'];
}

?>
<div class="location-search">
    <form class="location-search__form" action="">
        <div class="location-search__input-wrapper">
            <input id="location-input" class="location-search__input" type="search"
                   data-latlng="<?php echo $current_latlng ?? ''; ?>" placeholder="" autofocus>
            <a href="#" class="gps get_gps_location">
                <svg class="gps-icon">
                    <use xlink:href="#icon-gps"/>
                </svg>
            </a>
        </div>
        <button type="submit" class="location-search__button load-locations-with-ajax">
            <svg class="location-search__icon">
                <use xlink:href="#icon-search"/>
            </svg>
        </button>
    </form>

	<?php if ( isset( get_flow_info( 'location' )['page-id'] ) ) { ?><a href="<?php echo get_flow_urls( 2 ); ?>"
                                                                        class="button button--flow-next">Je hebt al een
            vestiging gekozen, klik hier om door te gaan.</a><?php } ?>

    <ul class="location-search__results ajax-posts-results">

		<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		if ( isset( $current_location['lat'] ) && $current_location['lng'] ):
			$location_query_args = array(
				'posts_per_page' => 5,
				'post_type'      => 'garage',
				'post_parent'    => 0,
				'geo_query'      => array(
					'lat_field' => '_latitude',
					// this is the name of the meta field storing latitude
					'lng_field' => '_longitude',
					// this is the name of the meta field storing longitude
					'latitude'  => $current_location['lat'],
					// this is the latitude of the point we are getting distance from
					'longitude' => $current_location['lng'],
					// this is the longitude of the point we are getting distance from
					'distance'  => 300,
					// this is the maximum distance to search
					'units'     => 'km'
					// this supports options: miles, mi, kilometers, km
				),
				'orderby'        => 'distance', // this tells WP Query to sort by distance
				'order'          => 'ASC',
				'paged' => $paged
			);
			$ajax_wp_query_args = $location_query_args;
			?>


			<?php
			// the query
			$the_query = new WP_Query( $location_query_args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

            <!-- pagination here -->

            <!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<?php get_template_part( 'template-parts/loop/content', 'garage' ); ?>

			<?php endwhile; ?>

            <!-- end of the loop -->

            <!-- pagination here -->
			<?php
				$total_pages = $the_query->max_num_pages;

				if ($total_pages > 1){
					echo paginate_links(array(
						'base' => get_flow_urls(1) . '%_%',
						'format' => '/page/%#%',
						'current' => $paged,
						'total' => $total_pages,
						'prev_text'    => __('«'),
						'next_text'    => __('»'),
					));
				}
			?>

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
		<?php endif; ?>

    </ul>
</div>
