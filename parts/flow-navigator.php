<div class="col-12 col-md-4">
  <?php if ( get_flow_info( 'vehicle' ) || get_flow_info( 'location' ) || get_flow_info( 'tasks' ) ): ?>
        <div class="flow__card">

      <?php if ( $vehicle = get_flow_info( 'vehicle' ) ): ?>
                <div class="flow__item">
                    <div class="flow__circle flow__circle--green">
                        <svg class="flow__circle-icon">
                            <use xlink:href="#icon-car"/>
                        </svg>
                    </div>
                    <div class="flow__caption">
                        <h4 class="flow__caption-title"><span>Mijn auto</span> <a
                                    href="<?php echo get_flow_urls( 0 ); ?>" class="link">Wijzig</a>
                        </h4>
                        <div class="flow__caption-subtitle"><?php echo $vehicle['brand'] ?><?php echo $vehicle['type'] ?></div>
                        <div class="flow__caption-meta"><?php echo date_i18n( 'j F Y', strtotime( $vehicle['apkdate'] ) ); ?></div>
                        <div class="license-plate">
                            <div class="license-plate__left">
                                <svg class="license-plate__icon">
                                    <use xlink:href="#icon-eu"/>
                                </svg>
                                <div class="license-plate__country">NL</div>
                            </div>
                            <div class="license-plate__input"><?php echo $vehicle['licenseplate'] ?></div>
                        </div>
                    </div>
                </div>
      <?php endif; ?>

      <?php if ( $flow_location = get_flow_info( 'location' ) ): ?>
                <div class="flow__item">
                    <div class="flow__circle flow__circle--blue">
                        <svg class="flow__circle-icon">
                            <use xlink:href="#icon-pointer-small"/>
                        </svg>
                    </div>
                    <div class="flow__caption">
                        <h4 class="flow__caption-title"><span>Jouw vestiging</span> <a
                                    href="<?php echo get_flow_urls( 1 ); ?>" class="link">Wijzig</a>
                        </h4>
                        <?php
                        $clientid           = get_field( 'klantenvertellen_clientid', $flow_location['page-id'] );
                        ?>

                        <div class="flow__rating"><?php the_klantenvertellen_total_score( $clientid ); ?></div>
                        <div class="flow__caption-subtitle"><?php echo  get_the_title($flow_location['page-id']); ?></div>
                        <ul class="location-search__list">
                        <?php
                        $location_fields = explode( ',', $flow_location['address'] );
                        foreach ( $location_fields as &$location ) {
                          echo '<li>' . $location . '</li>';
                        }
                        ?>
                            <li><a href="tel:<?php echo antispambot( $flow_location['phone'] ); ?>"
                                   class="link"><?php echo antispambot( $flow_location['phone'] ); ?></a></li>
                            <li><a href="mailto:<?php echo antispambot( $flow_location['email'] ); ?>"
                                   class="link"><?php echo antispambot( $flow_location['email'] ); ?></a></li>
                            <li><a href="<?php the_permalink( $flow_location['page-id'] ); ?>" class="link">Ga naar
                                    bedrijfspagina</a></li>
                        </ul>
                    </div>
                </div>
      <?php endif; ?>

      <?php if ( $tasks = get_flow_info( 'tasks' ) ): ?>
                <div class="flow__item">
                    <div class="flow__circle flow__circle--yellow">
                        <svg class="flow__circle-icon">
                            <use xlink:href="#icon-tool"/>
                        </svg>
                    </div>
                    <div class="flow__caption">
                        <h4 class="flow__caption-title"><span>Werkzaamheden</span> <a
                                    href="<?php echo get_flow_urls( 2 ); ?>" class="link">Wijzig</a>
                        </h4>
                        <div class="flow__activities">

              <?php foreach ( $tasks as $task ) { ?>
                                <div class="flow__activity">
                                    <svg class="flow__activity-icon">
                                        <use xlink:href="#icon-check"/>
                                    </svg>
                                    <span><?php echo sanitize_text_field( $task ); ?></span>
                                </div>
              <?php } ?>

                        </div>
                    </div>
                </div>
      <?php endif; ?>

        </div>
  <?php endif; ?>
</div>
