<?php
  $garage_home_id = get_garage_home_id();
?>

<div class="modal js-modal">
  <div class="modal__backdrop js-modal-toggle"></div>
  <div class="modal__content">
    <div class="modal__close js-modal-toggle">
      <svg class="modal__close-icon"><use xlink:href="#icon-close" /></svg>
    </div>
    <div class="modal__scroll">
      <div class="wysiwyg">
        <h5>Openingstijden werkplaats</h5>
          <?php if( have_rows('garage_hours_workshop', $garage_home_id) ): ?>
            <table class="company__table">
              <tbody>
                  <?php while( have_rows('garage_hours_workshop', $garage_home_id) ): the_row(); ?>
                  <tr>
                    <?php
                      $day_field = get_sub_field_object('days', $garage_home_id);
                      $day_value = $day_field['value']['value'];
                      $day_label = $day_field['value']['label'];
                    ?>
                    <td><?php echo $day_label; ?></td>
                    <?php if(get_sub_field('closed', $garage_home_id)): ?>
                      <td>Gesloten</td>
                    <?php else: ?>
                      <td><?php the_sub_field('start', $garage_home_id); ?> - <?php the_sub_field('end', $garage_home_id); ?></td>
                    <?php endif; ?>
                  </tr>
                  <?php endwhile; ?>
              </tbody>
            </table>
          <?php endif; ?>
          <h5>Openingstijden showroom</h5>
          <?php if( have_rows('garage_hours_showroom', $garage_home_id) ): ?>
            <table class="company__table">
              <tbody>
                  <?php while( have_rows('garage_hours_showroom', $garage_home_id) ): the_row(); ?>
                  <tr>
                    <?php
                      $day_field = get_sub_field_object('days', $garage_home_id);
                      $day_value = $day_field['value']['value'];
                      $day_label = $day_field['value']['label'];
                    ?>
                    <td><?php echo $day_label; ?></td>
                    <?php if(get_sub_field('closed', $garage_home_id)): ?>
                      <td>Gesloten</td>
                    <?php else: ?>
                      <td><?php the_sub_field('start', $garage_home_id); ?> - <?php the_sub_field('end', $garage_home_id); ?></td>
                    <?php endif; ?>
                  </tr>
                  <?php endwhile; ?>
              </tbody>
            </table>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>
