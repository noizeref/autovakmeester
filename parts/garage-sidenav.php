<nav class="side-nav">
	<ul class="side-nav__list">
      <?php
        global $post;
        $ancestors = $post->ancestors ?? '';
        if(isset($ancestors)):
          $ancestors = array_reverse($ancestors);
          if(isset($ancestors[1])):
            $current_toplevel_menu_id = $ancestors[1];
          else:
            $current_toplevel_menu_id = $post->ID;
          endif;
          $args = array(
          	'bem'             => 'side-nav',
          	'child_of'        => $current_toplevel_menu_id,
          	'post_type'       => $post->post_type,
          	'sort_column'     => 'menu_order',
          	'title_li'        => '',
          	'link_before'     => '<span>',
          	'link_after'      => '</span><span class="side-nav__expand"><svg class="side-nav__icon"><use xlink:href="#icon-faq" /></svg></span>',
          );
          wp_list_pages($args);
        endif;
      ?>

	</ul>
</nav>
