<div style="display: none; visibility: hidden;">
	<?php include_once( STYLESHEETPATH . '/assets/sprite/sprite.svg' ); ?>
</div>

<?php
global $wp_query;
if ( ( is_single() && get_post_type() === 'garage' ) || $wp_query->get( 'garage_slug' ) ): ?>
    <nav class="navbar">
        <div class="container navbar__container">
			<?php
			$garage_home_id = get_garage_home_id();
			?>
            <a href="<?php the_permalink( $garage_home_id ); ?>" class="navbar__logo"
               style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-garage.png)">
                <span class="navbar__logo-name"><?php echo get_the_title( $garage_home_id ); ?></span>
            </a>

            <ul class="navbar__list">
				<?php
				wp_list_pages( 'bem=navbar&sort_column=menu_order&title_li=&depth=1&post_type=garage&child_of=' . $garage_home_id );
				?>
            </ul>

            <div class="navbar__controls">
                <div class="topbar__toggle js-search">
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-search"/>
                    </svg>
                    <span class="topbar__text">Zoeken</span>
                </div>
                <div class="topbar__toggle js-menu">
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-hamburger"/>
                    </svg>
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-close"/>
                    </svg>
                </div>
            </div>
        </div>
    </nav>

<?php else: ?>

    <nav class="nav">
        <div class="container nav__container">
            <div class="nav__close js-menu"></div>
            <div class="nav__top">
                <div class="topbar__toggle js-menu">
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-close"/>
                    </svg>
                    <span class="topbar__text">Sluiten</span>
                </div>
            </div>

			<?php bem_menu( 'main-menu', 'nav__list', 'nav' ); ?>

            <div class="nav__bottom">
                <a href="<?php echo site_url(); ?>" class="nav__logo"></a>
                <ul>
                    <li>
                        <a href="mailto:<?php echo antispambot( the_field( 'global_mail', 'option' ) ); ?>"><?php echo antispambot( the_field( 'global_mail', 'option' ) ); ?></a>
                    </li>
                    <li>
                        <a href="tel:<?php the_field( 'global_phone', 'option' ); ?>"><?php the_field( 'global_phone', 'option' ); ?></a>
                    </li>
                </ul>
				<?php if ( get_field( 'global_facebook', 'option' ) || get_field( 'global_twitter', 'option' ) || get_field( 'global_youtube', 'option' ) || get_field( 'global_linkedin', 'option' ) ): ?>
                    <div class="share__flex  share__flex--clear">
						<?php if ( get_field( 'global_facebook', 'option' ) ): ?>
                            <a href="<?php the_field( 'global_facebook', 'option' ); ?>"
                               class="share__link share__link--facebook" target="_blank">
                                <svg>
                                    <use xlink:href="#icon-facebook"/>
                                </svg>
                            </a>
						<?php endif;
						if ( get_field( 'global_twitter', 'option' ) ): ?>
                            <a href="<?php the_field( 'global_twitter', 'option' ); ?>"
                               class="share__link share__link--twitter" target="_blank">
                                <svg>
                                    <use xlink:href="#icon-twitter"></use>
                                </svg>
                            </a>
						<?php endif;
						if ( get_field( 'global_youtube', 'option' ) ): ?>
                            <a href="<?php the_field( 'global_youtube', 'option' ); ?>"
                               class="share__link share__link--youtube" target="_blank">
                                <svg>
                                    <use xlink:href="#icon-youtube"></use>
                                </svg>
                            </a>
						<?php endif;
						if ( get_field( 'global_linkedin', 'option' ) ): ?>
                            <a href="<?php the_field( 'global_linkedin', 'option' ); ?>"
                               class="share__link share__link--linkedin" target="_blank">
                                <svg>
                                    <use xlink:href="#icon-linkedin"></use>
                                </svg>
                            </a>
						<?php endif; ?>
                    </div>
				<?php endif; ?>
            </div>
        </div>
        <div class="nav__backdrop js-menu"></div>
    </nav>

    <section class="topbar">
        <div class="topbar__container container">
            <div class="topbar__side">
                <div class="topbar__toggle js-menu">
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-hamburger"/>
                    </svg>
                    <span class="topbar__text">Menu</span>
                </div>
                <div class="topbar__toggle js-search">
                    <svg class="topbar__icon">
                        <use xlink:href="#icon-search"/>
                    </svg>
                    <span class="topbar__text">Zoeken</span>
                </div>
            </div>
            <a href="<?php echo site_url(); ?>" class="topbar__logo"></a>
            <div class="topbar__side">
                    <span class="topbar__rating">
                    <div class="topbar__rating-number"><?php the_klantenvertellen_total_score(); ?></div>
                </span>
            </div>
        </div>
    </section>
<?php endif; ?>

<div class="search">
    <div class="search__overlay">
        <div class="container  search__container">
            <div class="row  search__row">
                <div class="col-12  search__col">
                    <div class="search__top">
                        <div class="topbar__toggle js-search">
                            <svg class="topbar__icon">
                                <use xlink:href="#icon-close"/>
                            </svg>
                            <span class="topbar__text">Sluiten</span>
                        </div>
                    </div>
                    <div class="search__body">
                        <div class="search__input-wrapper">
                            <form class="search__form" role="form-search" method="get" id="header-search"
                                  action="<?php echo site_url(); ?>">
                                <input type="search" class="search__input" placeholder="Typ hier uw zoekwoord" value=""
                                       name="s" title="Seach for:">
                                <button type="submit" class="search__button">
                                    <svg class="hero__search-icon">
                                        <use xlink:href="#icon-search"/>
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search__backdrop"></div>
</div>

