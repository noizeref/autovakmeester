<?php

if ( get_field( 'popup_active', 'options' ) ) {
	if ( $date = get_field( 'popup_visible_from', 'options' ) ) {
		if ( strtotime( $date ) < time() ) {
			$title       = get_field( 'popup_title', 'options' );
			$content     = get_field( 'popup_content', 'options' );
			$image       = get_field( 'popup_image', 'options' );
			$button_text = get_field( 'popup_button_text', 'options' );
			$button_link = get_field( 'popup_button_link', 'options' );
			?>
			<div class="modal" data-modal="popup">
				<div class="modal__backdrop js-modal-toggle"></div>
				<div class="modal__content">
					<div class="modal__close modal-close js-modal-toggle">
						<svg class="modal__close-icon"><use xlink:href="#icon-close" /></svg>
					</div>
					<div class="modal__scroll">
						<div class="wysiwyg">
							<?php if ( $title ) { ?>
								<header class="modal-card__title">
									<h2><?php echo $title; ?></h2>
								</header>
							<?php } ?>
							<?php if ( $image ) { ?>
								<div class="modal-card__image">
									<figure>
										<img src="<?php echo $image[ 'url' ]; ?>"/>
									</figure>
								</div>
							<?php } ?>
							<?php if ( $content ) { ?>
								<div class="modal-card__content">
									<?php echo $content; ?>
								</div>
							<?php } ?>
							<?php if ( $button_text && $button_link ) { ?>
								<footer>
									<a href="<?php echo $button_link; ?>" class="button">
										<?php echo $button_text; ?>
									</a>
								</footer>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
}
