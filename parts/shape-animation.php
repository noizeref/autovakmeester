<div class="shape-animation">
	<div class="shape-animation__item shape-animation__cilinder"></div>
	<div class="shape-animation__item shape-animation__circle-1"></div>
	<div class="shape-animation__item shape-animation__triangle-1"></div>
	<div class="shape-animation__item shape-animation__triangle-2"></div>
	<div class="shape-animation__item shape-animation__circle-2"></div>
	<div class="shape-animation__item shape-animation__circle-3"></div>
</div>