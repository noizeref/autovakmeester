<?php
global $wp;
$url = home_url( $wp->request );
?>
<h6>Pagina delen</h6>
<div class="share__flex">
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>"
       class="share__link share__link--facebook" target="_blank">
        <svg>
            <use xlink:href="#icon-facebook"/>
        </svg>
    </a>
    <a href="https://twitter.com/home?status=<?php echo $url; ?>" class="share__link share__link--twitter"
       target="_blank">
        <svg>
            <use xlink:href="#icon-twitter"></use>
        </svg>
    </a>
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php the_title(); ?>&summary=&source="
       class="share__link share__link--linkedin" target="_blank">
        <svg>
            <use xlink:href="#icon-linkedin"></use>
        </svg>
    </a>
</div>