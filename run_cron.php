<?php
use Gravity\Hexon\Parser\HexonParser;
echo "included";
$pending   = get_template_directory() . '/includes/gravity-hexon/items/pending';
$done      = get_template_directory() . '/includes/gravity-hexon/items/done';
$parser    = new HexonParser( 'v2', $output );
$files     = array_diff( scandir( $pending ), [ '.', '..', '.gitkeep' ] );
$is_active = get_option( 'hexon_parser' );

if ( $is_active ) {
  return 0;
}

if ( ! empty( $files ) ) {
  add_option( 'hexon_parser', true );
} else {
  return 0;
}

foreach ( $files as $file ) {
  $car = new \SimpleXMLElement( preg_replace( '/&(?!#?[a-z0-9]+;)/', '&amp;', file_get_contents( $pending . '/' . $file ) ) );
  if ($parser->process_car( $car, 'voorraad', $file ) ) {
    rename( $pending . '/' . $file, $done . '/' . $file );
  }
}

if ( function_exists( 'rocket_clean_domain' ) ) {
  rocket_clean_domain();
}

delete_option( 'hexon_parser' );

return 0;
