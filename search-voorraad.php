<?php

use Gravity\Hexon\Search;

/**
 * The template for displaying aanbod zoeken
 */

global $wp_query;

$search       = new Search\HexonSearch();
$args         = $search->get_search_results( $wp_query->get( 'car_post_type' ) );



$object_query = new \WP_Query( $args );

get_header();

?>
    <article class="content">
        <header class="content-hero content-hero__hexon">
            <div class="container content-hero__container">
                <div class="row content-hero__row ">
                    <div class="col-12">
                        <h1><?php _e( 'Occasions' ); ?></h1>
                    </div>
                </div>
            </div>
        </header>
    </article>
    <section class="hexon__archive content__section section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="filter__toggle js-filter-toggle">Open filter</div>
                    <aside class="filter__wrapper">
                        <div class="filter__close js-filter-toggle">
                            <svg class="filter__close-icon">
                                <use xlink:href="#icon-close"/>
                            </svg>
                        </div>
                        <div class="filter__scroll">
							<?php echo do_shortcode( '[hexon_voorraad_filters]' ); ?>
                        </div>
                    </aside>
                </div>
                <div class="col-12 col-md-8 col-lg-9">
                    <div class="text-right">
                        <div class="select car-overview__select">
							<?php echo do_shortcode( '[hexon_voorraad_sort]' ); ?>
                        </div>
                    </div>
                    <section class="hexon-results car-overview">
	                    <?php
	                    if ( have_posts() ) :
		                    // Start the Loop.
		                    while ( have_posts() ) : the_post();
			                    include( locate_template( 'template-parts/loop/content-occasion.php' ) );
			                    // End the loop.
		                    endwhile;
		                    get_template_part( 'template-parts/loop/pagination' );
	                    else:
		                    ?>
                            <div class="no-results">Geen resultaten gevonden</div>
	                    <?php
	                    endif;
	                    ?>
                    </section>
                </div>
            </div>
        </div>
    </section>
<?php

get_footer();
