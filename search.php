<?php
/**
 * search template file
 * search template file
 *
 */

get_header(); ?>

  <header class="hero--news">
    <?php include( STYLESHEETPATH. '/parts/shape-animation.php');  ?>
    <div class="container container--hero">
      <div class="top--news">
        <h1 class="news__hero-title">U heeft gezocht naar "<?php esc_html_e( get_search_query()); ?>"</h1>
      </div>
    </div>
  </header>

  <?php if ($wp_query->have_posts() ) : ?>
  <section class="news__section">
    <div class="container  container--news">
      <div id="main" class="row row--news">


        <!-- the loop -->
        <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
            <?php get_template_part( 'template-parts/loop/content', 'post'); ?>
        <?php endwhile; ?>

        <!-- end of the loop -->

          <navigation class="pagination">
            <?php $args = array(
            'base'               => '%_%',
            'format'             => '?paged=%#%',
            'total'              => 1,
            'current'            => 0,
            'show_all'           => false,
            'end_size'           => 1,
            'mid_size'           => 2,
            'prev_next'          => true,
            'prev_text'          => __('« Vorige'),
            'next_text'          => __('Volgende »'),
            'type'               => 'plain',
            'add_args'           => false,
            'add_fragment'       => '',
            'before_page_number' => '',
            'after_page_number'  => ''
            ); ?>
            <?php //echo paginate_links( ); ?>
          </navigation>

        <!-- pagination here -->
        <?php
        global $wp_query; // you can remove this line if everything works for you

        // don't display the button if there are not enough posts
        if (  $wp_query->max_num_pages > 1 )
          echo '<button class="button button--primary button--news load-ajax-posts">Meer berichten</button>';
        ?>

        <?php wp_reset_postdata(); ?>

      </div>
    </div>
  </section>


  <?php else : ?>

    <section class="" style="margin: 60px 0px;">
      <div class="container">
        <div class="row">
          <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
            <h3>Je bent offroad gegaan</h3>
            <p>Sorry, we kunnen de pagina die je zoekt niet vinden.</p>
          </div>
        </div>
      </div>
    </section>

  <?php endif; ?>

<?php get_footer();
