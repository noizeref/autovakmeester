<?php
/**
 * The template for displaying the single for aanbod
 */
global $wp_query, $post;

use Gravity\Hexon\HexonObject;

$object = new HexonObject\HexonObject();
$search = new Gravity\Hexon\Search\HexonSearch();
$parser = new Gravity\Hexon\Parser\HexonParser();
get_header();

if ( $car_slug = $wp_query->get( 'car_slug' ) ) {
	$post = $search->get_page_by_post_name( $car_slug, OBJECT, 'voorraad' );
    setup_postdata($post);
}



// Get the object fields
$car          = $object->parse_car( get_the_ID() );
$gallery_type = 'local';
$gallery      = $object->parse_gallery( get_the_ID() );
$media        = [];

if ( empty( $gallery ) ) {
	$gallery = json_decode( get_post_meta( get_the_ID(), 'car_media', true ) );
    $gallery_type = 'empty';
    
	if ( ! empty( $gallery ) ) {
		if ( isset( $gallery->afbeelding->{'@attributes'} ) ) {
			$media[] = $gallery->afbeelding;
		} else {
			$media = reset( $gallery );
		}

		$gallery_type = 'hexon';
	}
}

$downloads = $object->parse_downloads( get_the_ID() );
$garage    = $parser->get_garage( $car['Klantnummer'] );

$videos = null;
if ( ! empty( $car['Car']['videos'] ) ) {
	$videos = $car['Car']['videos'];
}
?>
    <article id="post-<?php the_ID(); ?>">
        <header class="content-hero content-hero__hexon">
            <div class="container content-hero__container">
                <div class="content-hero__row row">
                    <header class="hexon-single__header hexon-single__section col-md-8">
                        <div class="d-flex">
                            <a href="javascript:history.back()" class="link link--red">Terug naar overzicht</a>
                        </div>
                        <h1 class="h2">

                    <?php echo( isset( $car['Car']['merk'] ) ? $car['Car']['merk'] : '' ); ?>&nbsp;<?php echo( isset( $car['Car']['model'] ) ? $car['Car']['model'] : '' ); ?>&nbsp;<?php echo( isset( $car['Car']['type'] ) ? $car['Car']['type'] : '' ); ?>

                        </h1>
                    </header>
                </div>
            </div>
        </header>
        <section class="section hexon-single">
            <div class="container">
                <div class="hexon-single__gallery hexon-single__section">
                    <div class="row">
                        <div class="col-lg-8 col-xl-9">
                            <div class="row">
                                <div class="col-xl-10">
                                    <div class="hexon-single__gallery--slider slider">
										<?php if ( $videos ) {
											if ( ! empty( $videos['video'] ) ) {
												if ( isset( $videos['video']['@attributes'] ) ) {
													$video = $videos['video'];
													if ( isset( $video['bron'] ) && $video['bron'] == 'taggle' ) {
														?>
                                                        <div class="video">
                                                            <iframe width="1024" height="768"
                                                                    src="https://taggleauto.movieplayer.nl/<?php echo $video['videocode']; ?>"
                                                                    frameborder="0"
                                                                    allow="autoplay; encrypted-media"
                                                                    allowfullscreen></iframe>
                                                        </div>
														<?php
													} else if ( isset( $video['videocode'] ) && $video['videocode'] == 'youtube' ) {
														?>
                                                        <div class="video">
                                                            <iframe width="1024" height="768"
                                                                    src="https://www.youtube.com/embed/<?php echo $video['videocode']; ?>"
                                                                    frameborder="0"
                                                                    allow="autoplay; encrypted-media"
                                                                    allowfullscreen></iframe>
                                                        </div>
														<?php
													}
												} else {
													foreach ( $videos['video'] as $video ) {
														if ( isset( $video['bron'] ) && $video['bron'] == 'taggle' ) {
															?>
                                                            <div class="video">
                                                                <iframe width="1024" height="768"
                                                                        src="https://taggleauto.movieplayer.nl/<?php echo $video['videocode']; ?>"
                                                                        frameborder="0"
                                                                        allow="autoplay; encrypted-media"
                                                                        allowfullscreen></iframe>
                                                            </div>
															<?php
															break;
														} else if ( isset( $video['videocode'] ) && $video['videocode'] == 'youtube' ) {
															?>
                                                            <div class="video">
                                                                <iframe width="1024" height="768"
                                                                        src="https://www.youtube.com/embed/<?php echo $video['videocode']; ?>"
                                                                        frameborder="0"
                                                                        allow="autoplay; encrypted-media"
                                                                        allowfullscreen></iframe>
                                                            </div>
															<?php
															break;
														}
													}
												}
											}
										} ?>

										<?php if ( ! empty( $media ) ) { ?>
											<?php foreach ( $media as $item ) { ?>
                                                <div class="contained--slide" style="background-image: url('<?php echo $item->url; ?>');">

                                                </div>
											<?php } ?>
										<?php } else { ?>
											<?php foreach ( $gallery as $item ) {
												$gallery_image = wp_get_attachment_image_src( $item->ID, 'large' );
												?>
                                                <div class="contained--slide" style="background-image: url('<?php echo $gallery_image[0]; ?>');"></div>
											<?php } ?>
										<?php } ?>

                                    </div>
                                </div>
                                <div class="col-xl-2 hexon-single__gallery-navigation">
                                    <div class="hexon-single__gallery--slider__navigation slider">
										<?php if ( $videos ) {
											if ( ! empty( $videos['video'] ) ) {
												if ( isset( $videos['video']['@attributes'] ) ) {
													$video = $videos['video'];
													if ( isset( $video['bron'] ) && $video['bron'] == 'taggle' ) {
														?>
                                                        <div>
                                                            <div class="video-thumbnail"
                                                                 style="background-image: url('https://taggleauto.movieplayer.nl/<?php echo $video['videocode']; ?>/image.jpg')"></div>
                                                        </div>
														<?php
													} else if ( isset( $video['videocode'] ) && $video['videocode'] == 'youtube' ) {
														?>
                                                        <div>
                                                            <div class="video-thumbnail"
                                                                 style="background-image: url('https://i4.ytimg.com/vi/<?php echo $video['videocode']; ?>/default.jpg')"></div>
                                                        </div>
														<?php
													}
												} else {
													foreach ( $videos['video'] as $video ) {
														if ( isset( $video['bron'] ) && $video['bron'] == 'taggle' ) {
															?>
                                                            <div>
                                                                <div class="video-thumbnail"
                                                                     style="background-image: url('https://taggleauto.movieplayer.nl/<?php echo $video['videocode']; ?>/image.jpg')"></div>
                                                            </div>
															<?php
															break;
														} else if ( isset( $video['videocode'] ) && $video['videocode'] == 'youtube' ) {
															?>
                                                            <div>
                                                                <div class="video-thumbnail"
                                                                     style="background-image: url('https://i4.ytimg.com/vi/<?php echo $video['videocode']; ?>/default.jpg')"></div>
                                                            </div>
															<?php
															break;
														}
													}
												}
											}
										} ?>

										<?php if ( ! empty( $media ) ) { ?>
											<?php foreach ( $media as $item ) { ?>
                                                <div>
                                                    <div class="video-thumbnail"
                                                         style="background-image: url('<?php echo $item->url; ?>')"></div>
                                                </div>
											<?php } ?>
										<?php } else { ?>
											<?php foreach ( $gallery as $item ) { ?>
												<?php $gallery_image = wp_get_attachment_image_src( $item->ID, 'thumbnail' ); ?>
                                                <div>
                                                    <div class="video-thumbnail"
                                                         style="background-image: url('<?php echo $gallery_image[0]; ?>')"></div>
                                                </div>
											<?php } ?>
										<?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 hexon-single__details">
                            <div class="hexon-single__card-holder service__wrapper">

								<?php if ( isset( $car['Prijs'] ) ) { ?>
                                    <div class="service__link">
                                        <div class="service__left">
                                            <div class="service__circle service__circle--green">
                                                <svg class="service__icon">
                                                    <use xlink:href="#icon-basic"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="service__right">
                                            <div class="service__caption">
                                                <h4 class="service__title"><?php if ( $car['Prijs'] !== 0 ) { ?>
														<?php echo '&euro; ' . number_format( $car['Prijs'], 0, ',', '.' ) . ',-'; ?>
													<?php } else { ?>
														<?php _e( 'Op aanvraag' ); ?>
													<?php } ?></h4>
                                                <div class="service__description">
													<?php if ( $car['Prijs'] !== 0 ) { ?>
														<?php if ( $car['Car']['voertuigsoort'] == 'BEDRIJF' ) { ?>
															<?php _e( 'excl. BTW' ); ?>
														<?php } ?>
													<?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
								<?php if ( isset( $car['Car']['bouwjaar'] ) && ! empty( $bouwjaar = $car['Car']['bouwjaar'] ) ) { ?>
                                    <div class="service__link">
                                        <div class="service__left">
                                            <div class="service__circle service__circle--blue">
                                                <svg class="service__icon">
                                                    <use xlink:href="#icon-calendar"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="service__right">
                                            <div class="service__caption">
                                                <h4 class="service__title"><?php echo $bouwjaar; ?></h4>
                                                <div class="service__description"><?php _e( 'Bouwjaar' ); ?></div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>

								<?php if ( isset( $car['Car']['brandstof'] ) && ! empty( $brandstof = $car['Car']['brandstof'] ) ) { ?>
                                    <div class="service__link">
                                        <div class="service__left">
                                            <div class="service__circle service__circle--yellow">
                                                <svg class="service__icon">
                                                    <use xlink:href="#icon-engine"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="service__right">
                                            <div class="service__caption">
                                                <h4 class="service__title"><?php
                                                    $brandstof_types = ['B' => 'Benzine',
                                                                        'D' => 'Diesel',
                                                                        '3' => 'LPG',
                                                                        'L' => 'LPG',
                                                                        'E' => 'Elektrisch of Hybride',
                                                                        'H' => 'Elektrisch of Hybride',
                                                                        'C' => 'CVT'];

                                                    echo array_key_exists($brandstof, $brandstof_types) ? $brandstof_types[$brandstof] : "Onbekend ($brandstof)";
													?></h4>
                                                <div class="service__description"><?php _e( 'Brandstof' ); ?></div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
								<?php if ( $garage ) { ?>
                                    <div class="service__link service__link--garage">
                                        <div class="service__right">
                                            <div class="service__caption">
                                                <h4 class="service__title"><?php _e( 'Jouw vestiging' ); ?></h4>
                                                <div class="service__description">
                                                    <strong><?php echo $garage->post_title; ?></strong>
													<?php if ( $phone = get_field( 'garage_phone', $garage->ID ) ) { ?>
                                                        <p><?php echo $phone; ?><br/>
															<?php if ( $mail = get_field( 'garage_mail', $garage->ID ) ) { ?>
                                                                <a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
															<?php } ?>
                                                        </p>
													<?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hexon-single__content hexon-single__section">
                    <header>
                        <h4><?php _e( 'Omschrijving' ); ?></h4>
                    </header>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="wysiwyg">
								<?php the_content(); ?>
                            </div>
                        </div>
                        <div class="col-lg-3 offset-lg-1 ">
                            <div class="hexon-single__card share">
								<?php get_template_part( 'parts/share' ); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="hexon-single__specifications hexon-single__section">
                    <header>
                        <h4><?php _e( 'Specificaties' ); ?></h4>
                    </header>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="wysiwyg">
                                <table class="table">
                                    <tr>
                                        <td colspan="2" class="table--heading"><h5><?php _e( 'Algemeen' ); ?></h5></td>
                                    </tr>
									<?php if ( isset( $car['Car']['merk'] ) && ! empty( $merk = $car['Car']['merk'] ) && isset( $car['Car']['model'] ) && ! empty( $model = $car['Car']['model'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Merk en model' ); ?>
                                            </td>
                                            <td>
												<?php echo $merk; ?>&nbsp;<?php echo $model; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['type'] ) && ! empty( $uitvoering = $car['Car']['type'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Uitvoering' ); ?>
                                            </td>
                                            <td>
												<?php echo $uitvoering; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['carrosserie'] ) && ! empty( $carrosserie = $car['Car']['carrosserie'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Carrosserie' ); ?>
                                            </td>
                                            <td>
												<?php echo $carrosserie; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['kenteken'] ) && ! empty( $kenteken = $car['Car']['kenteken'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Kenteken' ); ?>
                                            </td>
                                            <td>
												<?php echo $kenteken; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['tellerstand'] ) && ! empty( $tellerstand = $car['Car']['tellerstand'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Tellerstand' ); ?>
                                            </td>
                                            <td>
												<?php echo is_array($tellerstand) ? 0 : $tellerstand; ?><?php _e( 'km' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['transmissie'] ) && ! empty( $transmissie = $car['Car']['transmissie'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Transmissie' ); ?>
                                            </td>
                                            <td>
                                                <?php 
                                                $transmissie_types = ['S' => 'Semi-automaat',
                                                                      'A' => 'Automaat',
                                                                      'C' => 'CVT',
                                                ];
                                                echo array_key_exists($transmissie, $transmissie_types) ? $transmissie_types[$transmissie] : 'Handgeschakeld';
                                                ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['aantal_deuren'] ) && ! empty( $aantal_deuren = $car['Car']['aantal_deuren'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Aantal deuren' ); ?>
                                            </td>
                                            <td>
												<?php echo $aantal_deuren; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['btw_marge'] ) && ! empty( $btw = $car['Car']['btw_marge'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'BTW / Marge' ); ?>
                                            </td>
                                            <td>
												<?php switch ( $btw ) {
													case "B":
														echo 'BTW';
														break;
													case "M":
														echo 'Marge';
														break;
													default:
														echo 'BTW';
												} ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['nap_weblabel'] ) && ! empty( $nap = $car['Car']['nap_weblabel'] ) ) {
                                        if ($nap == 'j') {
                                        ?>
                                        <tr>
                                            <td>
												<?php _e( 'NAP' ); ?>
                                            </td>
                                            <td>
												<?php echo 'Ja'; ?>
                                            </td>
                                        </tr>
									<?php } } ?>
									<?php if ( isset( $car['Car']['bovag_garantie'] ) && ! empty( $bovag = $car['Car']['bovag_garantie'] ) ) { 
                                        if ($bovag == 'j') {
                                        ?>
                                        <tr>
                                            <td>
												<?php _e( 'Bovag Garantie' ); ?>
                                            </td>
                                            <td>
												<?php echo 'Ja'; ?>
                                            </td>
                                        </tr>
                                    <?php } } ?>
                                    <?php 
                                    if ( isset( $car['Car']['bovag_garantie'] ) && ! empty( $bovag = $car['Car']['bovag_garantie'] ) ) { 
                                        if ($bovag == 'j') {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php _e( 'Bovag Garantie geldig tot'); ?>
                                            </td>
                                            <td>
                                                <?php echo !empty($car['Car']['garantie_maanden']) ? $car['Car']['garantie_maanden'] : 'Niet beschikbaar'; ?>
                                            </td>
                                        </tr>
                                    <?php } } ?>
									<?php
									if ( isset( $car['Car']['apk'] ) && ! empty( $apk = $car['Car']['apk'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'APK bij aflevering' ); ?>
                                            </td>
                                            <td>
												<?php switch ( $apk['@attributes']['bij_aflevering'] ) {
													case "j":
														echo 'Ja';
														break;
													default:
														echo 'N.V.T';
												} ?>
                                            </td>
                                        </tr>
									<?php } ?>
                                    <?php 
                                    if ( isset( $car['Car']['apk'] ) && ! empty( $apk = $car['Car']['apk'] ) ) { 
                                        if ($apk['@attributes']['bij_aflevering'] == 'j') {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php _e( 'APK geldig tot'); ?>
                                            </td>
                                            <td>
                                                <?php echo !empty($apk['@attributes']['tot']) ? $apk['@attributes']['tot'] : 'Niet beschikbaar'; ?>
                                            </td>
                                        </tr>
                                    <?php } } ?>
									<?php
									if ( isset( $car['Car']['wegenbelasting_kwartaal'] ) && ! empty( $wegenbelasting = $car['Car']['wegenbelasting_kwartaal'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Wegenbelasting per kwartaal' ); ?>
                                            </td>
                                            <td>
                                                van &euro; <?php echo $wegenbelasting['@attributes']['min']; ?>,- tot
                                                &euro; <?php echo $wegenbelasting['@attributes']['max']; ?>,-
                                            </td>
                                        </tr>
									<?php } ?>
                                </table>

                                <table class="table">
                                    <tr>
                                        <td colspan="2" class="table--heading"><h5><?php _e( 'Motor' ); ?></h5></td>
                                    </tr>
									<?php if ( isset( $car['Car']['cilinder_inhoud'] ) && ! empty( $inhoud_cilinder = $car['Car']['cilinder_inhoud'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Cilinder inhoud' ); ?>
                                            </td>
                                            <td>
												<?php echo $inhoud_cilinder; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['cilinder_aantal'] ) && ! empty( $aantal_cilinders = $car['Car']['cilinder_aantal'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Aantal cilinders' ); ?>
                                            </td>
                                            <td>
												<?php echo $aantal_cilinders; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['vermogen_motor_pk'] ) && ! empty( $vermogen = $car['Car']['vermogen_motor_pk'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Vermogen' ); ?>
                                            </td>
                                            <td>
												<?php echo $vermogen; ?>&nbsp;<?php _e( 'pk' ); ?>
                                                / <?php echo $car['Car']['vermogen_motor_kw']; ?>
                                                &nbsp;<?php _e( 'kw' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['topsnelheid'] ) && ! empty( $topsnelheid = $car['Car']['topsnelheid'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Topsnelheid' ); ?>
                                            </td>
                                            <td>
												<?php echo $topsnelheid; ?>&nbsp;<?php _e( 'km/u' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="wysiwyg">
                                <table class="table">
                                    <tr>
                                        <td colspan="2" class="table--heading"><h5><?php _e( 'Uiterlijk' ); ?></h5></td>
                                    </tr>
									<?php if ( isset( $car['Car']['kleur'] ) && ! empty( $kleur = $car['Car']['kleur'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Kleur' ); ?>
                                            </td>
                                            <td>
												<?php echo $kleur; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['bekleding'] ) && ! empty( $bekleding = $car['Car']['bekleding'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Bekleding' ); ?>
                                            </td>
                                            <td>
												<?php echo $bekleding; ?>
                                            </td>
                                        </tr>
									<?php } ?>
                                </table>

                                <table class="table">
                                    <tr>
                                        <td colspan="2" class="table--heading"><h5><?php _e( 'Verbruik' ); ?></h5></td>
                                    </tr>
									<?php if ( isset( $car['Car']['energielabel'] ) && ! empty( $energielabel = $car['Car']['energielabel'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Energielabel' ); ?>
                                            </td>
                                            <td>
												<?php echo $energielabel; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['gemiddeld_verbruik'] ) && ! empty( $verbruik_gemiddeld = $car['Car']['gemiddeld_verbruik'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Gemiddeld verbruik' ); ?>
                                            </td>
                                            <td>
												<?php echo $verbruik_gemiddeld; ?>&nbsp;<?php _e( 'l/100km' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['verbruik_snelweg'] ) && ! empty( $verbruik_snelweg = $car['Car']['verbruik_snelweg'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Verbruik snelweg' ); ?>
                                            </td>
                                            <td>
												<?php echo $verbruik_snelweg; ?>&nbsp;<?php _e( 'l/100km' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['verbruik_stad'] ) && ! empty( $verbruik_stad = $car['Car']['verbruik_stad'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Verbruik stad' ); ?>
                                            </td>
                                            <td>
												<?php echo $verbruik_stad; ?>&nbsp;<?php _e( 'l/100km' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['co2_uitstoot'] ) && ! empty( $co2 = $car['Car']['co2_uitstoot'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'CO2 uitstoot' ); ?>
                                            </td>
                                            <td>
												<?php echo $co2; ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( ! $energielabel && ! $verbruik_gemiddeld && ! $verbruik_snelweg && ! $verbruik_stad && ! $co2 ) { ?>
                                        <tr>
                                            <td colspan="2">
												<?php _e( 'Niet bekend' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
                                </table>

                                <table class="table">
                                    <tr>
                                        <td colspan="2" class="table--heading"><h5><?php _e( 'Gewicht' ); ?></h5></td>
                                    </tr>
									<?php if ( isset( $car['Car']['massa'] ) && ! empty( $massa = $car['Car']['massa'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Massa' ); ?>
                                            </td>
                                            <td>
												<?php echo $massa; ?>&nbsp;<?php _e( 'kg' ); ?>
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['max_trekgewicht'] ) && ! empty( $car['Car']['max_trekgewicht'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Max. trekgewicht' ); ?>
                                            </td>
                                            <td>
												<?php echo $car['Car']['max_trekgewicht']; ?> kg
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['max_trekgewicht_ongeremd'] ) && ! empty( $car['Car']['max_trekgewicht_ongeremd'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Max. trekgewicht ongeremd' ); ?>
                                            </td>
                                            <td>
												<?php echo $car['Car']['max_trekgewicht_ongeremd']; ?> kg
                                            </td>
                                        </tr>
									<?php } ?>
									<?php if ( isset( $car['Car']['max_trekgewicht_geremd'] ) && ! empty( $car['Car']['max_trekgewicht_geremd'] ) ) { ?>
                                        <tr>
                                            <td>
												<?php _e( 'Max. trekgewicht geremd' ); ?>
                                            </td>
                                            <td>
												<?php echo $car['Car']['max_trekgewicht_geremd']; ?> kg
                                            </td>
                                        </tr>
									<?php } ?>
                                </table>
								<?php if ( isset( $car['Car']['bijtelling_pct'] ) && ! empty( $bijtelling = $car['Car']['bijtelling_pct'] ) ) { ?>
                                    <table class="table">
                                        <tr>
                                            <td colspan="2" class="table--heading"><h5><?php _e( 'Bijtelling' ); ?></h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
												<?php _e( 'Bijtelling percentage' ); ?>
                                            </td>
                                            <td>
												<?php echo $bijtelling; ?>%
                                            </td>
                                        </tr>

                                    </table>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

				<?php if ( isset( $car['Car']['accessoiregroepen'] ) ) { ?>
                    <div class="hexon-single__options hexon-single__section">
                        <header>
                            <h4><?php _e( 'Opties en accessoires' ); ?></h4>
                        </header>
                        <div class="row">
                            <div class="col-md-8">
                                <ul class="faq__list">
									<?php
									if ( count( $car['Car']['accessoiregroepen'] ) > 1 && ! isset( $car['Car']['accessoiregroepen']['accessoire'] ) ) {
										$accessoire_groups = $car['Car']['accessoiregroepen'];
										foreach ( $accessoire_groups as $accessoire ) {
											?>
                                            <li class="faq__item">
                                                <div class="faq__question js-question">
                                                    <span><?php echo $accessoire['@attributes']['naam']; ?></span>
                                                    <svg class="faq__icon">
                                                        <use xlink:href="#icon-faq"/>
                                                    </svg>
                                                </div>
                                                <div class="faq__answer js-answer">
                                                    <div class="wysiwyg">
                                                        <ul>
															<?php if ( is_array( $accessoire['accessoire'] ) ) {
																{
																	foreach ( $accessoire['accessoire'] as $item ) { ?>
                                                                        <li><?php echo ucfirst( $item ); ?></li>
																	<?php }
																}
															} else {
																?>
                                                                <li><?php echo ucfirst( $accessoire['accessoire'] ); ?></li>
																<?php
															}
															?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
										<?php }
									} else {
										$accessoire = $car['Car']['accessoiregroepen'];
										?>
                                        <li class="faq__item">
                                            <div class="faq__question js-question">
                                                <span><?php echo $accessoire['@attributes']['naam']; ?></span>
                                                <svg class="faq__icon">
                                                    <use xlink:href="#icon-faq"/>
                                                </svg>
                                            </div>
											<?php if ( isset( $accessoire['accessoire'] ) ) { ?>
                                                <div class="faq__answer js-answer">
                                                    <div class="wysiwyg">
                                                        <ul>
															<?php if ( is_array( $accessoire['accessoire'] ) ) {
																{
																	foreach ( $accessoire['accessoire'] as $item ) { ?>
                                                                        <li><?php echo ucfirst( $item ); ?></li>
																	<?php }
																}
															} else {
																?>
                                                                <li><?php echo ucfirst( $accessoire['accessoire'] ); ?></li>
																<?php
															}
															?>
                                                        </ul>
                                                    </div>
                                                </div>
											<?php } ?>
                                        </li>
										<?php
									}
									?>
                                </ul>
                            </div>
                        </div>
                    </div>
				<?php } ?>
				<?php if ( ! empty( $downloads ) ) { ?>
                    <div class="hexon-single__options hexon-single__section">
                        <header>
                            <h4><?php _e( 'Documenten' ); ?></h4>
                        </header>
                        <div class="row">
                            <div class="col-md-8">
                                <ul class="faq__list">
									<?php foreach ( $downloads as $download ) { ?>
                                        <li class="faq__item">
                                            <div class="faq__question js-question">
                                                <span><?php echo $download->post_title; ?></span>
                                                <svg class="faq__icon">
                                                    <use xlink:href="#icon-faq"/>
                                                </svg>
                                            </div>
                                            <div class="faq__answer js-answer">
                                                <div class="wysiwyg">
                                                    <ul>
                                                        <li>
                                                            <a href="<?php echo wp_get_attachment_url( $download->ID ); ?>"
                                                               target="_blank"><?php _e( 'Bekijk document' ); ?></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
									<?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
				<?php } ?>
            </div>
        </section>
    </article>
<?php

get_footer();
