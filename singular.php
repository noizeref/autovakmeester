<?php
/**
 * Default single template file
 *
 */

get_header(); ?>
<?php if ( have_posts() ) :

  $post_type = get_post_type();

  // Start the loop.
  while ( have_posts() ) : the_post();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('content'); ?>>
  <header class="news-hero">
    <?php 
      if(has_post_thumbnail()):
        $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' );
        $featured_image_url = $featured_image[0];
      else:
        $featured_image_url = get_stylesheet_directory_uri() . '/assets/img/news-hero.jpg';
      endif;
    ?>
      <figure class="post-image featured-image background-image news-hero__image" style="background-image: url(<?php echo $featured_image_url; ?>);"></figure>
      <div class="news-hero__gradient"></div>
    <div class="news-hero__container container">
      <div class="row">
        <div class="col-12 col-md-3">
          <h6 class="news-hero__subtitle">Bericht delen</h5>
          <div class="share__flex  share__flex--opacity">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank" class="share__link share__link--facebook"><svg><use xlink:href="#icon-facebook" /></svg></a>
            <a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" target="_blank" class="share__link share__link--twitter"><svg><use xlink:href="#icon-twitter"></use></svg></a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=" target="_blank" class="share__link share__link--linkedin"><svg><use xlink:href="#icon-linkedin"></use></svg></a>
          </div>
        </div>
        <div class="col-12 col-md-8">
          <h6 class="news-hero__subtitle"><?php echo get_the_date(); ?></h5>
          <h1 class="news-hero__title"><?php the_title(); ?></h1>
        </div>
        <div class="col-12 col-md-1">
          <div class="news-hero__triangles">
            <div class="news-hero__triangle"></div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <section class="section section--news-single">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-3">
          <a href="<?php echo get_post_type_archive_link( $post_type ); ?>" class="link link--red">Terug naar overzicht</a>
        </div>
        <div class="col-12 col-md-6">
          <div class="wysiwyg">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section">
    <div class="container container--news">
      <div class="row">
        <div class="col-12 text-center">
          <h2 class="news__subtitle">Bekijk ook eens</h2>
        </div>
      </div>
      <div class="row row--news">
          <?php
          $current_post = get_the_ID();
          $args = array(
              'posts_per_page'    => 2,
              'post_type'         => 'post',
              'orderby'           => 'rand',
              'post__not_in'      => array($current_post),  
          );
          $wp_query = new WP_Query( $args ); ?>

          <?php if( have_posts() ): ?>
              <?php while( have_posts() ): the_post(); ?>

                <?php get_template_part( 'template-parts/loop/content', $post_type ); ?>

              <?php endwhile; ?>
              <!-- end of the loop -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>

      </div>
    </div>
  </section>
</article>

<?php
  endwhile;

// If no content, include the "No posts found" template.
else :
  get_template_part( 'template-parts/content', 'none' );

endif;
?>

<?php get_footer();
