<?php

$garage_home_id = get_garage_home_id();
?>

<header class="contact-hero">
  <div class="contact-hero__top">
    <?php include (STYLESHEETPATH. '/parts/shape-animation.php'); ?>
    <div class="container container--hero">
      <div class="row">
        <div class="col-12">
          <h1 class="contact-hero__title">Hier kun je <br> ons bereiken</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="contact-hero__bottom">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-5">
          <?php the_content(); ?>
          <br/>
          <ul class="contact__list">

          <?php
            $garage_location  = get_field('business_location', $garage_home_id);
            $location_fields  = explode(',', $garage_location['address']);
            $garage_mail      = get_field('garage_mail', $garage_home_id);
            $garage_phone     = get_field('garage_phone', $garage_home_id);

            foreach($location_fields as &$location){
              echo '<li class="contact__item">'.$location.'</li>';
            }
          ?>
          </ul>
          <ul class="contact__list">
            <li class="contact__item">Tel: <a class="contact__link" href="tel:<?php echo antispambot($garage_phone); ?>"><?php echo antispambot($garage_phone); ?></a></li>
            <li class="contact__item">E-mail: <a class="contact__link" href="mailto:<?php echo antispambot($garage_mail); ?>"><?php echo antispambot($garage_mail); ?></a></li>
          </ul>
          <?php if(get_field('garage_hours_workshop', $garage_home_id) || get_field('garage_hours_showroom', $garage_home_id)): ?>
            <ul class="company__actions">
              <?php if(get_field('garage_hours_workshop', $garage_home_id) || get_field('garage_hours_showroom', $garage_home_id)): ?>
                <li>
                  <div class="link link--red js-modal-toggle">Openingstijden</div>
                </li>
              <?php endif; ?>
            </ul>
          <?php endif; ?>
          <?php if(get_field('garage_facebook', $garage_home_id) || get_field('garage_twitter', $garage_home_id) || get_field('garage_youtube', $garage_home_id) || get_field('garage_linkedin', $garage_home_id)): ?>
            <div class="company__share">
                <div class="share__flex  share__flex--clear">
                    <?php if(get_field('garage_facebook', $garage_home_id)): ?>
                        <a href="<?php the_field('garage_facebook', $garage_home_id); ?>" class="share__link share__link--facebook" target="_blank">
                            <svg>
                                <use xlink:href="#icon-facebook"/>
                            </svg>
                        </a>
                    <?php endif; if(get_field('garage_twitter', $garage_home_id)): ?>
                        <a href="<?php the_field('garage_twitter', $garage_home_id); ?>" class="share__link share__link--twitter" target="_blank">
                            <svg>
                                <use xlink:href="#icon-twitter"></use>
                            </svg>
                        </a>
                    <?php endif; if(get_field('garage_youtube', $garage_home_id)): ?>
                        <a href="<?php the_field('garage_youtube', $garage_home_id); ?>" class="share__link share__link--youtube" target="_blank">
                            <svg>
                                <use xlink:href="#icon-youtube"></use>
                            </svg>
                        </a>
                    <?php endif; if(get_field('garage_linkedin', $garage_home_id)): ?>
                        <a href="<?php the_field('garage_linkedin', $garage_home_id); ?>" class="share__link share__link--linkedin" target="_blank">
                            <svg>
                                <use xlink:href="#icon-linkedin"></use>
                            </svg>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
          <?php endif; ?>
        </div>
        <div class="col-12 col-md-7">
          <div class="contact__card">
            <h2 class="contact__title">Heb jij een vraag voor ons?</h2>
            <?php
              $form_id  = '3'; // get_sub_field( 'form' );
            ?>
            <?php gravity_form( $form_id, false, false, false, '', false, 90, true ); ?>

          </div>
        </div>
      </div>
      <hr class="contact__border">
    </div>
  </div>
</header>