<?php
  $garage_location  = get_field('business_location');
  $location_fields  = explode(',', $garage_location['address']);
  $garage_mail      = get_field('garage_mail');
  $garage_phone     = get_field('garage_phone');
  $flow_location    = get_flow_info('location'); // garage id, name en email
?>

<?php if(is_archive() || (isset($is_archive) && !empty($is_archive) ) ): ?>

  <?php // map view ?>
      <div class="map__result js-map-result marker" data-lat="<?php echo $garage_location['lat']; ?>" data-lng="<?php echo $garage_location['lng']; ?>" data-page-id="<?php echo get_the_ID(); ?>">
        <ul class="map__list">
          <li class="map__item">
            <h4 class="map__title"><?php the_title(); ?></h4>
          </li>
          <?php
            foreach($location_fields as &$location){
              echo '<li>'.$location.'</li>';
            }
          ?>
          <li class="map__item"><a href="tel:<?php echo antispambot($garage_phone); ?>">Tel. <?php echo antispambot($garage_phone); ?></a></li>
          <?php if(get_the_distance()): ?><div class="map__distance"><?php the_distance(null,true); ?>km</div><?php endif; ?>
        </ul>
        <article style="display: none;">
          <a href="<?php the_permalink(); ?>">
          <?php if(has_post_thumbnail()): ?>
            <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' ); ?>
            <div class="map__image" style="background-image: url(<?php echo $featured_image[0]; ?>);">
          <?php else: ?>
            <div class="map__image" style="background-image:url(<?php echo get_stylesheet_directory_uri(  ); ?>/assets/img/computer.jpg)">
          <?php endif; ?>
              <div class="map__image-caption">
                  <h4 class="map__image-title"><?php the_title(); ?></h4>
                  <?php if(get_the_distance()): ?><div class="map__distance"><?php the_distance(null,true); ?>km</div><?php endif; ?>
              </div>
            </div>
          </a>
            <ul class="map__list">
                <li style="list-style: none; display: inline">
                    <?php the_excerpt(); ?>
                </li>
                <?php
                  foreach($location_fields as &$location){
                    echo '<li class="map__item">'.$location.'</li>';
                  }
                ?>

                <li class="map__item"><a href="tel:<?php echo antispambot($garage_phone); ?>"><?php echo antispambot($garage_phone); ?></a></li>
                <li class="map__item"><a href="mailto:<?php echo antispambot($garage_mail); ?>"><?php echo antispambot($garage_mail); ?></a></li>
                <li class="map__item"><a href="<?php the_permalink(); ?>">Ga naar bedrijfspagina</a></li>
            </ul>
<!--             <a href="https://autovakmeester.staging.gravity.nl/vestiging/garage-2/" class="link">Route berekenen</a> -->
        </article>
      </div>
  <?php // end map view ?>

<?php else: ?>

  <?php // flow view ?>
    <?php  ?>

      <li class="location-search__result">
        <a href="" class="location-search__card <?php if( isset($flow_location['page-id']) && get_the_ID() == $flow_location['page-id'] ) echo "active-location"; ?>" data-page-id="<?php echo get_the_ID(); ?>">
          <figure class="location-search__aspect">

          <?php if(has_post_thumbnail()): ?>
            <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' ); ?>
              <div class="location-search__image post-image featured-image background-image" style="background-image: url(<?php echo $featured_image[0]; ?>);"></div>
          <?php else: ?>
              <div class="location-search__image" style="background-image:url(<?php echo get_stylesheet_directory_uri(  ); ?>/assets/img/computer.jpg)"></div>
          <?php endif; ?>


          </figure>
          <div class="location-search__caption">
            <?php get_post_meta(get_the_ID(),'_lat', true) ?>
            <h4 class="location-search__title"><Autovakmeester Lagerweij><?php the_title(); ?></h4>
            <div class="location-search__rating">
            <?php 
                if($clientid = get_field('klantenvertellen_clientid')){
                    the_klantenvertellen_total_score($clientid); 
                }
            ?>
            </div>
            <div class="location-search__information">
              <ul class="location-search__list">
                <?php
                  foreach($location_fields as &$location){
                    echo '<li>'.$location.'</li>';
                  }
                ?>
                <li>Tel. <?php echo antispambot($garage_phone); ?></li>
              </ul>
              <?php if(get_the_distance()): ?><div class="location-search__distance"><?php the_distance(null,true); ?>km<span> afstand</span></div><?php endif; ?>
            </div>
          </div>
        </a>
      </li>
  <?php // end flow view ?>

<?php endif; ?>



