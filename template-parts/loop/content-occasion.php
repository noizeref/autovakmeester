<?php

use Gravity\Hexon\HexonObject;

global $wp_query;
global $post;

$object = new HexonObject\HexonObject();
$car    = $object->parse_car( $post->ID );

if ( isset( $car['Car'] ) ) {
	$gallery_type   = 'local';
	$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

	if ( empty( $featured_image ) ) {
		$media = json_decode( get_post_meta( get_the_ID(), 'car_media', true ) );
		if ( ! empty( $media ) ) {
			if ( isset( $media->afbeelding->{'@attributes'} ) ) {
				$featured_image = $media->afbeelding;
			} elseif ( $media ) {
				if ( isset( $media->afbeelding ) ) {
					$media = reset( $media );
				}

				$object         = reset( $media );
				$media          = $object;
				$featured_image = $object;
			}
			$gallery_type = 'hexon';
		} else {
			$gallery_type = 'empty';
		}
	}

	if ( isset( $_POST['garage_slug'] ) ) {
		$garage_slug = $_POST['garage_slug'];
		$url         = get_bloginfo( 'url' ) . '/vestiging/' . $garage_slug . '/occasions/auto/' . $post->post_name . '/';
	} else if ( $garage_slug = $wp_query->get( 'garage_slug' ) ) {
		$url = get_bloginfo( 'url' ) . '/vestiging/' . $garage_slug . '/occasions/auto/' . $post->post_name . '/';
	} else {
		$url = get_the_permalink();
	}
	?>
    <a href="<?php echo $url; ?>" class="car-overview__link hexon-item">
        <article class="car-overview__article" id="post-<?php the_ID(); ?>">
			<?php if ( $featured_image && $gallery_type == 'local' ) { ?>
                <figure class="car-overview__aspect">
                    <div class="car-overview__image"
                         style="background-image: url(<?php echo $featured_image[0]; ?>);"></div>
                </figure>
			<?php } else if ( $featured_image && $gallery_type == 'hexon' ) { ?>
                <figure class="car-overview__aspect">
                    <div class="car-overview__image"
                         style="background-image: url(<?php echo $featured_image->url; ?>);"></div>
                </figure>
			<?php } ?>
            <header class="car-overview__caption">
                <h3 class="car-overview__title">
				<?php echo( isset( $car['Car']['merk'] ) ? $car['Car']['merk'] : '' ); ?>&nbsp;<?php echo( isset( $car['Car']['model'] ) ? $car['Car']['model'] : '' ); ?>&nbsp;<?php echo( isset( $car['Car']['type'] ) ? $car['Car']['type'] : '' ); ?>
					<!-- <?php echo( isset( $car['Car']['merk'] ) ? $car['Car']['merk'] : '' ); ?>&nbsp;

					<?php if(isset($car['Car']['model']) && is_array($car['Car']['model']) !== true ){
						echo $car['Car']['model'];
					} ?> &nbsp;

					<?php if( isset($car['Car']['type'])  &&  is_array($car['Car']['type']) !== true   ){
						echo $car['Car']['type'];
					}?> -->
				</h3>


                <div class="car-overview__subtitle">
					<?php if ( ! $garage_slug ) {
						if ( isset( $car['Klantnummer'] ) ) {
							$parser = new \Gravity\Hexon\Parser\HexonParser();
							if ( $garage = $parser->get_garage( $car['Klantnummer'] ) ) {
								echo $garage->post_title;
							}
						}
					} ?>
                </div>
                <ul class="car-overview__list">
					<?php if ( isset( $car['Car']['bouwjaar'] ) && ! empty( $bouwjaar = $car['Car']['bouwjaar'] ) ) { ?>
                        <li class="car-overview__list-item">
                            <svg class="car-overview__icon">
                                <use xlink:href="#icon-calendar"></use>
                            </svg>
                            <span><?php echo $car['Car']['bouwjaar']; ?></span>
                        </li>
					<?php } ?>
					<?php if ( isset( $car['Car']['tellerstand'] ) && ! empty( $tellerstand = $car['Car']['tellerstand'] ) ) { ?>
                        <li class="car-overview__list-item">
                            <svg class="car-overview__icon">
                                <use xlink:href="#icon-odometer"></use>
                            </svg>


                            <?php if(is_array($tellerstand)): ?>
                                <span><?php echo $tellerstand[array_keys($tellerstand)[1]]; ?><?php _e( 'km' ); ?></span>
                            <?php else: ?>
                            <span><?php echo $tellerstand; ?><?php _e( 'km' ); ?></span>
                            <?php endif; ?>
                        </li>
					<?php } ?>
					<?php if ( isset( $car['Car']['brandstof'] ) && ! empty( $brandstof = $car['Car']['brandstof'] ) ) { ?>
                        <li class="car-overview__list-item">
                            <svg class="car-overview__icon">
                                <use xlink:href="#icon-engine"></use>
                            </svg>
                            <span><?php
								$brandstof_types = ['B' => 'Benzine',
													'D' => 'Diesel',
													'3' => 'LPG',
													'L' => 'LPG',
													'E' => 'Elektrisch of Hybride',
													'H' => 'Elektrisch of Hybride',
													'C' => 'CVT'];

								echo array_key_exists($brandstof, $brandstof_types) ? $brandstof_types[$brandstof] : "Onbekend ($brandstof)";
								?></span>
                        </li>
					<?php } ?>
					<?php if ( isset( $car['Car']['transmissie'] ) && ! empty( $transmissie = $car['Car']['transmissie'] ) ) { ?>
                        <li class="car-overview__list-item">
                            <svg class="car-overview__icon">
                                <use xlink:href="#icon-gearshift"></use>
                            </svg>
							<span><?php 
								$transmissie_types = ['S' => 'Semi-automaat',
													  'A' => 'Automaat',
													  'C' => 'CVT',
													 ];
								echo array_key_exists($transmissie, $transmissie_types) ? $transmissie_types[$transmissie] : 'Handgeschakeld';
							?></span>
                        </li>
					<?php } ?>
                </ul>
				<?php if ( isset( $car['Prijs'] ) ) { ?>
                    <div class="car-overview__price"><?php if ( $car['Prijs'] !== 0 ) { ?>
							<?php echo '&euro; ' . number_format( $car['Prijs'], 2, ',', '.' ); ?>
							<?php if ( $car['Car']['voertuigsoort'] == 'BEDRIJF' ) { ?>
                                <small>
									<?php _e( 'excl. BTW' ); ?>
                                </small>
							<?php } ?>
						<?php } else { ?>
							<?php _e( 'Op aanvraag' ); ?>
						<?php } ?></div>
				<?php } ?>
            </header>
        </article>
    </a>
<?php } ?>
