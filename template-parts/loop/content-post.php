<div class="col-12 col-sm-6 col--news article-wrapper">
  <a href="<?php the_permalink(); ?>" class="news__link">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="news__aspect">
      <?php if(has_post_thumbnail()): ?>
        <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cropped-12-12' ); ?>
        <figure class="post-image featured-image background-image news__image" style="background-image: url(<?php echo $featured_image[0]; ?>);"></figure>
      <?php else: ?>
        <figure class="post-image no-featured-image background-image news__image" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/news-image.jpg);"></figure>
      <?php endif; ?>
    </div>
    <div class="news__information">
      <div class="news__date"><?php echo get_the_date(); ?></div>
      <h4 class="news__title"><?php the_title(); ?></h4>
      <div class="news__read-more"><?php _e('Lees meer', 'gravity_theme'); ?></div>
    </div>
    </article>
  </a>
</div>