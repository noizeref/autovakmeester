<navigation class="pagination">
	<?php 
	$paginate = array(
		'prev_text'          => __( '« Vorige' ),
		'next_text'          => __( 'Volgende »' ),
        'format'             => 'page/%#%'
	);

	if (array_key_exists("max_num_pages", $args)) {
		$paginate["total"] = $args["max_num_pages"];
	}
	?>
	<?php echo paginate_links( $paginate ); ?>
</navigation>
