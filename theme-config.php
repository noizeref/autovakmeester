<?php

// set the website in maintenance/under-construction mode
//define( 'IN_MAINTENANCE', true );

// Use CTP people
$GRAVITY_USE_CTP_PEOPLE         = false;

// Use the pritty search excerpts
$GRAVITY_USE_SEARCH_EXCERPT     = true;

//Show only exact search results
$GRAVITY_USE_EXACT_SEARCH       = false;

// Fix some default user roles
$GRAVITY_USE_USER_ROLES         = true;

// User the GDPR proof cookies allowed plugin
$GRAVITY_USE_COOKIES_ALLOWED    = true;

// Use function to lighten or darken any colour with php
$GRAVITY_USE_COLOR_LUMINANCE    = false;

// Allow ip's to visit the website when under construction
$GRAVITY_IP_WHITELIST           = array('123.123.123.123', '123.124.124.124');
